# Gc tool kit

[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.gc/gc.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.gc%22%20AND%20a%3A%22gc%22)
![Build Status](https://gitlab.com/geocaching/gc/badges/develop/build.svg)
[![The GNU General Public License, Version 3.0](https://img.shields.io/badge/license-GPL3-green.svg)](http://www.gnu.org/licenses/gpl-3.0.txt)

# Resources

* [Changelog and downloads](https://gitlab.com/geocaching/gc/blob/develop/CHANGELOG.md)
* [Documentation](http://ultreiaio.gitlab.io/gc)

# Community

* [Contact](mailto:gc@tchemit.fr)
