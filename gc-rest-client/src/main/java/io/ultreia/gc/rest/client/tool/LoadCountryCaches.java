package io.ultreia.gc.rest.client.tool;

/*-
 * #%L
 * GC toolkit :: REST Client
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.db.entity.GcCacheCountryStats;
import io.ultreia.gc.db.entity.GcCountry;
import io.ultreia.gc.service.api.GcCacheService;
import java.io.IOException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 15/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LoadCountryCaches extends GcRestClientToolSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LoadCountryCaches.class);

    public void run(String countryStr) throws IOException {

        GcCountry country = GcCountry.of(countryStr);

        log.info(String.format("Will load caches for country: %s", country));
        GcCacheService gcCacheService = applicationContext.newCacheService();

        GcCacheCountryStats stats = gcCacheService.getCountryStats(country, false);
        log.info(String.format("stats: %d, %d : %d%%", stats.getCurrent(), stats.getTotal(), stats.getCurrent() / stats.getTotal()));

    }
}
