package io.ultreia.gc.rest.client.tool;

/*-
 * #%L
 * GC toolkit :: REST Client
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import io.ultreia.gc.db.entity.GcCacheType;
import io.ultreia.gc.model.GcCache;
import io.ultreia.gc.rest.client.GcRestClientConfig;
import io.ultreia.gc.service.api.GcCacheService;
import io.ultreia.gc.service.internal.GpxServiceImpl;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 03/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class FixCoordinates extends GcRestClientToolSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(FixCoordinates.class);

    public void run(File gpxFile, boolean dryRun) throws IOException {
        Objects.requireNonNull(gpxFile);
        if (!gpxFile.exists()) {
            throw new IllegalStateException("Can't find " + gpxFile);
        }
        GpxServiceImpl gpxService = new GpxServiceImpl();

        GcRestClientConfig config = applicationContext.getConfig();

        File dataDirectory = config.getDataDirectory();
        File modifiedFiled = gpxService.toJsonFile(dataDirectory, gpxFile, "-modified");

        List<GcCache> incomingCaches = gpxService.loadGpxOrJson(dataDirectory, gpxFile);
        List<GcCache> mysteries = new LinkedList<>();
        List<GcCache> multis = new LinkedList<>();
        List<GcCache> ecs = new LinkedList<>();
        for (GcCache cache : incomingCaches) {
            if (!cache.getGcName().startsWith("GC")) {
                continue;
            }
            GcCacheType type = GcCacheType.of(cache.getGcType());
            if (GcCacheType.UNKNOWN == type) {
                mysteries.add(cache);
            } else if (GcCacheType.MULTI == type) {
                multis.add(cache);
            } else if (GcCacheType.EARTHCACHE == type) {
                ecs.add(cache);
            }
        }
        gpxService.store(mysteries, gpxService.toJsonFile(dataDirectory, gpxFile, "mysteries"));
        gpxService.store(multis, gpxService.toJsonFile(dataDirectory, gpxFile, "multis"));
        gpxService.store(ecs, gpxService.toJsonFile(dataDirectory, gpxFile, "ecs"));

        int incomingCachesSize = mysteries.size() + multis.size();
        GcCacheService gcCacheService = applicationContext.newCacheService();
        Set<String> badTypes = new LinkedHashSet<>();
        for (GcCache cache : incomingCaches) {
            GcCacheType type = GcCacheType.of(cache.getGcType());
            if (type == null) {
                if (badTypes.add(cache.getGcType())) {
                    log.warn(String.format("Cant find cache type for code: (GC:%s) %s", cache.getGcName(), cache.getGcType()));
                }
            }
        }
        if (!badTypes.isEmpty()) {
            throw new IllegalStateException("Can't find types: " + badTypes);
        }

        List<GcCache> caches;
        if (modifiedFiled.exists()) {


            caches = gpxService.load(modifiedFiled);
        } else {

            ImmutableSet<GcCacheType> TYPES = ImmutableSet.of(GcCacheType.UNKNOWN, GcCacheType.MULTI);

            MutableInt i = new MutableInt();

            caches = new LinkedList<>();
            incomingCaches.parallelStream().filter(r -> TYPES.contains(GcCacheType.of(r.getGcType()))).forEach(incomingCache -> {
                boolean addIt = checkCache(incomingCache, caches.size(), gcCacheService, i.incrementAndGet(), incomingCachesSize);
                if (addIt) {
                    caches.add(incomingCache);
                }

            });

            log.info(String.format("Found %d modified caches", caches.size()));

            gpxService.store(caches, modifiedFiled);
        }

        int size = caches.size();
        log.info(String.format("Will process %d cache(s).", size));

        MutableInt i = new MutableInt();
        caches.parallelStream().forEach(cache -> {
            String gcName = cache.getGcName();
            log.info(String.format("[%d/%d] Fix coordinates %s (%s,%s)", i.incrementAndGet(), size, cache.getGcName(), cache.getLat(), cache.getLon()));
            if (!dryRun) {

                GcCache myCache = gcCacheService.getMyCacheFromGcName(cache.getGcName());
                if (cache.equalsCoordinates(myCache)) {
                    log.info(String.format("[%d/%d] Cache: %s same coordinate than other, nothing to do.", i.getValue(), size, cache.getGcName()));
                } else {
                    gcCacheService.updateCoordinates(gcName, cache.getLat(), cache.getLon());
                }
            }
        });
    }

    private boolean checkCache(GcCache incomingCache, int size, GcCacheService gcCacheService, int currentSize, int totalSize) {
        String gcName = incomingCache.getGcName();

        GcCache neutralCache = gcCacheService.getCacheFromGcName(gcName);
        if (incomingCache.equalsCoordinates(neutralCache)) {

            log.info(String.format("[%d/%d] %d - Cache: %s coordinate not modified at all, nothing to do.", currentSize, totalSize, size, gcName));
            return false;
        }

        GcCache myCache = gcCacheService.getMyCacheFromGcName(gcName);
        if (incomingCache.equalsCoordinates(myCache)) {
            log.info(String.format("[%d/%d] %d - Cache: %s same coordinate than other, nothing to do.", currentSize, totalSize, size, gcName));
            return false;
        }

        log.info(String.format("[%d/%d] %d - Cache: %s coordinate modified, will add it.", currentSize, totalSize, size, gcName));
        return true;
    }
}
