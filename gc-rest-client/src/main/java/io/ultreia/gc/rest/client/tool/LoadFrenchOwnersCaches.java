package io.ultreia.gc.rest.client.tool;

/*-
 * #%L
 * GC toolkit :: REST Client
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.ultreia.gc.config.GcApiConfig;
import io.ultreia.gc.model.GcUser;
import io.ultreia.gc.service.api.GcCacheService;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 15/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LoadFrenchOwnersCaches extends GcRestClientToolSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LoadFrenchOwnersCaches.class);

    public void run(boolean reloadUsers) throws IOException {

        Path path = GcApiConfig.create().getDataDirectory().toPath().resolve("french-owners.json");

        Set<GcUser> users;

        if (Files.exists(path) && reloadUsers) {
            Files.delete(path);
        }

        if (Files.exists(path)) {

            try (BufferedReader reader = Files.newBufferedReader(path)) {
                users = new GsonBuilder().setPrettyPrinting().create().fromJson(reader, new TypeToken<Set<GcUser>>() {
                }.getType());
            }


        } else {
            users = new LoadFrenchOwners(applicationContext).runAndGet();
            try (BufferedWriter writer = Files.newBufferedWriter(path)) {
                new GsonBuilder().setPrettyPrinting().create().toJson(users, writer);
            }
        }

//        users = users.parallelStream().filter(u -> u.getCachesOwnedInFrance() >100).collect(Collectors.toSet());
//        users = users.parallelStream().filter(u -> u.getCachesOwnedInFrance() > 39).collect(Collectors.toSet());
//        users = users.parallelStream().filter(u -> u.getCachesOwnedInFrance() > 9 && u.getCachesOwnedInFrance() < 40).collect(Collectors.toSet());

        GcCacheService gcCacheService = applicationContext.newCacheService();

        int size = users.size();
        log.info(String.format("Found %s french owner(s).", size));

        MutableInt i = new MutableInt();

        List<GcUser> badUsers = new LinkedList<>();
        users.parallelStream().forEach(owner -> {

            String prefix = owner.getUserName();

            log.info(String.format("%S - [%d/%d] Loading caches (count: %d)", prefix, i.incrementAndGet(), size, owner.getCachesOwnedInFrance()));

            try {
                gcCacheService.loadFrenchOwnerCachesGcNames(owner);

            } catch (Exception e) {
                log.error(String.format("%S - [%d/%d] Error while loading caches....", prefix, i.getValue(), size));
                badUsers.add(owner);
            }

        });

        if (!badUsers.isEmpty()) {
            badUsers.forEach(b -> log.info("Could not get caches for user: " + b.getUserName()));
        }


    }
}
