package io.ultreia.gc.rest.client.tool;

/*-
 * #%L
 * GC toolkit :: REST Client
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.db.entity.GcCacheType;
import io.ultreia.gc.service.api.GcCacheService;
import java.io.IOException;
import java.util.Set;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 04/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReloadCaches extends GcRestClientToolSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReloadCaches.class);

    public void run(String gcCacheType, boolean dryRun) throws IOException {

//        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "20");

        GcCacheService cacheService = applicationContext.newCacheService();
        Set<String> gcNames = cacheService.getGcNames(GcCacheType.of(gcCacheType), true);

        int size = gcNames.size();
        log.info(String.format("Found %d mystery cache", size));

        MutableInt i = new MutableInt();
        gcNames.parallelStream().forEach(gcName ->
                                         {
                                             log.info(String.format("%d/%d - Reload cache %s.", i.incrementAndGet(), size, gcName));
                                             if (!dryRun) {
                                                 cacheService.reloadCache(gcName);
                                             }
                                         });

    }
}
