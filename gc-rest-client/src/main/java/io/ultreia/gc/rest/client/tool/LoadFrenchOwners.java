package io.ultreia.gc.rest.client.tool;

/*-
 * #%L
 * GC toolkit :: REST Client
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.model.GcUser;
import io.ultreia.gc.rest.client.GcRestClientApplicationContext;
import io.ultreia.gc.service.api.GcUserService;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 15/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LoadFrenchOwners extends GcRestClientToolSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LoadFrenchOwners.class);
    private Set<GcUser> frenchOwners;

    public LoadFrenchOwners(GcRestClientApplicationContext applicationContext) {
        super(applicationContext);
    }

    public Set<GcUser> runAndGet() {
        run();
        return frenchOwners;
    }

    public void run() {

        GcUserService userService = applicationContext.newUserService();

        frenchOwners = userService.getFrenchOwners(false);

        log.info(String.format("Found %s french owner(s).", frenchOwners.size()));

    }

}
