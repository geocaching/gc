package io.ultreia.gc.rest.client.v1;

/*
 * #%L
 * GC toolkit :: REST Client
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.rest.client.GcRequestBuilderFactory;
import io.ultreia.gc.rest.client.GcRestClientApplicationContext;
import io.ultreia.gc.service.api.GcServiceSupport;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HRequestBuilder;
import io.ultreia.java4all.http.HResponse;
import io.ultreia.java4all.http.HRestClientService;
import org.apache.commons.lang3.StringUtils;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class GcRestClientServiceSupport extends GcServiceSupport implements HRestClientService {

    @Override
    public GcRequestBuilderFactory getRequestBuilderFactory() {
        return getServiceContext().getRequestBuilderFactory();
    }

    @Override
    public GcRestClientApplicationContext getServiceContext() {
        return (GcRestClientApplicationContext) super.getServiceContext();
    }

    @Override
    public HRequestBuilder create(String baseUrl) {
        return getRequestBuilderFactory().create(StringUtils.removeEnd(getClass().getSimpleName(), "RestClient") + "/" + baseUrl);
    }

    @Override
    public HResponse executeRequest(HRequest request) {
        return getServiceContext().executeRequest(request);
    }

    @Override
    public HResponse executeRequest(HRequest request, int expectedStatusCode) {
        return getServiceContext().executeRequest(request, expectedStatusCode);
    }

}
