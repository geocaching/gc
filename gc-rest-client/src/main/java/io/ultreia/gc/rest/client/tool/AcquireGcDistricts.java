package io.ultreia.gc.rest.client.tool;

/*-
 * #%L
 * GC toolkit :: REST Client
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.GsonBuilder;
import io.ultreia.gc.db.entity.GcDistrict;
import io.ultreia.gc.service.api.GcCacheService;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 17/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class AcquireGcDistricts extends GcRestClientToolSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AcquireGcDistricts.class);

    private final String[] geonamesUsers = {
            "demo", "arktan44", "arktan45", "arktan46", "arktan47",
            "arktan50", "arktan51", "arktan42", "arktan43", "arktan52", "arktan53", "arktan54", "arktan55", "arktan56", "arktan57", "arktan58", "arktan59"

    };

    public void run() {

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                GcCacheService gcCacheService = applicationContext.newCacheService();

                Map<String, GcDistrict> result = new TreeMap<>();
                for (String geonamesUser : geonamesUsers) {

                    log.info("Try this user: " + geonamesUser);
                    try {
                        Map<String, GcDistrict> updateDistricts = gcCacheService.acquireDistricts(geonamesUser);
                        result.putAll(updateDistricts);
                        log.info(String.format("Update %d district(s) with user: %s", updateDistricts.size(), geonamesUser));
                    } catch (Exception e) {
                        // nothing to do continue
                    }
                }

                log.info(String.format("At %s, got %d district(s).", new Date(), result.size()));
                Path district = applicationContext.getConfig().getDataDirectory().toPath().resolve("district").resolve(new Date().getTime() + ".json");
                try {
                    if (!Files.exists(district.getParent())) {

                        Files.createDirectory(district.getParent());

                    }
                    log.info("Store district to " + district);
                    try (BufferedWriter bufferedWriter = Files.newBufferedWriter(district)) {
                        new GsonBuilder().setPrettyPrinting().create().toJson(result, bufferedWriter);
                    }
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }

            }
        };
        new Timer("update-districts").schedule(timerTask, 0, TimeUnit.MINUTES.toMillis(3));

    }

}
