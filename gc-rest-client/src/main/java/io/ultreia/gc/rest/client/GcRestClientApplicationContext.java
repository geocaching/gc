package io.ultreia.gc.rest.client;

/*
 * #%L
 * GC toolkit :: REST Client
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.model.GcCache;
import io.ultreia.gc.rest.client.v1.ClassMappingRestClient;
import io.ultreia.gc.service.api.GcService;
import io.ultreia.gc.service.internal.GcCacheServiceImpl;
import io.ultreia.gc.service.internal.GpxServiceImpl;
import io.ultreia.gc.ui.GcApplicationContextSupport;
import io.ultreia.java4all.http.HExecuteRequestException;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HResponse;
import io.ultreia.java4all.http.HResponseBuilder;
import java.awt.AWTException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcRestClientApplicationContext extends GcApplicationContextSupport<GcRestClientConfig> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcRestClientApplicationContext.class);

    private final HResponseBuilder responseBuilder = HResponseBuilder.create();
    private final GcRequestBuilderFactory requestBuilderFactory;

    private final ClassMappingRestClient classMapping = new ClassMappingRestClient();

    public GcRestClientApplicationContext(GcRestClientConfig config) {
        super(config);
        String apiUrl = config.getApiUrl().toExternalForm();
        if (!apiUrl.endsWith("/")) {
            apiUrl += "/";
        }
        requestBuilderFactory = new GcRequestBuilderFactory(() -> getAuthId().orElse(null), apiUrl, (int) TimeUnit.MINUTES.toMillis(60));
    }

    @Override
    public void close() {
        try {
            super.close();
        } finally {
            try {
                responseBuilder.close();
            } catch (IOException e) {
                log.error("Could not close response builder", e);
            }
        }

    }

    @Override
    public <S extends GcService> Class<S> getServiceClass(Class<S> serviceType) {

        if (!serviceType.isInterface() && !Modifier.isAbstract(serviceType.getModifiers())) {
            return serviceType;
        }
        return (Class) classMapping.getClass(serviceType);
    }

    public GcRequestBuilderFactory getRequestBuilderFactory() {
        return requestBuilderFactory;
    }

    public HResponse executeRequest(HRequest request, int expectedStatusCode) {
        try {
            return responseBuilder.executeRequest(request, expectedStatusCode);
        } catch (IOException e) {
            throw new HExecuteRequestException(e);
        }
    }

    public HResponse executeRequest(HRequest request) {
        try {
            return responseBuilder.executeRequest(request);
        } catch (IOException e) {
            throw new HExecuteRequestException(e);
        }
    }

    public static void main(String[] args) throws IOException, AWTException, IllegalAccessException, InstantiationException, InvocationTargetException {

//        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "20");

        GcRestClientConfig clientConfig = GcRestClientConfig.create(args);
        clientConfig.get().doAllAction();

//        GcRestClientApplicationContext applicationContext = new GcRestClientApplicationContext(clientConfig);
//        String authId = applicationContext.newLoginService().login(clientConfig.getLogin(), clientConfig.getPassword());
//        applicationContext.setAuthId(authId);
//        log.info("log in: " + authId);
//        GcSearchFilter filter = new GcSearchFilter();
//        filter.country = GcCountry.Madagascar.code();
//
//        GcLogService logService = applicationContext.newLogService();
//        Optional<GcArcheoLog> myArcheoLogFromGcName = logService.getMyArcheoLogFromGcName("GC60JWK", "akrtan");
//        log.info(myArcheoLogFromGcName);
//        Set<String> gcCacheNames = cacheService.searchCaches(filter);
//        gcCacheNames.removeAll(cacheService.getGcNames());
//        int size = gcCacheNames.size();
//        log.info("found " + size + " cache(s) to acquire.");
//
//        MutableInt i = new MutableInt();
//        gcCacheNames.parallelStream().forEach(c-> {
//
//            cacheService.getCacheFromGcName(c);
//            log.info(String.format("[%d,%d] Loaded %s", i.incrementAndGet(), size, c));
//        });

//        new LoadFrenchOwnersCaches(applicationContext, GcApiConfig.create(args).getDataDirectory().toPath().resolve("french-owners.json")).run();
//        new UpdateGcDistricts(applicationContext, "demo", "arktan44", "arktan45", "arktan46", "arktan47").run();

//        List<GcCache> gcCaches = new GcCacheServiceImpl().loadCaches(userConfig.getMyFindsJson().toFile());
//
//        GcChallengeResult check = new BonusChallengeChecker(gcCaches).check("");
//
//        log.info("\n"+check.getLog());

//        List<GcChallengeResult> challengeResults = ComputeChallengesResult.forState(applicationContext, GcState.France__Languedoc_Roussillon)
//                .compute(userConfig.getDataDirectory().toPath(), clientConfig.getLogin());
//
//        log.info("found " + challengeResults.size() + " challenge result(s).");
//        log.info("found " + challengeResults.stream().filter(GcChallengeResult::isSuccess).count() + " success challenge(s).");
//        log.info("found " + challengeResults.stream().filter(GcChallengeResult::isFailed).count() + " failed challenge(s).");
//        log.info("found " + challengeResults.stream().filter(GcChallengeResult::withError).count() + " with error challenge(s).");

//        Set<String> gcNames = gcCacheService.getGcNames();
//
//        Path gpxDirectory = config2.getDataDirectory().toPath().resolve("gpx");
//        Files.find(gpxDirectory, 1, (path, basicFileAttributes) -> path.toFile().getName().endsWith(".gpx")).forEach(gpxFile -> {
//
//
//            String prefix = gpxFile.toFile().getName();
//
//            log.info(String.format("%S - Loading gpx...", prefix));
//            List<GcCache> gcCaches = new GpxServiceImpl().getCaches(gpxFile.toFile());
//
//            log.info(String.format("%S - Found %d cache(s) to check", prefix, gcCaches.size()));
//            MutableInt i = new MutableInt();
//
//            List<GcCache> toAcquire = gcCaches.parallelStream().filter(gc -> !gcNames.contains(gc.getGcName())).collect(Collectors.toList());
//
//            toAcquire.sort(Comparator.comparing(GcCache::getGcName));
//            log.info(String.format("%S - Found %d cache(s) to acquire", prefix, toAcquire.size()));
//
//            toAcquire.parallelStream().forEach(gc -> {
//
//                try {
//                    gcCacheService.getCacheFromGcName(gc.getGcName());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                log.info(String.format("%S - Loaded %d/%d cache: %s", prefix, i.incrementAndGet(), toAcquire.size(), gc.getGcName()));
//
//            });
//
//        });


//        config2.setLogin("fafahakkai");
//
//        Path myFindsJson = config2.getMyFindsJson();
//        List<GcCache> gcCaches = new GcCacheServiceImpl().loadCaches(myFindsJson.toFile());


//        new LoadForState(applicationContext, GcStates.France__Pays_de_la_Loire).run();

//        gcCacheService.loadCountryStats(GcCountry.France);
//
//        for (GcState gcStates : GcState.values()) {
//            if (gcStates.name().startsWith("France__")) {
//                gcCacheService.loadStateStats(gcStates);
//            }
//        }


    }

    static List<GcCache> loadMyFinds(Path gpxPath, Path myDbPath) throws IOException {

        List<GcCache> caches;
        if (!Files.exists(myDbPath) || Files.getLastModifiedTime(myDbPath).compareTo(Files.getLastModifiedTime(gpxPath)) < 0) {
            log.info("Loading gpx " + gpxPath);
            caches = new GpxServiceImpl().getCaches(gpxPath.toFile());
            log.info("Load " + caches.size() + " cache(s).");

            new GcCacheServiceImpl().storeCaches(caches, myDbPath.toFile());
        } else {

            log.info("Loading json " + myDbPath);
            caches = new GcCacheServiceImpl().loadCaches(myDbPath.toFile());
            log.info("Load " + caches.size() + " cache(s).");
        }
        return caches;
    }

}
