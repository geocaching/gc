package io.ultreia.gc.rest.client;

/*-
 * #%L
 * GC toolkit :: REST Client
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.config.GcConfig;
import io.ultreia.gc.config.SecurityHelper;
import java.io.File;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ArgumentsParserException;

public class GcRestClientConfig extends GeneratedGcRestClientConfig implements GcConfig {

    /** Logger. */
    private static Log log = LogFactory.getLog(GcRestClientConfig.class);

    private void init(String... args) {

        if (log.isInfoEnabled()) {
            log.info("Starts to create Gc-Rest-client configuration...");
        }

        get().setConfigFileName(getConfigFile());
        try {
            get().parse(args);
        } catch (ArgumentsParserException e) {
            throw new IllegalStateException("could not parse configuration", e);
        }

        if (log.isInfoEnabled()) {
            log.info("Gc REST Client configuration create done.");
        }

    }


    public static GcRestClientConfig create(String... args) {
        GcRestClientConfig config = new GcRestClientConfig();
        config.get().loadActions(GcRestClientConfigAction.values());
        config.init(args);
        return config;
    }

    @Override
    public String getConfigFile() {
        return "gc.conf";
    }

    @Override
    public SecurityHelper getSecurityHelper() {
        return null;
    }

    public File getMyFindsGpxFile() {
        return  new File(getDataDirectory(), "myfinds-" + getLogin() + ".gpx");
    }
}
