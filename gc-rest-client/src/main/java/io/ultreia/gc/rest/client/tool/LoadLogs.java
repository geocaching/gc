package io.ultreia.gc.rest.client.tool;

/*-
 * #%L
 * GC toolkit :: REST Client
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.model.GcLog;
import io.ultreia.gc.service.api.GcCacheService;
import io.ultreia.gc.service.api.GcLogService;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 10/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LoadLogs extends GcRestClientToolSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LoadLogs.class);

    public void run() {

        MutableInt i = new MutableInt();

        GcCacheService cacheService = applicationContext.newCacheService();
        GcLogService logService = applicationContext.newLogService();

        List<String> gcNames = new LinkedList<>(cacheService.getGcNames());

        gcNames.sort(String::compareTo);
        Collections.reverse(gcNames);
        int size = gcNames.size();

        gcNames.parallelStream().forEach(gcName -> {

            try {
                log.info(String.format("%d/%d : %s", i.incrementAndGet(), size, gcName));
                List<GcLog> logsFromGcName = logService.getLogsFromGcName(gcName, true);
                log.info(String.format("%d/%d : %s - %d", i.getValue(), size, gcName, logsFromGcName.size()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
