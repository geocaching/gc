package io.ultreia.gc.rest.client.tool;

/*-
 * #%L
 * GC toolkit :: REST Client
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.rest.client.GcRestClientApplicationContext;
import io.ultreia.gc.rest.client.GcRestClientConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 18/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class GcRestClientToolSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcRestClientToolSupport.class);

    protected final GcRestClientApplicationContext applicationContext;

    GcRestClientToolSupport() {

        GcRestClientConfig clientConfig = GcRestClientConfig.create();

        applicationContext = new GcRestClientApplicationContext(clientConfig);

        String authId = applicationContext.newLoginService().login(clientConfig.getLogin(), clientConfig.getPassword());
        applicationContext.setAuthId(authId);

        log.info(String.format("log in: %s", authId));

    }

    GcRestClientToolSupport(GcRestClientApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
