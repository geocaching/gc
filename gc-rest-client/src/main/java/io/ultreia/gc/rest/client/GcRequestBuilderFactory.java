package io.ultreia.gc.rest.client;

/*-
 * #%L
 * GC toolkit :: REST Client
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.http.HRequestBuilder;
import io.ultreia.java4all.http.HRequestBuilderFactory;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * Created by tchemit on 16/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcRequestBuilderFactory implements HRequestBuilderFactory {

    private final Supplier<String> authTokenSupplier;
    private final String apiUrl;
    private final int defaultTimeOutInMiliSeconds;

    GcRequestBuilderFactory(Supplier<String> authTokenSupplier, String apiUrl, int defaultTimeOutInMiliSeconds) {
        this.authTokenSupplier = authTokenSupplier;
        this.apiUrl = apiUrl;
        this.defaultTimeOutInMiliSeconds = defaultTimeOutInMiliSeconds;
    }

    @Override
    public HRequestBuilder create(String baseUrl) {
        return new HRequestBuilder(apiUrl + baseUrl)
                .addAuthTokenSupplier(authTokenSupplier)
                .setTimeout(TimeUnit.MILLISECONDS, defaultTimeOutInMiliSeconds);
    }

}
