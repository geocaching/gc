package io.ultreia.gc.rest.client.tool;

/*-
 * #%L
 * GC toolkit :: REST Client
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.service.api.GcCacheService;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 17/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class UpdateElevations extends GcRestClientToolSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(UpdateElevations.class);

    private final String[] geonamesUsers = {
//            "demo",
//            "arktan44", "arktan45", "arktan46", "arktan47",
            "arktan50",
//            "arktan51", "arktan42", "arktan43", "arktan52", "arktan53", "arktan54", "arktan55", "arktan56", "arktan57", "arktan58", "arktan59"

    };

    public void run(String geonamesUser) {

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                GcCacheService gcCacheService = applicationContext.newCacheService();

                MutableInt result = new MutableInt();

                log.info("Try this user: " + geonamesUser);
                try {
                    int updateElevations = gcCacheService.updateElevations(geonamesUser);
                    result.add(updateElevations);
                    log.info(String.format("Update %d elevation(s) with user: %s", updateElevations, geonamesUser));
                } catch (Exception e) {
                    // nothing to do continue
                }

                log.info(String.format("At %s, got %d elevation(s).", new Date(), result.getValue()));
            }
        };
        timerTask.run();

//        new Timer("update-elevations").schedule(timerTask, 0, TimeUnit.MINUTES.toMillis(31));

    }

}
