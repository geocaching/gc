---
-- #%L
-- GC toolkit :: DB
-- %%
-- Copyright (C) 2017 Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
create extension postgis;
SELECT AddGeometryColumn('gccache', 'the_geom', 4326, 'POINT',2 );
UPDATE gccache SET the_geom=ST_SetSRID(ST_MakePoint(lon,lat), 4326);
CREATE OR REPLACE function sync_gccache_the_geom () returns trigger as '
BEGIN

  -- TODO : differencier si on est en creation ou update
  --  IF (TG_OP = ''UPDATE'') THEN ...

  IF (NEW.lat IS NULL OR NEW.lon IS NULL) THEN
    -- do not trigger if one of coordinate component is null

    RAISE NOTICE ''No latitude or longitude, can not compute postgis field for cache % '', NEW.gcName;
    return NEW;
  END IF;

  RAISE NOTICE ''Will compute the_geom for cache % - latitude % and longitude %'', NEW.gcName , NEW.lat, NEW.lon;

  -- affectation du point
  NEW.the_geom := ST_SetSRID(ST_MakePoint(NEW.lon,NEW.lat), 4326);

  RAISE NOTICE ''Computed for cache % latitude % and longitude %, the_geom %'', NEW.gcName, NEW.lat, NEW.lon, NEW.the_geom;

  RETURN NEW;
END
'
LANGUAGE 'plpgsql';

CREATE INDEX idx_gccache_gist ON gccache USING GIST (the_geom);

DROP TRIGGER IF EXISTS sync_gccache_the_geom ON gccache;
CREATE TRIGGER tr_sync_gccache_the_geom BEFORE insert or update ON gccache FOR EACH ROW EXECUTE PROCEDURE sync_gccache_the_geom();
