---
-- #%L
-- GC toolkit :: DB
-- %%
-- Copyright (C) 2017 Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0001',0, CURRENT_DATE, 'dogs', 1, 'Dogs');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0002',0, CURRENT_DATE, 'fee', 2, 'Access or Parking Fee');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0003',0, CURRENT_DATE, 'rapelling',3, 'Climbing Gear');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0004',0, CURRENT_DATE, 'boat', 4, 'Boat');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0005',0, CURRENT_DATE, 'scuba', 5, 'Scuba Gear');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0006',0, CURRENT_DATE, 'kids',6, 'Recommended for Kids');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0007',0, CURRENT_DATE, 'onehour', 7, 'Take Less Than an Hour');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0008',0, CURRENT_DATE, 'scenic', 8, 'Scenic View');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0009',0, CURRENT_DATE, 'hiking',9, 'Significant Hiking');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0010',0, CURRENT_DATE, 'climbing', 10, 'Difficult Climbing');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0011',0, CURRENT_DATE, 'wading', 11, 'May Require Wading');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0012',0, CURRENT_DATE, 'swimming',12, 'May Require Swimming');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0013',0, CURRENT_DATE, 'available', 13, 'Available at All Times');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0014',0, CURRENT_DATE, 'night', 14, 'Recommended at Night');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0015',0, CURRENT_DATE, 'winter',15, 'Available during Winter');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0016',0, CURRENT_DATE, 'cliff', 21, 'Cliff / Falling Rocks');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0017',0, CURRENT_DATE, 'hunting', 22, 'Hunting');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0018',0, CURRENT_DATE, 'danger',23, 'Dangerous Area');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0019',0, CURRENT_DATE, 'wheelchair', 24, 'Wheelchair Accessible');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0020',0, CURRENT_DATE, 'camping', 31, 'Camping Available');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0021',0, CURRENT_DATE, 'bicycles',32, 'Bicycles');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0022',0, CURRENT_DATE, 'motorcycles', 33, 'Motorcycles');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0023',0, CURRENT_DATE, 'quads', 34, 'Quads');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0024',0, CURRENT_DATE, 'jeeps',35, 'Off-Road Vehicles');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0025',0, CURRENT_DATE, 'snowmobiles', 36, 'Snowmobiles');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0026',0, CURRENT_DATE, 'campfires', 38, 'Campfires');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0027',0, CURRENT_DATE, 'poisonoak',17, 'Poisonous Plants');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0028',0, CURRENT_DATE, 'thorn', 39, 'Thorns');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0029',0, CURRENT_DATE, 'dangerousanimals', 18, 'Dangerous Animals');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0030',0, CURRENT_DATE, 'ticks',19, 'Ticks');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0031',0, CURRENT_DATE, 'mine', 20, 'Abandoned Mines');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0032',0, CURRENT_DATE, 'parking', 25, 'Parking Available');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0033',0, CURRENT_DATE, 'public',26, 'Public Transportation');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0034',0, CURRENT_DATE, 'picnic', 30, 'Picnic Tables Nearby');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0035',0, CURRENT_DATE, 'horses', 37, 'Horses');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0036',0, CURRENT_DATE, 'water',27, 'Drinking Water Nearby');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0037',0, CURRENT_DATE, 'restrooms', 28, 'Public Restrooms Nearby');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0038',0, CURRENT_DATE, 'phone', 29, 'Telephone Nearby');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0039',0, CURRENT_DATE, 'stroller',41, 'Stroller Accessible');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0040',0, CURRENT_DATE, 'firstaid', 42, 'Need Maintenance');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0041',0, CURRENT_DATE, 'cow', 43, 'Watch for Livestock');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0042',0, CURRENT_DATE, 'stealth',40, 'Stealth Required');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0043',0, CURRENT_DATE, 'landf', 45, 'Lost and Found Tour');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0044',0, CURRENT_DATE, 'flashlight', 44, 'Flashlight Required');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0045',0, CURRENT_DATE, 'rv',46, 'Truck Driver/RV');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0046',0, CURRENT_DATE, 'UV', 48, 'UV Light Required');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0047',0, CURRENT_DATE, 'snowshoes', 49, 'snowshoes');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0048',0, CURRENT_DATE, 'skiis',50, 'Cross Country Skis');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0049',0, CURRENT_DATE, 's-tool', 51, 'Special Tool Required');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0050',0, CURRENT_DATE, 'nightcache', 52, 'Night Cache');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0051',0, CURRENT_DATE, 'prkngrab',53, 'Park and Grab');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0052',0, CURRENT_DATE, 'AbandonedBuilding', 54, 'Abandoned Structure');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0053',0, CURRENT_DATE, 'hike_short', 55, 'Short hike (Less than 1km)');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0054',0, CURRENT_DATE, 'hike_med',56, 'Medium Hike (1km-10km)');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0055',0, CURRENT_DATE, 'hike_long', 57, 'Long Hike (+10km)');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0056',0, CURRENT_DATE, 'fuel', 58, 'Fuel Nearby');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0057',0, CURRENT_DATE, 'food',59, 'Food Nearby');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0058',0, CURRENT_DATE, 'wirelessbeacon', 60, 'Wireless Beacon');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0059',0, CURRENT_DATE, 'partnership', 61, 'Partnership Cache');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0060',0, CURRENT_DATE, 'field_puzzle',47, 'Field Puzzle');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0061',0, CURRENT_DATE, 'seasonal', 62, 'Seasonal Access');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0062',0, CURRENT_DATE, 'touristOK', 63, 'Tourist Friendly');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0063',0, CURRENT_DATE, 'treeclimbing',64, 'Tree Climbing');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0064',0, CURRENT_DATE, 'frontyard', 65, 'Front Yard (Private Residence)');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0065',0, CURRENT_DATE, 'teamwork', 66, 'Teamwork Required');
INSERT INTO GcAttribute (topiaid, topiaversion, topiacreatedate, id, code, text) VALUES ('io.ultreia.gc.db.entity.GcAttribute#0#0066',0, CURRENT_DATE, 'geotour',67, 'GeoTour');
