---
-- #%L
-- GC toolkit :: DB
-- %%
-- Copyright (C) 2017 Ultreia.io
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
DROP TABLE d;
create table GcDistrict ( id varchar(100) not null constraint gcdistrict_pkey primary key, state varchar(100) not null , name varchar(100) not null ) ;
alter TABLE d add CONSTRAINT u_gcdistrict_name UNIQUE(name);
alter table d add CONSTRAINT fk_gcdistrict_state FOREIGN KEY (state) REFERENCES gcstate(id);

INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_01', 'Ain');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_02', 'Aisne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_03', 'Allier');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_04', 'Alpes-de-Haute-Provence');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_05', 'Hautes-Alpes');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_06', 'Alpes-Maritimes');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_07', 'Ardèche');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_08', 'Ardennes');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_09', 'Ariège');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_10', 'Aube');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_11', 'Aude');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_12', 'Aveyron');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_13', 'Bouches-du-Rhône');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_14', 'Calvados');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_15', 'Cantal');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_16', 'Charente');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_17', 'Charente-Maritime');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_18', 'Cher');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_19', 'Corrèze');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_21', 'Côte-d''Or');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_22', 'Côtes-d''Armor');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_23', 'Creuse');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_24', 'Dordogne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_25', 'Doubs');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_26', 'Drôme');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_27', 'Eure');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_28', 'Eure-et-Loir');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_29', 'Finistère');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_30', 'Gard');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_31', 'Haute-Garonne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_32', 'Gers');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_33', 'Gironde');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_34', 'Hérault');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_35', 'Ille-et-Vilaine');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_36', 'Indre');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_37', 'Indre-et-Loire');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_38', 'Isère');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_39', 'Jura');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_40', 'Landes');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_41', 'Loir-et-Cher');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_42', 'Loire');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_43', 'Haute-Loire');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_44', 'Loire-Atlantique');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_45', 'Loiret');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_46', 'Lot');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_47', 'Lot-et-Garonne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_48', 'Lozère');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_49', 'Maine-et-Loire');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_50', 'Manche');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_51', 'Marne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_52', 'Haute-Marne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_53', 'Mayenne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_54', 'Meurthe-et-Moselle');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_55', 'Meuse');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_56', 'Morbihan');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_57', 'Moselle');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_58', 'Nièvre');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_59', 'Nord');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_60', 'Oise');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_61', 'Orne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_62', 'Pas-de-Calais');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_63', 'Puy-de-Dôme');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_64', 'Pyrénées-Atlantiques');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_65', 'Hautes-Pyrénées');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_66', 'Pyrénées-Orientales');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_67', 'Bas-Rhin');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_68', 'Haut-Rhin');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_69', 'Rhône');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_70', 'Haute-Saône');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_71', 'Saône-et-Loire');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_72', 'Sarthe');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_73', 'Savoie');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_74', 'Haute-Savoie');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_75', 'Paris');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_76', 'Seine-Maritime');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_77', 'Seine-et-Marne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_78', 'Yvelines');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_79', 'Deux-Sèvres');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_80', 'Somme');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_81', 'Tarn');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_82', 'Tarn-et-Garonne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_83', 'Var');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_84', 'Vaucluse');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_85', 'Vendée');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_86', 'Vienne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_87', 'Haute-Vienne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_88', 'Vosges');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_89', 'Yonne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_90', 'Territoire de Belfort');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_91', 'Essonne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_92', 'Hauts-de-Seine');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_93', 'Seine-Saint-Denis');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_94', 'Val-de-Marne');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_95', 'Val-d''Oise');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_971', 'Guadeloupe');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_972', 'Martinique');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_973', 'Guyane');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_974', 'La Réunion');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_975', 'Saint-Pierre-et-Miquelon');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_976', 'Mayotte');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_2A', 'Corse-du-Sud');
INSERT INTO d (state, id,  name) VALUES ('France__Pays_de_la_Loire', 'FR_2B', 'Haute Corse');
