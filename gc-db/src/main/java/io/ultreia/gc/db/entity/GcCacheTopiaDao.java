package io.ultreia.gc.db.entity;

/*-
 * #%L
 * GC toolkit :: DB
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;

public class GcCacheTopiaDao extends AbstractGcCacheTopiaDao<GcCache> {

    public Set<String> findAllGcNames() {
        return new LinkedHashSet<>(topiaSqlSupport.findMultipleResult(new TopiaSqlQuery<String>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("select gcname from gccache order by gcname");
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        }));
    }

    public Set<String> findAllGcNames(String cacheType, boolean nullLastUpdate) {
        return new LinkedHashSet<>(topiaSqlSupport.findMultipleResult(new TopiaSqlQuery<String>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement(String.format("select gcname from gccache where gctype='%s' "+(nullLastUpdate?" and lastUpdate is null":"")+" order by gcname", cacheType));
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        }));
    }

    public Set<String> findAllFrenchGcNames() {
        return new LinkedHashSet<>(topiaSqlSupport.findMultipleResult(new TopiaSqlQuery<String>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("select gcname from gccache where country='France' order by gcname");
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        }));
    }

    public Set<String> findAllGcNamesWithoutLogs() {
        return new LinkedHashSet<>(topiaSqlSupport.findMultipleResult(new TopiaSqlQuery<String>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("select gcname from gccache where logsjson is null order by gcname");
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        }));
    }

    public void updateDistricts(Map<String, GcDistrict> result) {
        StringBuilder sqlBuilder = new StringBuilder();
        for (Map.Entry<String, GcDistrict> entry : result.entrySet()) {
            sqlBuilder.append(String.format("update gccache set district = '%s' where topiaid ='%s';\n", entry.getValue().name(), entry.getKey()));
        }
        topiaSqlSupport.executeSql(sqlBuilder.toString());
    }

    public void updateElevations(Map<String, Integer> result) {
        StringBuilder sqlBuilder = new StringBuilder();
        for (Map.Entry<String, Integer> entry : result.entrySet()) {
            sqlBuilder.append(String.format("update gccache set elevation = %d where topiaid ='%s';\n", entry.getValue(), entry.getKey()));
        }
        topiaSqlSupport.executeSql(sqlBuilder.toString());
    }

    public Set<GcCacheRefWithCoordinates> findFrenchGcNamesWithoutDistrict() {
        return new LinkedHashSet<>(topiaSqlSupport.findMultipleResult(new TopiaSqlQuery<GcCacheRefWithCoordinates>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("select topiaid, gcname, lat, lon from gccache where district is null and country = 'France' order by gcname");
            }

            @Override
            public GcCacheRefWithCoordinates prepareResult(ResultSet set) throws SQLException {

                return new GcCacheRefWithCoordinates(
                        set.getString(1),
                        set.getString(2),
                        set.getDouble(3),
                        set.getDouble(4)
                );
            }
        }));
    }

    public Set<GcCacheRefWithCoordinates> findGcNamesWithoutElevation() {
        return new LinkedHashSet<>(topiaSqlSupport.findMultipleResult(new TopiaSqlQuery<GcCacheRefWithCoordinates>() {
            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("select topiaid, gcname, lat, lon from gccache where elevation is null order by gcname");
            }

            @Override
            public GcCacheRefWithCoordinates prepareResult(ResultSet set) throws SQLException {

                return new GcCacheRefWithCoordinates(
                        set.getString(1),
                        set.getString(2),
                        set.getDouble(3),
                        set.getDouble(4)
                );
            }
        }));
    }
}
