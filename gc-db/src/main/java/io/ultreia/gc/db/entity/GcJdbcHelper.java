package io.ultreia.gc.db.entity;

/*-
 * #%L
 * GC toolkit :: DB
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import org.nuiton.topia.persistence.jdbc.JdbcConfiguration;
import org.nuiton.topia.persistence.jdbc.JdbcHelper;
import org.nuiton.util.sql.SqlFileReader;

/**
 * Created by tchemit on 03/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcJdbcHelper extends JdbcHelper {

    public static final int BUZZER_SIZE = 100;

    GcJdbcHelper(JdbcConfiguration jdbcConfiguration) {
        super(jdbcConfiguration);
    }

    public void executeSql(File file) {

        try (BufferedReader bufferedReader = Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8)) {
            executeSql(bufferedReader);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public void executeSql(InputStream file) {

        try (InputStreamReader bufferedReader = new InputStreamReader(file, StandardCharsets.UTF_8)) {
            executeSql(bufferedReader);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public void executeSql(Reader reader) {

        try (Connection connection = openConnection()) {
            try (Statement preparedStatement = connection.createStatement()) {

                int statementCount = 0;
                StringBuilder currentStatement = null;
                for (String sqlStatement : new SqlFileReader(reader)) {
                    if (sqlStatement.startsWith("--")) {
                        continue;
                    }
                    if (sqlStatement.trim().endsWith(";")) {
                        if (currentStatement == null) {
                            preparedStatement.addBatch(sqlStatement);
                        } else {
                            currentStatement.append("\n").append(sqlStatement);
                            preparedStatement.addBatch(currentStatement.toString());
                            currentStatement = null;
                        }
                        statementCount++;
                    } else {
                        if (currentStatement == null) {
                            currentStatement = new StringBuilder(sqlStatement);
                        } else {
                            currentStatement.append("\n").append(sqlStatement);
                        }
                    }

                    if (statementCount % BUZZER_SIZE == 0) {
                        preparedStatement.executeBatch();
                        preparedStatement.clearBatch();
                    }
                }
                preparedStatement.executeBatch();
                preparedStatement.clearBatch();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }

}
