package io.ultreia.gc.db.entity;

/*-
 * #%L
 * GC toolkit :: DB
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by tchemit on 04/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcCacheLogAdapter implements JsonDeserializer<GcCacheLog>, JsonSerializer<GcCacheLog> {

    private static final SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyy");

    @Override
    public GcCacheLog deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        GcCacheLog result = new GcCacheLog();
        JsonObject asJsonObject = json.getAsJsonObject();
        result.setLogId(asJsonObject.get("id").getAsInt());
        result.setLogGuid(asJsonObject.get("guid").getAsString());
        result.setLogText(asJsonObject.get("text").getAsString());
        result.setLogType(asJsonObject.get("type").getAsString());
        result.setUser(asJsonObject.get("user").getAsString());
        try {
            result.setLogDate(df.parse(asJsonObject.get("date").getAsString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public JsonElement serialize(GcCacheLog src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();
        result.add("id", context.serialize(src.getLogId()));
        result.add("guid", context.serialize(src.getLogGuid()));
        result.add("text", context.serialize(src.getLogText()));
        result.add("type", context.serialize(src.getLogType()));
        result.add("user", context.serialize(src.getUser()));
        result.add("date", context.serialize(df.format(src.getLogDate())));
        return result;
    }
}
