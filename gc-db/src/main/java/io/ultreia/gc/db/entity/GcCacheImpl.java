package io.ultreia.gc.db.entity;

/*-
 * #%L
 * GC toolkit :: DB
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Created by tchemit on 04/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcCacheImpl extends GcCacheAbstract {

    private static final Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(GcCacheLog.class, new GcCacheLogAdapter()).create();
    private static final Type type = new TypeToken<List<GcCacheLog>>() {
    }.getType();

    private List<GcCacheLog> logs;

    @Override
    public List<GcCacheLog> getLogs() {
        if (logs == null) {
            if (getLogsJson() == null) {
                return Collections.emptyList();
            }
            synchronized (gson) {
                return gson.fromJson(getLogsJson(), type);
            }
        }
        return logs;
    }

    @Override
    public void setLogs(List<GcCacheLog> logs) {
        synchronized (gson) {
            this.logs = logs;
            setLogsJson(gson.toJson(logs));
        }
    }
}
