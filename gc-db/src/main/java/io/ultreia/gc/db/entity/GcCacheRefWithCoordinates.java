package io.ultreia.gc.db.entity;

/*-
 * #%L
 * GC toolkit :: DB
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

/**
 * Created by tchemit on 20/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcCacheRefWithCoordinates {

    private final String id;
    private final String gcName;
    private final double lat;
    private final double lng;
    private GcDistrict district;
    private Integer elevation;

    public GcCacheRefWithCoordinates(GcCache entity) {
        this(entity.getTopiaId(), entity.getGcName(), entity.getLat(), entity.getLon());
    }

    public GcCacheRefWithCoordinates(String id, String gcName, double lat, double lng) {
        this.id = id;
        this.gcName = gcName;
        this.lat = lat;
        this.lng = lng;
    }

    public String getId() {
        return id;
    }

    public String getGcName() {
        return gcName;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public GcDistrict getDistrict() {
        return district;
    }

    public void setDistrict(GcDistrict district) {
        this.district = district;
    }

    public Integer getElevation() {
        return elevation;
    }

    public void setElevation(Integer elevation) {
        this.elevation = elevation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GcCacheRefWithCoordinates that = (GcCacheRefWithCoordinates) o;
        return Objects.equals(gcName, that.gcName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gcName);
    }
}
