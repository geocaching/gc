package io.ultreia.gc.db.entity;

/*-
 * #%L
 * GC toolkit :: DB
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GcUserTopiaDao extends AbstractGcUserTopiaDao<GcUser> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcUserTopiaDao.class);

    public void consolidate(GcUser gcUser) {

        List<GcUser> gcUsers = forNameEquals(gcUser.getName()).findAll();

        gcUsers.addAll(forUserGuidEquals(gcUser.getUserGuid()).findAll());
        gcUsers.addAll(forUserGuidEquals(gcUser.getUserId() + "").findAll());
        gcUsers.addAll(forUserIdEquals(gcUser.getUserId()).findAll());

        String gcUserTopiaId = gcUser.getTopiaId();
        gcUsers.removeIf(u -> u.getTopiaId().equals(gcUserTopiaId));
        gcUsers.removeIf(u -> u.getUserGuid() != null && u.getUserId() > 0);

        log.info(String.format("Found %d user(s) to remove (user: %s - %s - %s)", gcUsers.size(), gcUser.getName(), gcUser.getUserId(), gcUser.getUserGuid()));

        for (GcUser user : gcUsers) {

            log.info(String.format("Replace obsolete user: %s - %d - %s", user.getName(), user.getUserId(), user.getUserGuid()));

            topiaSqlSupport.executeSql(String.format("update gcCache set owner = '%s' where owner='%s'", gcUserTopiaId, user.getTopiaId()));

            log.info(String.format("Delete obsolete user: %s - %d - %s", user.getName(), user.getUserId(), user.getUserGuid()));
            delete(user);

        }
    }
}
