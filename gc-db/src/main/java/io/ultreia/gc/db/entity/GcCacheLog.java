package io.ultreia.gc.db.entity;

/*-
 * #%L
 * GC toolkit :: DB
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tchemit on 04/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcCacheLog {

    private int logId;
    private String logGuid;
    private String logText;
    private String logType;
    private Date logDate;

    private static final long serialVersionUID = 1L;
    private String user;
    private String userId;

    public void setLogId(int logId) {
        this.logId = logId;
    }


    public int getLogId() {
        return this.logId;
    }


    public void setLogGuid(String logGuid) {
        this.logGuid = logGuid;
    }


    public String getLogGuid() {
        return this.logGuid;
    }


    public void setLogText(String logText) {
        this.logText = logText;
    }


    public String getLogText() {
        return this.logText;
    }


    public void setLogType(String logType) {
        this.logType = logType;
    }


    public String getLogType() {
        return this.logType;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    public Date getLogDate() {
        return this.logDate;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
