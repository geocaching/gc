package io.ultreia.gc.db.entity;

/*-
 * #%L
 * GC toolkit :: DB
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created by tchemit on 15/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface GcReferential {

    String name();

    String compactName();

    int code();


    static <R extends GcReferential> R ofCompact(String compactName, R... values) {
        for (R value : values) {
            if (value.compactName().equalsIgnoreCase(compactName)) {
                return value;
            }
        }
        throw new IllegalStateException(String.format("Can't find referential for compact name: %s", compactName));
    }
}
