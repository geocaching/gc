package io.ultreia.gc.db.entity;

/*-
 * #%L
 * GC toolkit :: DB
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by tchemit on 10/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum GcTerrain {
    T1(1),
    T1_5(1.5f),
    T2(2),
    T2_5(2.5f),
    T3(3),
    T3_5(3.5f),
    T4(4),
    T4_5(4.5f),
    T5(5);

    private final float code;

    GcTerrain(float code) {
        this.code = code;
    }

    public float getCode() {
        return code;
    }

    @Override
    public String toString() {
        return name().substring(1).replace("_", ".");
    }

    public static GcTerrain valueOf(float code) {
        for (GcTerrain gcTerrain : values()) {
            if (code == gcTerrain.code) {
                return gcTerrain;
            }
        }
        throw new IllegalStateException("Can't find terrain with code: " + code);
    }

    public static List<GcTerrain> reverseValues() {
        List<GcTerrain> result = Arrays.asList(values());
        Collections.reverse(result);
        return result;
    }
}
