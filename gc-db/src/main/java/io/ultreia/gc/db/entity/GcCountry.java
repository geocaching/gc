package io.ultreia.gc.db.entity;

/*-
 * #%L
 * GC toolkit :: DB
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created by tchemit on 26/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum GcCountry implements GcReferential {

    Zambia(224),
    Yemen(220),
    Western_Sahara(271),
    Wallis_And_Futuna_Islands(218),
    Vietnam(215),
    Venezuela(214),
    Vatican_City_State(213),
    Vanuatu(212),
    Uzbekistan(211),
    US_Virgin_Islands(235),
    US_Minor_Outlying_Islands(270),
    Uruguay(210),
    United_States(2),
    United_Kingdom(11),
    United_Arab_Emirates(206),
    Ukraine(207),
    Uganda(208),
    Tuvalu(205),
    Turks_and_Caicos_Islands(197),
    Turkmenistan(199),
    Turkey(204),
    Tunisia(203),
    Trinidad_and_Tobago(202),
    Tonga(201),
    Tokelau(269),
    Togo(200),
    Thailand(198),
    Tanzania(196),
    Tajikistan(195),
    Taiwan(194),
    Syria(193),
    Switzerland(192),
    Sweden(10),
    Swaziland(190),
    Svalbard_and_Jan_Mayen(268),
    Suriname(189),
    Sudan(188),
    St__Martin(174),
    St_Vince_Grenadines(177),
    St_Pierre_Miquelon(175),
    St_Kitts(172),
    St_Eustatius(170),
    St_Barthelemy(169),
    Sri_Lanka(187),
    Spain(186),
    South_Sudan(278),
    South_Korea(180),
    South_Georgia_and_Sandwich_Islands(267),
    South_Africa(165),
    Somalia(185),
    Solomon_Islands(184),
    Slovenia(181),
    Slovakia(182),
    Singapore(179),
    Sierra_Leone(178),
    Seychelles(168),
    Serbia(222),
    Senegal(167),
    Saudi_Arabia(166),
    Sao_Tome_and_Principe(176),
    San_Marino(183),
    Samoa(217),
    Saint_Lucia(173),
    Saint_Kitts_and_Nevis(264),
    Saint_Helena(171),
    Saba(277),
    Rwanda(164),
    Russia(163),
    Romania(162),
    Reunion(161),
    Qatar(160),
    Puerto_Rico(226),
    Portugal(159),
    Poland(158),
    Pitcairn_Islands(155),
    Philippines(154),
    Peru(153),
    Paraguay(262),
    Papua_New_Guinea(156),
    Panama(152),
    Palestine(276),
    Palau(261),
    Pakistan(151),
    Oman(150),
    Norway(147),
    Northern_Mariana_Islands(236),
    North_Korea(146),
    Norfolk_Island(260),
    Niue(149),
    Nigeria(145),
    Niger(143),
    Nicaragua(144),
    New_Zealand(9),
    New_Caledonia(41),
    Nevis_and_St_Kitts(142),
    Netherlands_Antilles(148),
    Netherlands(141),
    Nepal(140),
    Nauru(138),
    Namibia(137),
    Myanmar(136),
    Mozambique(133),
    Morocco(132),
    Montserrat(135),
    Montenegro(274),
    Mongolia(131),
    Monaco(130),
    Moldova(237),
    Micronesia(242),
    Mexico(228),
    Mayotte(259),
    Mauritius(134),
    Mauritania(123),
    Martinique(122),
    Marshall_Islands(240),
    Malta(128),
    Mali(127),
    Maldives(124),
    Malaysia(121),
    Malawi(129),
    Madagascar(119),
    Macedonia(125),
    Macau(258),
    Luxembourg(8),
    Lithuania(117),
    Liechtenstein(116),
    Libya(112),
    Liberia(115),
    Lesotho(114),
    Lebanon(113),
    Latvia(111),
    Laos(110),
    Kyrgyzstan(108),
    Kuwait(241),
    Kiribati(109),
    Kenya(107),
    Kazakhstan(106),
    Jordan(103),
    Jersey(102),
    Japan(104),
    Jamaica(101),
    Ivory_Coast(100),
    Italy(99),
    Israel(98),
    Isle_of_Man(243),
    Ireland(7),
    Iraq(97),
    Iran(96),
    Indonesia(95),
    India(94),
    Iceland(93),
    Hungary(92),
    Hong_Kong(91),
    Honduras(90),
    Heard_Island_And_Mcdonald_Islands(256),
    Haiti(89),
    Guyana(87),
    Guinea_Bissau(85),
    Guinea(255),
    Guernsey(86),
    Guatemala(84),
    Guam(229),
    Guadeloupe(77),
    Grenada(81),
    Greenland(83),
    Greece(82),
    Gibraltar(80),
    Ghana(254),
    Germany(79),
    Georgia(78),
    Gambia(76),
    Gabon(75),
    French_Southern_Territories(253),
    French_Polynesia(74),
    French_Guiana(70),
    France(73),
    Finland(72),
    Fiji(71),
    Faroe_Islands(68),
    Falkland_Islands(69),
    Ethiopia(67),
    Estonia(66),
    Eritrea(65),
    Equatorial_Guinea(62),
    El_Salvador(64),
    Egypt(63),
    Ecuador(61),
    East_Timor(252),
    Dominican_Republic(60),
    Dominica(59),
    Djibouti(58),
    Denmark(57),
    Democratic_Republic_of_the_Congo(257),
    Czech_Republic(56),
    Cyprus(55),
    Curacao(54),
    Cuba(238),
    Croatia(53),
    Costa_Rica(52),
    Cook_Islands(48),
    Congo(51),
    Comoros(50),
    Colombia(49),
    Cocos__Keeling__Islands(251),
    Christmas_Island(250),
    China(47),
    Chile(6),
    Chad(249),
    Central_African_Republic(46),
    Cayman_Islands(44),
    Cape_Verde(239),
    Canada(5),
    Cameroon(43),
    Cambodia(42),
    Burundi(35),
    Burkina_Faso(216),
    Bulgaria(37),
    Brunei(36),
    British_Virgin_Islands(39),
    British_Indian_Ocean_Territories(248),
    Brazil(34),
    Bouvet_Island(247),
    Botswana(33),
    Bosnia_and_Herzegovina(234),
    Bonaire(275),
    Bolivia(32),
    Bhutan(30),
    Bermuda(27),
    Benin(26),
    Belize(31),
    Belgium(4),
    Belarus(40),
    Barbados(25),
    Bangladesh(24),
    Bahrain(29),
    Bahamas(23),
    Azerbaijan(21),
    Austria(227),
    Australia(3),
    Aruba(20),
    Armenia(15),
    Argentina(19),
    Antigua_and_Barbuda(13),
    Antarctica(18),
    Anguilla(246),
    Angola(17),
    Andorra(16),
    American_Samoa(245),
    Algeria(14),
    Albania(244),
    Aland_Islands(272),
    Afghanistan(12);

    private final int code;
    private final String compactName;

    GcCountry(int code) {
        this.code = code;
        this.compactName = name().replaceAll("_", "");
    }

    @Override
    public String compactName() {
        return compactName;
    }

    @Override
    public int code() {
        return code;
    }

    public static GcCountry ofCompact(String compactName) {
        return GcReferential.ofCompact(compactName, values());
    }

    public static GcCountry of(String name) {
        name = name.trim().replaceAll("\\s", "_");
        for (GcCountry gcCountries : values()) {
            if (gcCountries.name().equalsIgnoreCase(name)) {
                return gcCountries;
            }
        }
        throw new IllegalStateException(String.format("Can't find GcCountry for name: %s", name));
    }

    public static GcCountry of(int code) {
        for (GcCountry gcCountries : values()) {
            if (gcCountries.code() == code) {
                return gcCountries;
            }
        }
        throw new IllegalStateException(String.format("Can't find GcCountry for code: %s", code));
    }

    @Override
    public String toString() {
        return name().replaceAll("_", " ");
    }
}
