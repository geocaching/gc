package io.ultreia.gc.db.entity;

/*-
 * #%L
 * GC toolkit :: DB
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.db.GcDbConfig;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.hikaricp.internal.HikariCPConnectionProvider;
import org.nuiton.topia.persistence.BeanTopiaConfiguration;
import org.nuiton.topia.persistence.HibernateAvailableSettings;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.internal.LegacyTopiaIdFactory;
import org.nuiton.topia.persistence.jdbc.JdbcConfiguration;
import org.nuiton.topia.persistence.jdbc.JdbcConfigurationBuilder;

public class GcDbTopiaApplicationContext extends AbstractGcDbTopiaApplicationContext {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcDbTopiaApplicationContext.class);

    public static synchronized GcDbTopiaApplicationContext create(String classifier, String... args) {

        log.info(String.format("Create db application context (classifier: %s), (args: %s)", classifier, Arrays.toString(args)));

        GcDbConfig config = new GcDbConfig(classifier, args);

        JdbcConfiguration jdbcConfiguration = new JdbcConfigurationBuilder().forDatabase(config.getJdbcUrl(), config.getJdbcLogin(), config.getJdbcPassword());
        BeanTopiaConfiguration topiaConfiguration = new BeanTopiaConfiguration(jdbcConfiguration);
        topiaConfiguration.setTopiaIdFactoryClass(LegacyTopiaIdFactory.class);
        topiaConfiguration.setValidateSchema(false);
        topiaConfiguration.setInitSchema(false);

        Map<String, String> hibernateExtraConfiguration = topiaConfiguration.getHibernateExtraConfiguration();

        hibernateExtraConfiguration.put(HibernateAvailableSettings.CONNECTION_PROVIDER, HikariCPConnectionProvider.class.getName());
        hibernateExtraConfiguration.put("hibernate.hikari.minimumIdle", "" + 2);
        hibernateExtraConfiguration.put("hibernate.hikari.maximumPoolSize", "" + 100);
        hibernateExtraConfiguration.put("hibernate.hikari.autoCommit", "" + false);
        hibernateExtraConfiguration.put("hibernate.hikari.registerMbeans", "" + true);
//        hibernateExtraConfiguration.put(HibernateAvailableSettings.HBM2DDL_AUTO, "none");
        return new GcDbTopiaApplicationContext(topiaConfiguration, config);
    }

    private final GcJdbcHelper gcJdbcHelper;
    private final GcDbConfig config;

    @Override
    public void createSchema() {

        log.info("Create schema...");
        super.createSchema();

        log.info("Loading countries...");
        loadClassPathScript("/db/01_country.sql");

        log.info("Loading states...");
        loadClassPathScript("/db/02_state.sql");

        log.info("Loading attributes...");
        loadClassPathScript("/db/03_attribute.sql");

        log.info("Loading districts...");
        loadClassPathScript("/db/04_district.sql");

        log.info("Create postgis...");
        loadClassPathScript("/db/05_cache_postgis.sql");
        loadClassPathScript("/db/06_district_postgis.sql");

    }

    public GcDbTopiaApplicationContext(TopiaConfiguration topiaConfiguration, GcDbConfig config) {
        super(topiaConfiguration);
        this.config = config;
        this.gcJdbcHelper = new GcJdbcHelper(getConfiguration());
    }

    public GcDbConfig getConfig() {
        return config;
    }

    public GcJdbcHelper getGcJdbcHelper() {
        return gcJdbcHelper;
    }

    private void loadClassPathScript(String resourcePath) {

        try (InputStream inputStream = getClass().getResourceAsStream(resourcePath)) {
            getGcJdbcHelper().executeSql(inputStream);
        } catch (IOException e) {
            throw new TopiaException("Can't load class-path script: " + resourcePath, e);
        }

    }

//    protected static <E extends TopiaEntity> void replicate(String name, GcDbTopiaPersistenceContext persistenceContext, GcDbTopiaPersistenceContext targetPersistenceContext, List<E> entities) {
//
//        log.info(String.format("Will replicate %s %s(s).", entities.size(), name));
//        int i = 0;
//        for (E entity : entities) {
//
//            persistenceContext.replicateEntity(targetPersistenceContext, entity);
//            if (i++ % 100 == 0) {
//                targetPersistenceContext.commit();
//            }
//        }
//        targetPersistenceContext.commit();
//    }
}
