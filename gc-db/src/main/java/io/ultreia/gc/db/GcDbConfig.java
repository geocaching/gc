package io.ultreia.gc.db;

/*-
 * #%L
 * GC toolkit :: DB
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfigInit;
import org.nuiton.config.ApplicationConfigScope;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.topia.persistence.TopiaException;

public class GcDbConfig extends GeneratedGcDbConfig {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcDbConfig.class);

    public GcDbConfig(String classifier, String... args) {
        log.info(String.format("Create db config (classifier: %s) (args: %s)", classifier, Arrays.toString(args)));
        get().setDefaultOption(GcDbConfigOption.CLASSIFIER.getKey(), classifier);

        ApplicationConfig2 defaultConf = new ApplicationConfig2(ApplicationConfigInit.forScopes(ApplicationConfigScope.CLASS_PATH, ApplicationConfigScope.DEFAULTS, ApplicationConfigScope.OPTIONS).setConfigFileName("gc-db.conf"));
        try {
            defaultConf.parse(args);
        } catch (ArgumentsParserException e) {
            throw new TopiaException(e);
        }
        Properties properties = defaultConf.getProperties(ApplicationConfigScope.CLASS_PATH);
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            get().setDefaultOption("" + entry.getKey(), "" + entry.getValue());
        }

        log.info(String.format("Use %s configuration file", getConfigFile()));
        get().setConfigFileName(getConfigFile());
        try {
            get().parse(args);
        } catch (ArgumentsParserException e) {
            throw new TopiaException(e);
        }
        log.info(String.format("db config is initialized:\n%s", getConfigurationDescription()));
    }

}
