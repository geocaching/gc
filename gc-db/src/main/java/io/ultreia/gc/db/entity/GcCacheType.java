package io.ultreia.gc.db.entity;

/*-
 * #%L
 * GC toolkit :: DB
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.ImageIcon;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

/**
 * Created by tchemit on 03/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum GcCacheType {
    TRADITIONNAL("Traditional Cache", 2),
    MULTI("Multi-cache", 3),
    VIRTUAL("Virtual Cache", 4),
    LETTERBOX("Letterbox Hybrid", 5),
    EVENT("Event Cache", 6),
    APE("Ape", 7),
    UNKNOWN("Unknown Cache", 8),
    APE2("Ape", 9),
    WEBCAM("Webcam Cache", 11),
    LOCATIONLESS("Locationess Cache", 12),
    CITO("Cache In Trash Out Event", 13),
    EARTHCACHE("Earthcache", 137),
    MEGA("Mega-Event Cache", 453),
    GPS("GPS Adventures Exhibit", 1304),
    WHEREIGO("Wherigo Cache", 1858),
    GIGA("Giga-Event Cache", 7005),
    HQ("Geocaching HQ Geocache", -1);

    private final int code;
    private final String label;
    private ImageIcon icon;

    GcCacheType(String label, int code) {
        this.label = label;
        this.code = code;
    }

    public ImageIcon getIcon() {
        if (icon == null) {
            icon = SwingUtil.createImageIcon("type/" + code + ".gif");
        }
        return icon;
    }

    public static GcCacheType of(String code) {
        for (GcCacheType gcCacheType : values()) {
            if (code.equals(gcCacheType.label)) {
                return gcCacheType;
            }
        }
        try {
            Integer integer = Integer.valueOf(code);
            for (GcCacheType gcCacheType : values()) {
                if (integer == gcCacheType.code) {
                    return gcCacheType;
                }
            }
        } catch (NumberFormatException e) {
            // Not a number
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }
}
