package io.ultreia.gc.db.entity;

/*-
 * #%L
 * GC toolkit :: DB
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created by tchemit on 17/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum GcDistrict implements GcReferential {

    FR_01("Ain"),
    FR_02("Aisne"),
    FR_2A("Corse-du-Sud"),
    FR_2B("Haute-Corse"),
    FR_03("Allier"),
    FR_04("Alpes-de-Haute-Provence"),
    FR_05("Hautes-Alpes"),
    FR_06("Alpes-Maritimes"),
    FR_07("Ardèche"),
    FR_08("Ardennes"),
    FR_09("Ariège"),
    FR_10("Aube"),
    FR_11("Aude"),
    FR_12("Aveyron"),
    FR_13("Bouches-du-Rhône"),
    FR_14("Calvados"),
    FR_15("Cantal"),
    FR_16("Charente"),
    FR_17("Charente-Maritime"),
    FR_18("Cher"),
    FR_19("Corrèze"),
    FR_21("Côte-d'Or"),
    FR_22("Côtes-d'Armor"),
    FR_23("Creuse"),
    FR_24("Dordogne"),
    FR_25("Doubs"),
    FR_26("Drôme"),
    FR_27("Eure"),
    FR_28("Eure-et-Loir"),
    FR_29("Finistère"),
    FR_30("Gard"),
    FR_31("Haute-Garonne"),
    FR_32("Gers"),
    FR_33("Gironde"),
    FR_34("Hérault"),
    FR_35("Ille-et-Vilaine"),
    FR_36("Indre"),
    FR_37("Indre-et-Loire"),
    FR_38("Isère"),
    FR_39("Jura"),
    FR_40("Landes"),
    FR_41("Loir-et-Cher"),
    FR_42("Loire"),
    FR_43("Haute-Loire"),
    FR_44("Loire-Atlantique"),
    FR_45("Loiret"),
    FR_46("Lot"),
    FR_47("Lot-et-Garonne"),
    FR_48("Lozère"),
    FR_49("Maine-et-Loire"),
    FR_50("Manche"),
    FR_51("Marne"),
    FR_52("Haute-Marne"),
    FR_53("Mayenne"),
    FR_54("Meurthe-et-Moselle"),
    FR_55("Meuse"),
    FR_56("Morbihan"),
    FR_57("Moselle"),
    FR_58("Nièvre"),
    FR_59("Nord"),
    FR_60("Oise"),
    FR_61("Orne"),
    FR_62("Pas-de-Calais"),
    FR_63("Puy-de-Dôme"),
    FR_64("Pyrénées-Atlantiques"),
    FR_65("Hautes-Pyrénées"),
    FR_66("Pyrénées-Orientales"),
    FR_67("Bas-Rhin"),
    FR_68("Haut-Rhin"),
    FR_69("Rhône"),
    FR_70("Haute-Saône"),
    FR_71("Saône-et-Loire"),
    FR_72("Sarthe"),
    FR_73("Savoie"),
    FR_74("Haute-Savoie"),
    FR_75("Paris"),
    FR_76("Seine-Maritime"),
    FR_77("Seine-et-Marne"),
    FR_78("Yvelines"),
    FR_79("Deux-Sèvres"),
    FR_80("Somme"),
    FR_81("Tarn"),
    FR_82("Tarn-et-Garonne"),
    FR_83("Var"),
    FR_84("Vaucluse"),
    FR_85("Vendée"),
    FR_86("Vienne"),
    FR_87("Haute-Vienne"),
    FR_88("Vosges"),
    FR_89("Yonne"),
    FR_90("Territoire de Belfort"),
    FR_91("Essonne"),
    FR_92("Hauts-de-Seine"),
    FR_93("Seine-Saint-Denis"),
    FR_94("Val-de-Marne"),
    FR_95("Val-d'Oise"),
    FR_971("Guadeloupe"),
    FR_972("Martinique"),
    FR_973("Guyane"),
    FR_974("La Réunion"),
    FR_975("Saint-Pierre-et-Miquelon"),
    FR_976("Mayotte");

//    private final int code;
    private final String compactName;
    private final String text;

    GcDistrict(String text) {
//        this.code = Integer.valueOf(name().split("_")[1]);
        this.text = text;
        this.compactName = text.replaceAll("-", "");
    }

    @Override
    public String compactName() {
        return compactName;
    }

    @Override
    public int code() {
        return 0;
    }

    public String text() {
        return text;
    }

    @Override
    public String toString() {
        return text;
    }
}
