package io.ultreia.gc.lfn.actions;

/*-
 * #%L
 * GC toolkit :: LFN
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.lfn.ui.LogFieldNotesTableModel;
import io.ultreia.gc.lfn.ui.LogFieldNotesUI;
import io.ultreia.gc.model.GcCacheImage;
import io.ultreia.gc.model.GcFieldNote;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import org.nuiton.jaxx.widgets.file.JaxxFileChooser;


import static io.ultreia.gc.lfn.actions.SelectPhotoAction.IMAGE_EXTENSIONS;

/**
 * Created by tchemit on 15/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SelectAllPhotoAction extends LogFieldNotesActionSupport {

    public static final KeyStroke KEY_STROKE = KeyStroke.getKeyStroke("shift F3");

    public SelectAllPhotoAction(LogFieldNotesUI ui) {
        super(ui, KEY_STROKE);
        putValue(NAME, "Photo all");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JTable table = getFieldNotesTable();

        File file = JaxxFileChooser.forLoadingFile()
                .setApprovalText("Choose photo")
                .setFileHidingEnabled(true)
                .setParent(ui.getContentPanel())
                .setPatternOrDescriptionFilters(IMAGE_EXTENSIONS)
                .choose();
        if (file == null) {
            return;
        }
        GcCacheImage gcCacheImage = new GcCacheImage();
        gcCacheImage.setContent(file);

        LogFieldNotesTableModel model = (LogFieldNotesTableModel) table.getModel();
        for (GcFieldNote gcFieldNote : model) {
            gcFieldNote.setImage(gcCacheImage);
        }
        model.fireTableRowsUpdated(0, model.getRowCount());

        ui.getContentPanel().updateApply();
    }
}
