package io.ultreia.gc.lfn.actions;

/*-
 * #%L
 * GC toolkit :: LFN
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.lfn.ui.LogFieldNotesTableModel;
import io.ultreia.gc.lfn.ui.LogFieldNotesUI;
import io.ultreia.gc.lfn.ui.LogFieldNotesUIModel;
import io.ultreia.gc.model.GcFieldNote;
import io.ultreia.gc.service.api.FieldNotesService;
import io.ultreia.gc.ui.ProgressPanelUI;
import java.awt.event.ActionEvent;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingWorker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;

/**
 * Created by tchemit on 19/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DeleteFieldNotesAction extends LogFieldNotesActionSupport {


    /** Logger. */
    private static final Log log = LogFactory.getLog(SendLogsAction.class);

    private final FieldNotesService fieldNotesService;

    public DeleteFieldNotesAction(LogFieldNotesUI ui, FieldNotesService fieldNotesService) {
        super(ui, KeyStroke.getKeyStroke("ctrl DELETE"));
        this.fieldNotesService = fieldNotesService;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        JTable table = ui.getContentPanel().getFieldNotesTable();
        int[] selectedRows = table.getSelectedRows();
        LogFieldNotesTableModel model = (LogFieldNotesTableModel) table.getModel();
        List<GcFieldNote> toDelete = new LinkedList<>();
        for (int selectedRow : selectedRows) {

            GcFieldNote fieldNote = model.getFieldNote(selectedRow);
            toDelete.add(fieldNote);

        }
        ui.getProgressPanel().init((int) ui.getModel().getNbSelected());

        ui.setBusy(true);

        new RunWorker(toDelete).execute();

    }

    public class RunWorker extends SwingWorker<Void, Integer> {

        private final List<GcFieldNote> toDelete;

        public RunWorker(List<GcFieldNote> toDelete) {

            this.toDelete = toDelete;
        }

        @Override
        protected Void doInBackground() throws Exception {

            long t0 = System.nanoTime();

            LogFieldNotesUIModel model = ui.getModel();

            log.info("Found " + toDelete.size() + " cache(s) to delete.");

            ProgressPanelUI progressPanel = ui.getProgressPanel();

            toDelete.parallelStream().forEach(fieldNote -> {

                log.info(String.format("Delete field note: %s", fieldNote.getName()));

                try {
                    fieldNotesService.deleteFieldNote(fieldNote.getReferenceCode());

                    progressPanel.increment(String.format("Field note deleted : %s", fieldNote.getName()));

                } catch (Exception e) {
                    throw new RuntimeException("Can't execute field note: " + fieldNote.getName());
                }

            });

            long t1 = System.nanoTime();

            log.info("Done in " + StringUtil.convertTime(t1 - t0));

            return null;
        }

        @Override
        protected void done() {

            ui.getProgressPanel().increment("Reload field notes");

            ui.loadFieldNotes();

            ui.getProgressPanel().increment(String.format("Deleted %d logs with success!", toDelete.size()));

            ui.setBusy(false);
        }
    }


}
