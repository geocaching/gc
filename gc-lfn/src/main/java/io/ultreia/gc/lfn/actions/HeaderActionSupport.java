package io.ultreia.gc.lfn.actions;

/*-
 * #%L
 * GC toolkit :: LFN
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.AbstractAction;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

/**
 * Created by tchemit on 18/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class HeaderActionSupport extends AbstractAction {

    protected final JTextArea logText;

    protected HeaderActionSupport(String name, String iconPath, JTextArea logText, KeyStroke keyStroke) {
        super(name, SwingUtil.createIcon(iconPath));
        this.logText = logText;
        if (keyStroke != null) {
            String actionName = getClass().getName() + iconPath;
            logText.getInputMap().put(keyStroke, actionName);
            logText.getActionMap().put(actionName, this);
        }

    }

}
