package io.ultreia.gc.lfn.actions;

/*-
 * #%L
 * GC toolkit :: LFN
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;

/**
 * Created by tchemit on 18/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BoxTextAction extends HeaderActionSupport {

    private final String boxLeft;
    private final String boxRight;
    private final int boxRightSize;
    final int boxLeftSize;
    final String str;


    public BoxTextAction(String boxLeft, String boxRight, String text, String iconPath, JTextArea logText, KeyStroke keyStroke) {
        super(null, iconPath, logText, keyStroke);

        this.boxLeft = boxLeft;
        this.boxRight = boxRight;
        this.boxRightSize = boxRight.length();
        this.boxLeftSize = boxLeft.length();
        this.str = text;
    }

    String box(String text) {
        return String.format("%s%s%s", boxLeft, text, boxRight);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String selectedText = logText.getSelectedText();
        if (selectedText == null || selectedText.isEmpty()) {
            try {
                insertAtCaretPosition(logText.getCaretPosition());
            } catch (BadLocationException e1) {
                // don't care
            }
        } else {
            try {
                updateFromSelection(logText.getSelectionStart(), selectedText);
            } catch (BadLocationException e1) {
                // don't care
            }
        }
        SwingUtilities.invokeLater(logText::requestFocus);
    }

    void insertAtCaretPosition(int caretPosition) throws BadLocationException {
        logText.getDocument().insertString(caretPosition, box(str), null);
        logText.setSelectionStart(caretPosition + boxLeftSize);
        logText.setSelectionEnd(caretPosition + boxLeftSize + str.length());
    }

    void updateFromSelection(int start, String selectedText) throws BadLocationException {

        try {
            String text = logText.getDocument().getText(start - boxLeftSize, selectedText.length() + boxLeftSize + boxRightSize);
            String boxedText = box(selectedText);
            if (boxedText.equals(text)) {

                // unbox selection

                logText.getDocument().remove(start - boxLeftSize, selectedText.length() + boxLeftSize + boxRightSize);
                logText.getDocument().insertString(start - boxLeftSize, selectedText, null);

                logText.setSelectionStart(start - boxLeftSize);
                logText.setSelectionEnd(start - boxLeftSize + selectedText.length());

            } else {

                // box selection

                logText.getDocument().remove(start, selectedText.length());
                logText.getDocument().insertString(start, boxedText, null);

                logText.setSelectionStart(start + boxLeftSize);
                logText.setSelectionEnd(start + boxLeftSize + selectedText.length());

            }

        } catch (BadLocationException e1) {
            // don't care
        }

    }
}
