package io.ultreia.gc.lfn.actions;

/*-
 * #%L
 * GC toolkit :: LFN
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.lfn.ui.LogFieldNotesTableModel;
import io.ultreia.gc.lfn.ui.LogFieldNotesUI;
import io.ultreia.gc.model.GcCacheImage;
import io.ultreia.gc.model.GcFieldNote;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import org.nuiton.jaxx.widgets.file.JaxxFileChooser;

/**
 * Created by tchemit on 15/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SelectPhotoAction extends LogFieldNotesActionSupport {

    public static final KeyStroke KEY_STROKE = KeyStroke.getKeyStroke("F3");

    public static final List<String> IMAGE_EXTENSIONS = Arrays.asList(".+\\.jpg|.+\\.JPG|.+\\.jpeg|.+\\.JPEG|.+\\.gif|.+\\.GIF|.+\\.png|.+\\.PNG|.+\\.bmp|.+\\.BMP", "Images (jpg, jpeg, png, gif, bmp)",
                                                                      ".+", "All files");

    public SelectPhotoAction(LogFieldNotesUI ui) {
        super(ui, KEY_STROKE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JTable table = getFieldNotesTable();
        int[] selectedRows = table.getSelectedRows();

        File file = JaxxFileChooser.forLoadingFile()
//                .setStartDirectory(Paths.get("/tmp").toFile())
                .setApprovalText("Choose photo")
                .setFileHidingEnabled(true)
                .setParent(ui.getContentPanel())
                .setPatternOrDescriptionFilters(IMAGE_EXTENSIONS)
                .choose();
        if (file == null) {
            return;
        }
        GcCacheImage gcCacheImage = new GcCacheImage();
        gcCacheImage.setContent(file);

        LogFieldNotesTableModel model = (LogFieldNotesTableModel) table.getModel();
        for (int selectedRow : selectedRows) {

            GcFieldNote fieldNote = model.getFieldNote(selectedRow);
            fieldNote.setImage(gcCacheImage);
            model.fireTableRowsUpdated(selectedRow, selectedRow);

        }
        ui.getContentPanel().updateApply();
    }
}
