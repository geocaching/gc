package io.ultreia.gc.lfn.actions;

/*-
 * #%L
 * GC toolkit :: LFN
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.model.GcSmiley;
import java.awt.Point;
import java.awt.event.ActionEvent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.text.Caret;

/**
 * Created by tchemit on 18/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ShowSmileysAction extends HeaderActionSupport {

    private final JToolBar actionsPanel;
    private JPopupMenu popup;

    public ShowSmileysAction(JTextArea logText, JToolBar actionsPanel) {
        super(null, GcSmiley.smile.getIcon(), logText, KeyStroke.getKeyStroke("ctrl Y"));
        this.actionsPanel = actionsPanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (popup == null) {
            popup = new JPopupMenu();
            for (GcSmiley gcSmiley : GcSmiley.values()) {
                popup.add(new JMenuItem(new InsertSmileyAction(gcSmiley, logText)));
            }
            popup.addPopupMenuListener(new PopupMenuListener() {
                @Override
                public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

                }

                @Override
                public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                    SwingUtilities.invokeLater(logText::requestFocus);
                }

                @Override
                public void popupMenuCanceled(PopupMenuEvent e) {

                }
            });
        }
        Caret caret = logText.getCaret();
        Point p = new Point(caret.getMagicCaretPosition());
        p.x += logText.getLocationOnScreen().x;
        p.y += logText.getLocationOnScreen().y;

        SwingUtilities.convertPointFromScreen(p, actionsPanel);
        popup.show(actionsPanel, p.x, p.y);
    }
}
