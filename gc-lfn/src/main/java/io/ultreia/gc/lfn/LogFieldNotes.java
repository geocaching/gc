package io.ultreia.gc.lfn;

/*-
 * #%L
 * GC toolkit :: LFN
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.lfn.ui.LogFieldNotesUI;
import io.ultreia.gc.lfn.ui.LogFieldNotesUIModel;
import io.ultreia.gc.rest.client.GcRestClientApplicationContext;
import io.ultreia.gc.rest.client.GcRestClientConfig;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 14/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LogFieldNotes {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LogFieldNotes.class);

    private final LogFieldNotesContext applicationContext;

    public static void main(String[] args) throws Exception {

        LogFieldNotes runner = new LogFieldNotes(args);

        runner.start();

    }

    private LogFieldNotes(String... args) throws Exception {
        GcLfnConfig config = new GcLfnConfig();
        config.get().parse(args);

        log.info("Config loaded\n" + config.getConfigurationDescription());

        GcRestClientConfig clientConfig = GcRestClientConfig.create(args);

        log.info("Client config loaded\n" + clientConfig.getConfigurationDescription());

        GcRestClientApplicationContext client = new GcRestClientApplicationContext(clientConfig);

        applicationContext = new LogFieldNotesContext(config, client);
    }

    private void start() throws Exception {

        GcLfnConfig config = applicationContext.getConfig();

        LogFieldNotesUIModel model = new LogFieldNotesUIModel();
        model.setAutoVisitTrackables(config.isAutoVistTrackables());

        File logFile = config.getLogFile();
        if (logFile.exists()) {
            String logText = new String(Files.readAllBytes(logFile.toPath()), StandardCharsets.UTF_8);
            model.setLogText(logText);
        }

        LogFieldNotesUI ui = new LogFieldNotesUI(applicationContext, model);
        ui.setBusy(true);
        ui.logIn(config.isAutoLogin());
        ui.setBusy(false);

    }

}
