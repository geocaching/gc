package io.ultreia.gc.lfn.ui;

/*-
 * #%L
 * GC toolkit :: LFN
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.lfn.actions.BoxHeadingAction;
import io.ultreia.gc.lfn.actions.BoxTextAction;
import io.ultreia.gc.lfn.actions.RedoAction;
import io.ultreia.gc.lfn.actions.ShowSmileysAction;
import io.ultreia.gc.lfn.actions.UndoAction;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.KeyEvent;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.undo.UndoManager;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.widgets.font.FontSizor;

/**
 * Created by tchemit on 17/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LogTextHeader extends JToolBar {

    static {

        UIManager.put("action.font-size-down", SwingUtil.createActionIcon("font-size-down"));
        UIManager.put("action.font-size-up", SwingUtil.createActionIcon("font-size-up"));
        UIManager.put("action.font-size", SwingUtil.createActionIcon("font-size"));

    }

    private final FontSizor fontSizor;
    private final Action undoAction;
    private final Action redoAction;

    private final UndoManager undoManager = new UndoManager();

    LogTextHeader(JTextArea logText) {

        setFloatable(false);
        setBorderPainted(false);
        setOpaque(false);

        fontSizor = new FontSizor();
        fontSizor.setShowDefaultFontSize(true);
        fontSizor.setDefaultFontSize(14f);
        fontSizor.setFontSize(14f);
        fontSizor.setCallBack(() -> {
            if (logText.getFont() != null && fontSizor.getFontSize() != null) {
                logText.setFont(logText.getFont().deriveFont(fontSizor.getFontSize()));
            }
        });
        fontSizor.init();
        add(fontSizor);

        JToolBar actionsPanel = (JToolBar) fontSizor.getObjectById("$JToolBar0");

        actionsPanel.add(undoAction = new UndoAction(this, logText, undoManager));

        actionsPanel.add(redoAction = new RedoAction(this, logText, undoManager));

        updateDoActions();

        logText.getDocument().addUndoableEditListener(e -> {
            undoManager.addEdit(e.getEdit());
            updateDoActions();
        });

        actionsPanel.add(new BoxTextAction("**", "**", "text", "/icons/text_bold.png", logText, KeyStroke.getKeyStroke("ctrl B")));
        actionsPanel.add(new BoxTextAction("*", "*", "text", "/icons/text_italic.png", logText, KeyStroke.getKeyStroke("ctrl I")));

        //TODO
        JButton quote = new JButton(SwingUtil.createIcon("/icons/text_quote.png"));
        actionsPanel.add(quote);
        quote.setEnabled(false);

        //TODO
        JButton link = new JButton(SwingUtil.createIcon("/icons/text_link.png"));
        actionsPanel.add(link);
        link.setEnabled(false);

        //TODO
        JButton horizontalRow = new JButton(SwingUtil.createIcon("/icons/text_horizontal_rule.png"));
        actionsPanel.add(horizontalRow);
        horizontalRow.setEnabled(false);

        actionsPanel.add(new BoxHeadingAction(1, logText, KeyStroke.getKeyStroke("ctrl AMPERSAND")));
        actionsPanel.add(new BoxHeadingAction(2, logText, KeyStroke.getKeyStroke(16777449, KeyEvent.CTRL_DOWN_MASK)));
        actionsPanel.add(new BoxHeadingAction(3, logText, KeyStroke.getKeyStroke("ctrl QUOTEDBL")));
        actionsPanel.add(new BoxHeadingAction(4, logText, KeyStroke.getKeyStroke(222, KeyEvent.CTRL_DOWN_MASK)));

        //TODO
        JButton bullets = new JButton(SwingUtil.createIcon("/icons/text_list_bullets.png"));
        actionsPanel.add(bullets);
        bullets.setEnabled(false);

        //TODO
        JButton numberings = new JButton(SwingUtil.createIcon("/icons/text_list_numbers.png"));
        numberings.setEnabled(false);
        actionsPanel.add(numberings);

        actionsPanel.add(new ShowSmileysAction(logText, actionsPanel));

        JPanel status = new JPanel();
        status.setLayout(new BorderLayout());
        JLabel statusLabel = new JLabel();
        status.add(statusLabel, BorderLayout.EAST);
        actionsPanel.add(status);

        logText.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                logLength(e);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                logLength(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                logLength(e);
            }

            void logLength(DocumentEvent e) {
                int length = e.getDocument().getLength();
                if (length == 0 || length > 4000) {
                    statusLabel.setForeground(Color.RED);
                } else {
                    statusLabel.setForeground(Color.BLACK);
                }
                statusLabel.setText("Log length: " + length);
            }
        });
    }

    public void updateDoActions() {
        undoAction.setEnabled(undoManager.canUndo());
        redoAction.setEnabled(undoManager.canRedo());
    }

}
