package io.ultreia.gc.lfn.ui;

/*-
 * #%L
 * GC toolkit :: LFN
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.model.GcFieldNote;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import javax.swing.table.AbstractTableModel;

/**
 * Created by tchemit on 15/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LogFieldNotesTableModel extends AbstractTableModel implements Iterable<GcFieldNote> {

    private static final String[] COLUMN_NAMES = {"n°", "F?", "P?", "Log", "Date", "Name"};
    private static final Class<?>[] COLUMN_TYPES = {int.class, boolean.class, boolean.class, String.class, String.class, String.class};

    private final LogFieldNotesUIModel model;

    LogFieldNotesTableModel(LogFieldNotesUIModel model) {
        this.model = model;
    }

    private Optional<List<GcFieldNote>> getData() {
        return Optional.ofNullable(model.getFieldNotes());
    }

    public GcFieldNote getFieldNote(int row) {
        return getData().map(f -> f.get(row)).orElse(null);
    }

    @Override
    public int getRowCount() {
        return getData().map(List::size).orElse(0);
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return COLUMN_TYPES[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        GcFieldNote fieldNote = getFieldNote(rowIndex);
        switch (columnIndex) {
            case 0:
                return rowIndex + 1;
            case 1:
                return fieldNote.isFavorite();
            case 2:
                return fieldNote.withImage();
            case 3:
                return fieldNote.getLogType();
            case 4:
                return fieldNote.getDate();
            case 5:
                return fieldNote.getGc()+" "+fieldNote.getName();
        }
        return null;
    }

    @Override
    public Iterator<GcFieldNote> iterator() {
        return getData().map(List::iterator).orElse(null);
    }
}
