package io.ultreia.gc.lfn.ui;

/*-
 * #%L
 * GC toolkit :: LFN
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.model.GcFieldNote;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by tchemit on 15/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LogFieldNotesUIModel {

    private List<GcFieldNote> fieldNotes;
    private String logText;
    private boolean autoVisitTrackables;

    public int getNbFieldNotes() {
        return getFieldNotes().size();
    }

    public List<GcFieldNote> getFieldNotes() {
        return Optional.ofNullable(fieldNotes).orElse(Collections.emptyList());
    }

    public void setFieldNotes(List<GcFieldNote> fieldNotes) {
        this.fieldNotes = fieldNotes;
    }

    public long getNbSelected() {
        return getFieldNotes().stream().filter(GcFieldNote::isSelected).count();
    }

    public long getNbWithImages() {
        return getFieldNotes().stream().filter(GcFieldNote::withImage).count();
    }

    public long getNbFavorites() {
        return getFieldNotes().stream().filter(GcFieldNote::isFavorite).count();
    }

    public String getLogText() {
        return logText;
    }

    public void setLogText(String logText) {
        this.logText = logText;
    }

    public boolean isValid() {
        long nbCaches = getNbSelected();
        if (nbCaches == 0 || nbCaches > fieldNotes.size()) {
            return false;
        }
        if (logText == null || logText.isEmpty() || logText.length() > 4000) {
            return false;
        }
        return true;
    }

    public boolean isAutoVisitTrackables() {
        return autoVisitTrackables;
    }

    public void setAutoVisitTrackables(boolean autoVisitTrackables) {
        this.autoVisitTrackables = autoVisitTrackables;
    }

}
