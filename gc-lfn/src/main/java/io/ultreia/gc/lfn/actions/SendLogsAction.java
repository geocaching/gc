package io.ultreia.gc.lfn.actions;

/*-
 * #%L
 * GC toolkit :: LFN
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.lfn.ui.LogFieldNotesUI;
import io.ultreia.gc.lfn.ui.LogFieldNotesUIModel;
import io.ultreia.gc.model.GcFieldNote;
import io.ultreia.gc.service.api.FieldNotesService;
import io.ultreia.gc.ui.ProgressPanelUI;
import java.awt.event.ActionEvent;
import javax.swing.KeyStroke;
import javax.swing.SwingWorker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.util.StringUtil;

/**
 * Created by tchemit on 16/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SendLogsAction extends LogFieldNotesActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SendLogsAction.class);

    private final LogFieldNotesUIModel model;
    private final FieldNotesService fieldNotesService;

    public SendLogsAction(LogFieldNotesUI ui, LogFieldNotesUIModel model, FieldNotesService fieldNotesService) {
        super(ui, KeyStroke.getKeyStroke("alt pressed S"));
        this.fieldNotesService = fieldNotesService;
        putValue(MNEMONIC_KEY, (int) 'S');
        putValue(SMALL_ICON, SwingUtil.createIcon("/icons/note_go.png"));
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        long nbSteps = model.getNbSelected() + 2;

        ui.getProgressPanel().init((int) nbSteps);

        ui.setBusy(true);

        new RunWorker().execute();

    }

    public class RunWorker extends SwingWorker<Void, Integer> {

        @Override
        protected Void doInBackground() throws Exception {

            long t0 = System.nanoTime();

            log.info("Found " + model.getNbSelected() + " cache(s) to log.");

            String logText = model.getLogText();

            ui.getApplicationContext().getConfig().storeLastLogText(logText);

            boolean autoVisitTrackables = model.isAutoVisitTrackables();
            ProgressPanelUI progressPanel = ui.getProgressPanel();

            model.getFieldNotes().parallelStream().filter(GcFieldNote::isSelected).forEach(fieldNote -> {

                log.info(String.format("Log cache %s", fieldNote.getName()));

                try {
                    fieldNotesService.logFieldNote(fieldNote, logText, autoVisitTrackables);
                    progressPanel.increment(String.format("Field note log: %s", fieldNote.getName()));

                } catch (Exception e) {
                    throw new RuntimeException("Can't execute field note: " + fieldNote.getName());
                }

            });

            long t1 = System.nanoTime();

            log.info("Done in " + StringUtil.convertTime(t1 - t0));

            return null;
        }

        @Override
        protected void done() {

            log.info("Reload field notes");

            long nbCaches = model.getNbSelected();

            ui.getProgressPanel().increment("Reload field notes");

            ui.loadFieldNotes();

            ui.getProgressPanel().increment(String.format("Send %d logs with success!", nbCaches));

            ui.setBusy(false);
        }
    }

}
