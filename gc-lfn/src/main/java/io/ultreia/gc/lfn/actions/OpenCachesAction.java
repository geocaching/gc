package io.ultreia.gc.lfn.actions;

/*-
 * #%L
 * GC toolkit :: LFN
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.lfn.ui.LogFieldNotesTableModel;
import io.ultreia.gc.lfn.ui.LogFieldNotesUI;
import io.ultreia.gc.model.GcFieldNote;
import io.ultreia.gc.service.internal.GcUrls;
import java.awt.event.ActionEvent;
import java.net.URI;
import java.net.URISyntaxException;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import org.nuiton.util.DesktopUtil;

/**
 * Created by tchemit on 14/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class OpenCachesAction extends LogFieldNotesActionSupport {

    public OpenCachesAction(LogFieldNotesUI ui) {
        super(ui, KeyStroke.getKeyStroke("F4"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        JTable table = ui.getContentPanel().getFieldNotesTable();
        int[] selectedRows = table.getSelectedRows();
        LogFieldNotesTableModel model = (LogFieldNotesTableModel) table.getModel();
        for (int selectedRow : selectedRows) {

            GcFieldNote fieldNote = model.getFieldNote(selectedRow);
            String gc = fieldNote.getGc();
            try {
                DesktopUtil.browse(new URI(String.format(GcUrls.URL_CACHE, gc)));
            } catch (URISyntaxException ignored) {
            }
        }

    }

}
