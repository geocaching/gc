package io.ultreia.gc.lfn.actions;

/*-
 * #%L
 * GC toolkit :: LFN
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.model.GcSmiley;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

/**
 * Created by tchemit on 19/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class InsertSmileyAction extends HeaderActionSupport {

    private final GcSmiley smiley;

    public InsertSmileyAction(GcSmiley smiley, JTextArea logText) {
        super(InsertSmileyAction.class.getName(), smiley.getIcon(), logText, null);
        this.smiley = smiley;
        putValue(NAME, smiley.name() + " [" + smiley.getText() + "]");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int caretPosition = logText.getCaretPosition();
        logText.insert("[" + smiley.getText() + "]", caretPosition);
        logText.requestFocus();
    }
}
