package io.ultreia.gc.lfn.ui;

/*-
 * #%L
 * GC toolkit :: LFN
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.lfn.GcLfnConfig;
import io.ultreia.gc.lfn.LogFieldNotesContext;
import io.ultreia.gc.model.GcFieldNote;
import io.ultreia.gc.ui.GcApplicationUISupport;
import java.util.Collections;
import java.util.List;

/**
 * Created by tchemit on 15/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LogFieldNotesUI extends GcApplicationUISupport<GcLfnConfig, LogFieldNotesContext, LogFieldNotesUIModel, LogFieldNotesContentPanelUI> {

    public LogFieldNotesUI(LogFieldNotesContext applicationContext, LogFieldNotesUIModel model) {
        super(applicationContext, model, "Log field notes");
    }

    @Override
    protected LogFieldNotesContentPanelUI createContentPanel(LogFieldNotesUIModel model) {
        return new LogFieldNotesContentPanelUI(this, model);
    }

    @Override
    protected void afterLogOut() {
        loadFieldNotes(Collections.emptyList());
    }

    @Override
    public void afterLogin(String name, String authId) {

        super.afterLogin(name, authId);
        loadFieldNotes();

    }

    public void loadFieldNotes() {

        List<GcFieldNote> fieldNotes = getApplicationContext().newFieldNotesService().loadFieldNotes();

        loadFieldNotes(fieldNotes);

    }

    private void loadFieldNotes(List<GcFieldNote> fieldNotes) {
        getModel().setFieldNotes(fieldNotes);
        LogFieldNotesContentPanelUI contentPanel = getContentPanel();
        contentPanel.getFieldNotesTable().clearSelection();
        LogFieldNotesTableModel tableModel = (LogFieldNotesTableModel) contentPanel.getFieldNotesTable().getModel();
        tableModel.fireTableDataChanged();

        contentPanel.updateApply();
    }

}
