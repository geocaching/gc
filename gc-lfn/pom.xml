<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  GC toolkit :: LFN
  %%
  Copyright (C) 2017 Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>io.ultreia.gc</groupId>
    <artifactId>gc</artifactId>
    <version>1.3.5-SNAPSHOT</version>
  </parent>

  <artifactId>gc-lfn</artifactId>
  <version>1.3.5-SNAPSHOT</version>

  <name>GC toolkit :: LFN</name>
  <description>Gc :: Log Field notes</description>

  <properties>
    <maven.jar.main.class>io.ultreia.gc.lfn.LogFieldNotes</maven.jar.main.class>
    <exec.mainClass>${maven.jar.main.class}</exec.mainClass>
  </properties>

  <dependencies>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>gc-db</artifactId>
      <version>${project.version}</version>
    </dependency>
    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>gc-api</artifactId>
      <version>${project.version}</version>
    </dependency>

    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>gc-rest-client</artifactId>
      <version>${project.version}</version>
    </dependency>

    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-config</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-version</artifactId>
    </dependency>

    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-utils</artifactId>
    </dependency>

    <dependency>
      <groupId>commons-logging</groupId>
      <artifactId>commons-logging</artifactId>
    </dependency>

    <dependency>
      <groupId>log4j</groupId>
      <artifactId>log4j</artifactId>
      <scope>runtime</scope>
    </dependency>

    <dependency>
      <groupId>io.ultreia.java4all.jaxx</groupId>
      <artifactId>jaxx-runtime</artifactId>
    </dependency>

    <dependency>
      <groupId>io.ultreia.java4all.jaxx</groupId>
      <artifactId>jaxx-widgets-font</artifactId>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all.jaxx</groupId>
      <artifactId>jaxx-widgets-file</artifactId>
    </dependency>

  </dependencies>

  <build>

    <plugins>

      <plugin>
        <artifactId>maven-dependency-plugin</artifactId>
        <executions>
          <execution>
            <id>copy-dependencies</id>
            <goals>
              <goal>copy-dependencies</goal>
            </goals>
            <phase>prepare-package</phase>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>buildnumber-maven-plugin</artifactId>
        <executions>
          <execution>
            <goals>
              <goal>create</goal>
            </goals>
            <phase>validate</phase>
          </execution>
        </executions>
        <configuration>
          <doCheck>false</doCheck>
          <doUpdate>false</doUpdate>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.nuiton</groupId>
        <artifactId>nuiton-config-maven-plugin</artifactId>
        <executions>
          <execution>
            <phase>generate-sources</phase>
            <id>generate-config</id>
            <goals>
              <goal>generate</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

    </plugins>
  </build>

  <profiles>

    <profile>
      <id>assembly-profile</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>

      <build>
        <defaultGoal>verify</defaultGoal>
        <plugins>

          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>build-helper-maven-plugin</artifactId>
            <executions>
              <execution>
                <id>parse-version</id>
                <goals>
                  <goal>parse-version</goal>
                </goals>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <groupId>com.akathist.maven.plugins.launch4j</groupId>
            <artifactId>launch4j-maven-plugin</artifactId>
            <executions>
              <execution>
                <id>launch4j</id>
                <phase>package</phase>
                <goals>
                  <goal>launch4j</goal>
                </goals>
              </execution>
            </executions>
          </plugin>

          <plugin>
            <artifactId>maven-assembly-plugin</artifactId>
            <executions>
              <execution>
                <id>create-application-assembly</id>
                <phase>package</phase>
                <goals>
                  <goal>single</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>

    <profile>
      <id>reporting</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>

      <reporting>
        <plugins>

          <plugin>
            <groupId>org.nuiton</groupId>
            <artifactId>nuiton-config-maven-plugin</artifactId>
            <version>${nuitonConfigVersion}</version>
            <inherited>false</inherited>
            <reportSets>
              <reportSet>
                <reports>
                  <report>report</report>
                </reports>
              </reportSet>
            </reportSets>
            <configuration>
              <!--<i18nBundleName>gc-lfn</i18nBundleName>-->
            </configuration>
          </plugin>

        </plugins>
      </reporting>

    </profile>
  </profiles>
</project>
