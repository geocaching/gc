package io.ultreia.gc.service.internal;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.service.internal.db.GcLogsLoader;
import io.ultreia.gc.db.entity.GcCache;
import io.ultreia.gc.db.entity.GcDbTopiaPersistenceContext;
import io.ultreia.gc.model.GcArcheoLog;
import io.ultreia.gc.model.GcLog;
import io.ultreia.gc.service.api.GcCacheService;
import io.ultreia.gc.service.api.GcLogService;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 18/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcLogServiceImpl extends GcServiceSupportImpl implements GcLogService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcLogServiceImpl.class);

    @Override
    public void updateLogs() {
        GcCacheService cacheService = getServiceContext().newCacheService();
        Set<String> gcNames = cacheService.getGcNames();
        log.info("found " + gcNames.size() + " cache(s).");
        MutableInt i = new MutableInt();

        gcNames.parallelStream().forEach(gcName -> {

            try {
                log.info(String.format("%d/%d : %s", i.incrementAndGet(), gcNames.size(), gcName));
                io.ultreia.gc.model.GcCache cache = cacheService.getCacheFromGcName(gcName);
                List<GcLog> logsFromGcName = getLogsFromGcName(gcName, true);
                log.info(String.format("%d/%d : %s - %d", i.getValue(), gcNames.size(), cache.getGcName(), logsFromGcName.size()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void loadLogs() {

        GcCacheService cacheService = getServiceContext().newCacheService();
        Set<String> gcNames = cacheService.getGcNamesWithoutLogs();
        log.info("found " + gcNames.size() + " cache(s).");
        MutableInt i = new MutableInt();

        gcNames.parallelStream().forEach(gcName -> {

            try {
                log.info(String.format("%d/%d : %s", i.incrementAndGet(), gcNames.size(), gcName));
                io.ultreia.gc.model.GcCache cache = cacheService.getCacheFromGcName(gcName);
                List<GcLog> logsFromGcName = getLogsFromGcName(gcName, false);
                log.info(String.format("%d/%d : %s - %d", i.getValue(), gcNames.size(), cache.getGcName(), logsFromGcName.size()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public List<GcLog> getLogsFromGcName(String gcName, boolean updateLogs) {

        try (GcDbTopiaPersistenceContext persistenceContext = getServiceContext().newPersistenceContext()) {
            return new GcLogsLoader(this, persistenceContext, updateLogs).load(gcName).stream().map(GcLog::fromEntity).collect(Collectors.toList());
        }
    }

    @Override
    public void deleteLogsForGcName(String gcName) {

        log.info(String.format("Delete logs for cache: %s", gcName));
        try (GcDbTopiaPersistenceContext persistenceContext = getServiceContext().newPersistenceContext()) {
            GcCache gcCache = persistenceContext.getGcCacheDao().forGcNameEquals(gcName).tryFindAny().orNull();
            if (gcCache != null) {
                gcCache.setLogs(Collections.emptyList());
                persistenceContext.commit();
            }
        }
    }


    @Override
    public Optional<GcArcheoLog> getMyArcheoLogFromGcName(String gcName, String username) {

        log.info(String.format("Seek archeo logs for cache: %s", gcName));

        List<GcLog> logs = getLogsFromGcName(gcName, false);

        GcLog myLog = null;

        Iterator<GcLog> iterator = logs.iterator();

        while (iterator.hasNext()) {
            GcLog gcLog = iterator.next();
            if (gcLog.isFoundByUser(username)) {
                myLog = gcLog;
                break;
            }
        }

        if (myLog == null) {
            return Optional.empty();
        }

        Date created = myLog.getLogDate();
        List<GcLog> sameDayLogs = new LinkedList<>();
        GcLog previousLog = null;
        GcLog previousLogOtherDate = null;
        while (iterator.hasNext()) {
            GcLog gcLog = iterator.next();
            if (gcLog.isFound()) {
                if (previousLog == null) {
                    previousLog = gcLog;
                }
                if (created.equals(gcLog.getLogDate())) {
                    sameDayLogs.add(gcLog);
                } else {
                    if (!sameDayLogs.isEmpty()) {
                        previousLogOtherDate = gcLog;
                    }
                    break;
                }
            }
        }

        return Optional.of(new GcArcheoLog(gcName, myLog, previousLog, previousLogOtherDate, sameDayLogs));

    }


}
