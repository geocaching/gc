package io.ultreia.gc.service.internal.db;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.db.entity.GcCache;
import io.ultreia.gc.db.entity.GcCacheTopiaDao;
import io.ultreia.gc.db.entity.GcDbTopiaPersistenceContext;
import io.ultreia.gc.db.entity.GcEntityLoader;
import io.ultreia.gc.service.internal.GcServiceSupportImpl;
import io.ultreia.gc.session.GcSession;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HResponse;
import java.util.Optional;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;


import static io.ultreia.gc.service.internal.GcUrls.URL_CACHE;

/**
 * Created by tchemit on 27/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcCacheLoader implements GcEntityLoader<GcCache> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcCacheLoader.class);

    private final GcSession gcSession;
    private final GcDbTopiaPersistenceContext persistenceContext;
    private final boolean commit;
    private GcCacheBinder cacheBinder;

    public GcCacheLoader(GcServiceSupportImpl service, GcDbTopiaPersistenceContext persistenceContext, boolean commit) {
        this.gcSession = service.getNeutralGcSession();
        this.persistenceContext = persistenceContext;
        this.commit = commit;
        this.cacheBinder = new GcCacheBinder(service, this.persistenceContext);
    }

    public GcCacheLoader(GcServiceSupportImpl service, GcSession gcSession, GcDbTopiaPersistenceContext persistenceContext, boolean commit) {
        this.gcSession = gcSession;
        this.persistenceContext = persistenceContext;
        this.commit = commit;
        this.cacheBinder = new GcCacheBinder(service, this.persistenceContext);
    }

    public io.ultreia.gc.model.GcCache get(String gcName) {
        log.debug("Get cache: " + gcName);

        HRequest request = gcSession.create(String.format(URL_CACHE, gcName)).get();
        HResponse build = gcSession.executeRequest(request);

        Document doc = build.toHtml();

        return io.ultreia.gc.model.GcCache.fromGCHtml(doc);
    }

    public GcCache acquire(String gcName, boolean create) {

        log.info("Acquire cache: " + gcName);

        io.ultreia.gc.model.GcCache cacheModel = get(gcName);

        return persist(cacheModel, create);
    }

    public GcCache persist(io.ultreia.gc.model.GcCache cacheModel, boolean create) {

        log.debug("Persist cache: " + cacheModel.getGcName());

        GcCache gcCache;
        if (create) {
            gcCache = persistenceContext.getGcCacheDao().newInstance();
        } else {
            gcCache = persistenceContext.getGcCacheDao().forGcNameEquals(cacheModel.getGcName()).findUnique();
        }
        cacheBinder.toEntity(cacheModel, gcCache);

        if (create) {
            persistenceContext.getGcCacheDao().create(gcCache);
        } else {
            persistenceContext.getGcCacheDao().update(gcCache);
        }

        if (commit) {
            persistenceContext.commit();
        }
        return gcCache;
    }

    @Override
    public GcCache load(String gcName) {

        GcCacheTopiaDao cacheDao = persistenceContext.getGcCacheDao();

        Optional<GcCache> optionalDbCache = Optional.ofNullable(cacheDao.forGcNameEquals(gcName).tryFindAny().orNull());
        if (optionalDbCache.isPresent()) {

            log.info("Use db cache: " + gcName);

            return optionalDbCache.get();
        }

        return acquire(gcName, true);

    }
}
