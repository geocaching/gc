package io.ultreia.gc.service.api;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.model.GcFieldNote;
import io.ultreia.gc.model.GcLogDraft;
import io.ultreia.java4all.http.spi.Delete;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Post;
import java.util.List;

/**
 * Created by tchemit on 16/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface FieldNotesService extends GcService {

    @Get
    List<GcFieldNote> loadFieldNotes();

    @Post(useMultiPartForm = true)
    void logFieldNote(GcFieldNote fieldNote, String logText, boolean autoVisitTrackables);

    @Get
    GcLogDraft getLogDraft(String referenceCode);

    @Delete
    void deleteFieldNote(String draftGuid);

    @Post
    String computeCommon(String common);
}
