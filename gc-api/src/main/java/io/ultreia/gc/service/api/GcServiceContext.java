package io.ultreia.gc.service.api;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Optional;

/**
 * Created by tchemit on 16/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface GcServiceContext extends GcServiceFactory {

    Optional<String> getAuthId();

    void setAuthId(String authId);

    <S extends GcService> Class<S> getServiceClass(Class<S> serviceType);

    <S extends GcService> S newService(Class<S> serviceType);

}
