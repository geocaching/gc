package io.ultreia.gc.service.internal;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.model.GcTrackable;
import io.ultreia.gc.service.api.GcTrackableService;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HResponse;
import java.util.Set;

/**
 * Created by tchemit on 25/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcTrackableServiceImpl extends GcServiceSupportImpl implements GcTrackableService {

    @Override
    public Set<GcTrackable> getTrackables() {

        HRequest request = create(URL_GET_TRACKABLES)
                .addAuthToken()
                .get();

        HResponse build = executeRequest(request);

        return build.toSet(GcTrackable.class);

    }

}
