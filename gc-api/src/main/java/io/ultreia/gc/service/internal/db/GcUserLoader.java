package io.ultreia.gc.service.internal.db;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.db.entity.GcDbTopiaPersistenceContext;
import io.ultreia.gc.db.entity.GcEntityLoader;
import io.ultreia.gc.db.entity.GcUser;
import io.ultreia.gc.db.entity.GcUserTopiaDao;
import io.ultreia.gc.service.internal.GcServiceSupportImpl;
import io.ultreia.gc.session.GcSession;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HResponse;
import java.io.Closeable;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;


import static io.ultreia.gc.service.internal.GcUrls.URL_GET_PROFILE;
import static io.ultreia.gc.service.internal.GcUrls.URL_GET_PROFILE_GUID;
import static io.ultreia.gc.service.internal.GcUrls.URL_GET_PROFILE_NAME;

/**
 * Created by tchemit on 03/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcUserLoader implements GcEntityLoader<GcUser>, Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcUserLoader.class);

    private final GcSession gcSession;
    private final GcDbTopiaPersistenceContext persistenceContext;
    private final boolean commit;
    private final boolean useCache;
    private final Map<Integer, GcUser> existingIds;
    private final Map<String, GcUser> existingGuids;
    private final Map<String, GcUser> existingUsernames;

    public GcUserLoader(GcServiceSupportImpl service, GcDbTopiaPersistenceContext persistenceContext, boolean commit, boolean useCache) {
        this.gcSession = service.getNeutralGcSession();
        this.persistenceContext = persistenceContext;
        this.commit = commit;
        this.useCache = useCache;
        this.existingIds = new TreeMap<>();
        this.existingGuids = new TreeMap<>();
        this.existingUsernames = new TreeMap<>();
        if (useCache) {
            persistenceContext.getGcUserDao().findAll().forEach(u -> {
                existingIds.put(u.getUserId(), u);
                existingGuids.put(u.getUserGuid(), u);
                existingUsernames.put(u.getName(), u);
            });
        }
    }

    private static final String[] GUID_A = {
            "ctl00_ContentBody_ProfilePanel1_lnkEmailUser",
            "ctl00_ProfileHead_ProfileHeader_lnkEmailUser",
            "ctl00_ContentBody_ProfileHeader_lnkGiftSubscription",
            "ctl00_ProfileHead_ProfileHeader_lnkGiftSubscription",
            "ctl00_ContentBody_ProfileHeader_lnkAddAsFriend",
            "ctl00_ProfileHead_ProfileHeader_lnkAddAsFriend",
            "lnkSendMessage"
    };

    public enum AcquireType {
        ID(URL_GET_PROFILE),
        GUID(URL_GET_PROFILE_GUID),
        USERNAME(URL_GET_PROFILE_NAME);

        private final String url;

        AcquireType(String url) {
            this.url = url;
        }
    }

    public GcUser acquire(String userId, AcquireType type) {

        log.info(String.format("Acquire user (by %s): %s", type, userId));
        HRequest request = gcSession.create(String.format(type.url, userId)).get();
        HResponse build = gcSession.executeRequest(request);

        Document doc = build.toHtml();

        GcUserTopiaDao gcUserDao = persistenceContext.getGcUserDao();
        GcUser user = gcUserDao.newInstance();

        user.setName(doc.select("span[id='ctl00_ProfileHead_ProfileHeader_lblMemberName']").text());
        if (StringUtils.isEmpty(user.getName())) {
            user.setName(doc.select("span[id='ctl00_ContentBody_ProfilePanel1_lblMemberName']").text());
        }
        try {
            user.setUserId(Integer.valueOf(doc.select("a[id='ctl00_ProfileHead_ProfileHeader_lnkSeePosts']").attr("href").split("mid=")[1]));
        } catch (Exception e) {
            user.setUserId(Integer.valueOf(doc.select("a[id='ctl00_ContentBody_ProfilePanel1_lnkSeePosts']").attr("href").split("mid=")[1]));
        }

        try {
            user.setMemberSince(doc.select("span[id='ctl00_ProfileHead_ProfileHeader_lblMemberSinceDate']").text());
        } catch (Exception e) {
            user.setMemberSince(doc.select("span[id='ctl00_ContentBody_ProfilePanel1_lblMemberSinceDate']").text());
        }
        for (String a : GUID_A) {
            try {
                user.setUserGuid(doc.select("a[id='" + a + "']").attr("href").split("=")[1]);
                break;
            } catch (Exception e) {
                // try next one
            }
        }
        Objects.requireNonNull(user.getUserGuid());

        log.info(String.format("Persist user: %s - id: %d - guid: %s", user.getName(), user.getUserId(), user.getUserGuid()));
        gcUserDao.create(user);
        if (useCache) {
            synchronized (existingGuids) {
                existingGuids.put(user.getUserGuid(), user);
            }
            synchronized (existingIds) {
                existingIds.put(user.getUserId(), user);
            }
        }
        if (commit) {
            persistenceContext.commit();
        }
        return user;
    }

    @Override
    public GcUser load(String userId) {
        Optional<GcUser> gcUserOptional;
        AcquireType usingGuid = AcquireType.ID;
        try {
            Integer uId = Integer.valueOf(userId);
            if (useCache) {
                synchronized (existingIds) {
                    gcUserOptional = existingIds.entrySet().stream().filter(e -> Objects.equals(uId, e.getKey())).findFirst().map(Map.Entry::getValue);
                }

            } else {
                GcUserTopiaDao dao = persistenceContext.getGcUserDao();
                gcUserOptional = Optional.ofNullable(dao.forUserIdEquals(uId).tryFindAny().orNull());
            }

        } catch (NumberFormatException eee) {
            usingGuid = AcquireType.GUID;
            if (useCache) {
                synchronized (existingGuids) {
                    gcUserOptional = existingGuids.entrySet().stream().filter(e -> Objects.equals(userId, e.getKey())).findFirst().map(Map.Entry::getValue);
                }

            } else {
                GcUserTopiaDao dao = persistenceContext.getGcUserDao();
                gcUserOptional = Optional.ofNullable(dao.forUserGuidEquals(userId).tryFindAny().orNull());
            }

            if (!gcUserOptional.isPresent()) {

                usingGuid = AcquireType.USERNAME;
                // try by name
                if (useCache) {
                    synchronized (existingUsernames) {
                        gcUserOptional = existingUsernames.entrySet().stream().filter(e -> Objects.equals(userId, e.getKey())).findFirst().map(Map.Entry::getValue);
                    }

                } else {
                    GcUserTopiaDao dao = persistenceContext.getGcUserDao();
                    gcUserOptional = Optional.ofNullable(dao.forNameEquals(userId).tryFindAny().orNull());
                }

            }
        }
        AcquireType finalUsingGuid = usingGuid;
        return gcUserOptional.orElseGet(() -> acquire(userId, finalUsingGuid));
    }

    @Override
    public void close() {
        existingGuids.clear();
        existingIds.clear();
    }
}
