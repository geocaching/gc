package io.ultreia.gc.service.api;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Optional;

/**
 * Created by tchemit on 16/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class GcServiceContextSupport implements GcServiceContext {

    private String authId;

    @Override
    public Optional<String> getAuthId() {
        return Optional.ofNullable(authId);
    }

    @Override
    public void setAuthId(String authId) {
        this.authId = authId;
    }

    @Override
    public <S extends GcService> S newService(Class<S> serviceType) {

        try {

            S s = getServiceClass(serviceType).newInstance();
            s.setServiceContext(this);
            return s;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public LoginService newLoginService() {
        return newService(LoginService.class);
    }

    @Override
    public GcChallengeService newChallengeService() {
        return newService(GcChallengeService.class);
    }

    @Override
    public GcCacheService newCacheService() {
        return newService(GcCacheService.class);
    }

    @Override
    public GcUserService newUserService() {
        return newService(GcUserService.class);
    }

    @Override
    public GcLogService newLogService() {
        return newService(GcLogService.class);
    }

    @Override
    public GcTrackableService newTrackableService() {
        return newService(GcTrackableService.class);
    }

    @Override
    public GpxService newGpxService() {
        return newService(GpxService.class);
    }

    @Override
    public FieldNotesService newFieldNotesService() {
        return newService(FieldNotesService.class);
    }

}
