package io.ultreia.gc.service.internal.db;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * Created by tchemit on 11/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DateConverter {


    private final ImmutableList<DateFormat> en = ImmutableList.<DateFormat>builder()
            .add(new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH))
            .add(new SimpleDateFormat("d. M. yyyy", Locale.ENGLISH))
            .add(new SimpleDateFormat("d.M.yyyy", Locale.ENGLISH))
            .add(new SimpleDateFormat("d.MM.yyyy", Locale.ENGLISH))
            .add(new SimpleDateFormat("d/M/yy", Locale.ENGLISH))
            .add(new SimpleDateFormat("d/M/yyyy", Locale.ENGLISH))
            .add(new SimpleDateFormat("d/MM/yyyy", Locale.ENGLISH))
            .add(new SimpleDateFormat("dd MMM yy", Locale.ENGLISH))
            .add(new SimpleDateFormat("dd.MM.yy", Locale.ENGLISH))
            .add(new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH))
            .add(new SimpleDateFormat("dd.MM.yyyy.", Locale.ENGLISH))
            .add(new SimpleDateFormat("dd.MMM.yyyy", Locale.ENGLISH))
            .add(new SimpleDateFormat("dd/MM/yy", Locale.ENGLISH))
            .add(new SimpleDateFormat("dd/MMM/yyyy", Locale.ENGLISH))
            .add(new SimpleDateFormat("dd-MM-yy", Locale.ENGLISH))
            .add(new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH))
            .add(new SimpleDateFormat("d-M-yyyy", Locale.ENGLISH))
            .add(new SimpleDateFormat("M/d/yyyy", Locale.ENGLISH))
            .add(new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH))
            .add(new SimpleDateFormat("MMM/dd/yyyy", Locale.ENGLISH))
            .add(new SimpleDateFormat("yyyy.MM.dd.", Locale.ENGLISH))
            .add(new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH))
            .add(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH))
            .build();

    private final ImmutableList<DateFormat> fr = ImmutableList.<DateFormat>builder()
            .add(new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH))
            .add(new SimpleDateFormat("d. M. yyyy", Locale.FRENCH))
            .add(new SimpleDateFormat("d.M.yyyy", Locale.FRENCH))
            .add(new SimpleDateFormat("d.MM.yyyy", Locale.FRENCH))
            .add(new SimpleDateFormat("d/M/yy", Locale.FRENCH))
            .add(new SimpleDateFormat("d/M/yyyy", Locale.FRENCH))
            .add(new SimpleDateFormat("d/MM/yyyy", Locale.FRENCH))
            .add(new SimpleDateFormat("dd MMM yy", Locale.FRENCH))
            .add(new SimpleDateFormat("dd.MM.yy", Locale.FRENCH))
            .add(new SimpleDateFormat("dd.MM.yyyy", Locale.FRENCH))
            .add(new SimpleDateFormat("dd.MM.yyyy.", Locale.FRENCH))
            .add(new SimpleDateFormat("dd.MMM.yyyy", Locale.FRENCH))
            .add(new SimpleDateFormat("dd/MM/yy", Locale.FRENCH))
            .add(new SimpleDateFormat("dd/MMM/yyyy", Locale.FRENCH))
            .add(new SimpleDateFormat("dd-MM-yy", Locale.FRENCH))
            .add(new SimpleDateFormat("dd-MM-yyyy", Locale.FRENCH))
            .add(new SimpleDateFormat("d-M-yyyy", Locale.FRENCH))
            .add(new SimpleDateFormat("M/d/yyyy", Locale.FRENCH))
            .add(new SimpleDateFormat("MM/dd/yyyy", Locale.FRENCH))
            .add(new SimpleDateFormat("MMM/dd/yyyy", Locale.FRENCH))
            .add(new SimpleDateFormat("yyyy.MM.dd.", Locale.FRENCH))
            .add(new SimpleDateFormat("yyyy/MM/dd", Locale.FRENCH))
            .add(new SimpleDateFormat("yyyy-MM-dd", Locale.FRENCH))
            .build();

    public Date convert(String txt) {
        Date result = convert(en, txt);
        if (result == null) {
            result = convert(fr, txt);
        }
        Objects.requireNonNull(result, "Could not get log date from: " + txt);
        return result;
    }

    private Date convert(List<DateFormat> en, String txt) {
        Date result = null;
        for (DateFormat df : en) {
            try {
                result = df.parse(txt);
            } catch (ParseException e) {
                //
            }
        }
        return result;

    }

    public static void main(String[] args) {
        new DateConverter().convert("31/Jul/2015");
    }
}
