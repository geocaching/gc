package io.ultreia.gc.service.internal;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.ultreia.gc.model.GcCache;
import io.ultreia.gc.model.GcCacheAttribute;
import io.ultreia.gc.service.api.GpxService;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by tchemit on 18/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GpxServiceImpl extends GcServiceSupportImpl implements GpxService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GpxServiceImpl.class);

    @Override
    public Set<String> getGcNames(File gpxFile) {

        Set<String> result = new LinkedHashSet<>();
        try (BufferedReader read = Files.newBufferedReader(gpxFile.toPath())) {
            String line;
            while ((line = read.readLine()) != null) {
                int i = line.indexOf("<name>");
                if (i > -1 && !line.contains("Project")) {
                    String gcName = line.substring(i + "<name>".length(), line.indexOf("</name>", i));
                    if (gcName.toUpperCase().startsWith("GC")) {
                        result.add(gcName);
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public List<GcCache> getCaches(File gpxFile) {

        List<GcCache> result = new LinkedList<>();
        String cleanXml = flatXmlNamespaces(gpxFile.toPath());
        Document document = Jsoup.parse(cleanXml);
        for (Element wpt : document.select("wpt")) {

            GcCache gcCache = new GcCache();

            gcCache.setLat(wpt.attr("lat"));
            gcCache.setLon(wpt.attr("lon"));

            gcCache.setGcName(wpt.select("name").text());
            gcCache.setDesc(wpt.select("desc").text());
            gcCache.setUrl(wpt.select("url").text());
            gcCache.setUrlname(wpt.select("urlname").text());
            gcCache.setSym(wpt.select("sym").text());
            gcCache.setTime(wpt.select("time").text());
            gcCache.setGcType(wpt.select("type").text().split("\\|")[1]);

            Elements groundspeak = wpt.select("groundspeak_cache");

            gcCache.setAvailable("True".equals(groundspeak.attr("available")));
            gcCache.setArchived("True".equals(groundspeak.attr("archived")));
            gcCache.setGcId(groundspeak.attr("id"));
            gcCache.setPlacedby(groundspeak.select("groundspeak_placed_by").text());
            gcCache.setOwner(groundspeak.select("groundspeak_owner").text());
            gcCache.setOwnerId(groundspeak.select("groundspeak_owner").attr("id"));
//            Elements types = groundspeak.select("groundspeak_type");
//            if (types.size()>0) {
//                gcCache.setGcType(types.get(0).text());
//            }else {
//                System.out.println("No gc type ?!!! for "+gcCache.getGcName());
//            }
            gcCache.setContainer(groundspeak.select("groundspeak_container").text());
            try {
                gcCache.setDifficulty(Float.valueOf(groundspeak.select("groundspeak_difficulty").text()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                gcCache.setTerrain(Float.valueOf(groundspeak.select("groundspeak_terrain").text()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                gcCache.setCountry(groundspeak.select("groundspeak_country").text());
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                gcCache.setState(groundspeak.select("groundspeak_state").text());
            } catch (Exception e) {
                e.printStackTrace();
            }
            gcCache.setShortDesc(groundspeak.select("groundspeak_short_description").text());
            gcCache.setShortDescHtml("True".equals(groundspeak.select("groundspeak_short_description").attr("html")));
            gcCache.setLongDesc(groundspeak.select("groundspeak_long_description").text());
            gcCache.setLongDescHtml("True".equals(groundspeak.select("groundspeak_long_description").attr("html")));
            gcCache.setHint(groundspeak.select("groundspeak_encoded_hints").text());

            Set<GcCacheAttribute> attributes = new LinkedHashSet<>();
            for (Element groundspeakAttribute : groundspeak.select("groundspeak_attribute")) {

                int id = Integer.valueOf(groundspeakAttribute.attr("id"));
                int inc = Integer.valueOf(groundspeakAttribute.attr("inc"));
                GcCacheAttribute attribute = new GcCacheAttribute();
                attribute.setId(id);
                attribute.setInc(inc);
                attributes.add(attribute);
            }
            gcCache.setAttributes(attributes);
            //TODO Manage waypoints
            //TODO Manage logs

            Elements gsak_wptExtensions = wpt.select("gsak_wptExtension");
            if (gsak_wptExtensions.size() == 1) {

                Element gsak = gsak_wptExtensions.get(0);
                gcCache.setHasGsakExt(true);

                String gsak_oldLat = gsak.select("gsak_LatBeforeCorrect").text();
                if (!gsak_oldLat.isEmpty()) {
                    gcCache.setOldLat(gsak_oldLat);
                }
                String gsak_oldLon = gsak.select("gsak_LonBeforeCorrect").text();
                if (!gsak_oldLon.isEmpty()) {
                    gcCache.setOldLon(gsak_oldLon);
                }
                String gsak_note = gsak.select("gsak_GcNote").text();
                if (!gsak_note.isEmpty()) {
                    gcCache.setUserNote(gsak_note);
                }
                String gsak_favPoints = gsak.select("gsak_FavPoints").text();
                if (!gsak_favPoints.isEmpty()) {
                    gcCache.setFavPoints(Integer.valueOf(gsak_favPoints));
                }
                gcCache.setPremium("1".equals(gsak.select("gsak_IsPremium").text()));

            }

            result.add(gcCache);
        }
        return result;
    }

    public static void main(String... args) throws IOException {

        Path path = Paths.get("/home/tchemit/.gc/finish-st-seb.gpx");

        List<GcCache> caches = new GpxServiceImpl().getCaches(path.toFile());

        System.out.printf(String.format("Found %d cache(s).", caches.size()));
    }

    private static String flatXmlNamespaces(Path path) {
        StringBuilder cleanXmlBuilder = new StringBuilder();
        try (BufferedReader reader = Files.newBufferedReader(path)) {
            String line;
            while ((line = reader.readLine()) != null) {

                String cleanLine = line.replaceAll("<(\\w+):(\\w+)>", "<$1_$2>")
                        .replaceAll("<(\\w+):(\\w+)\\s([^>]+)>", "<$1_$2 $3>")
                        .replaceAll("</(\\w+):(\\w+)>", "</$1_$2>");
                cleanXmlBuilder.append(cleanLine).append('\n');
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return cleanXmlBuilder.toString();
    }

    public List<GcCache> load(File jsonFile) throws IOException {
        try (BufferedReader reader = Files.newBufferedReader(jsonFile.toPath())) {
            return new GsonBuilder().create().fromJson(reader, new TypeToken<List<GcCache>>() {
            }.getType());
        }
    }

    public void store(Collection<GcCache> caches, File jsonFile) throws IOException {
        try (BufferedWriter reader = Files.newBufferedWriter(jsonFile.toPath())) {
            new GsonBuilder().setPrettyPrinting().create().toJson(caches, reader);
        }
    }

    public File toJsonFile(File dataDirectory, File gpxFile, String suffix) {

        String fileNamePrefix = StringUtils.removeEnd(gpxFile.getName(), ".gpx");
        String gsonFileName = fileNamePrefix + suffix + ".json";
        return new File(dataDirectory, gsonFileName);

    }

    public List<GcCache> loadGpxOrJson(File dataDirectory, File gpxFile) throws IOException {
        File jsonFile = toJsonFile(dataDirectory, gpxFile, "");
        List<GcCache> caches;
        if (jsonFile.exists()) {
            log.info("Loading caches from json file: " + jsonFile);
            caches = load(jsonFile);
        } else {
            log.info("Loading caches from gpx file: " + gpxFile);
            caches = getCaches(gpxFile);
            log.info("Store caches to json file: " + jsonFile);
            store(caches, jsonFile);
        }
        return caches.stream().filter(c->c.getGcName().startsWith("GC")).collect(Collectors.toList());
    }
}
