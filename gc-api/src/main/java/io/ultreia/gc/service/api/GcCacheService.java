package io.ultreia.gc.service.api;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.db.entity.GcCacheCountryStats;
import io.ultreia.gc.db.entity.GcCacheStateStats;
import io.ultreia.gc.db.entity.GcCountry;
import io.ultreia.gc.db.entity.GcDifficulty;
import io.ultreia.gc.db.entity.GcDistrict;
import io.ultreia.gc.db.entity.GcState;
import io.ultreia.gc.db.entity.GcTerrain;
import io.ultreia.java4all.http.spi.Get;
import io.ultreia.java4all.http.spi.Post;
import io.ultreia.gc.model.GcCache;
import io.ultreia.gc.db.entity.GcCacheType;
import io.ultreia.gc.model.GcUser;
import java.util.Map;
import java.util.Set;

/**
 * Created by tchemit on 18/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface GcCacheService extends GcService {

    @Post(timeOut = 60)
    int updateDistricts(String geonamesUser);

    @Get(timeOut = 60)
    Map<String, GcDistrict> acquireDistricts(String geonamesUser);

    @Post(timeOut = 60)
    void updateDistrict(String geonamesUser, String gcName);

    @Post(timeOut = 60)
    int updateElevations(String geonamesUser);

    @Get(timeOut = 60)
    Map<String, Integer> acquireElevations(String geonamesUser);

    @Post(timeOut = 60)
    void updateElevation(String geonamesUser, String gcName);

    @Post
    void reloadCache(String gcName);

    @Post
    void storeCache(GcCache cache);

    @Get
    Set<String> getGcNames();

    @Get
    Set<String> getGcNames(GcCacheType cacheType, boolean nullLastUpdate);

    @Get
    Set<String> getGcNamesWithoutLogs();

    @Get
    Set<String> searchCaches(GcSearchFilter filter);

    @Get
    int searchCachesCount(GcSearchFilter filter);

    @Get
    GcCache getCacheFromGcName(String gcName);

    @Get
    GcCache getMyCacheFromGcName(String gcName);

    @Get(timeOut = 60 * 2)
    GcCacheCountryStats getCountryStats(GcCountry country, boolean reload);

    @Get(timeOut = 60 * 2)
    GcCacheStateStats getStateStats(GcState state, boolean reload);

    @Post(timeOut = 60 * 2)
    void loadCountryStats(GcCountry country);

    @Post(timeOut = 60 * 2)
    void loadStateStats(GcState state);

    @Post(timeOut = 60 * 2)
    void computeCountryStats(GcCountry country, GcDifficulty difficulty, GcTerrain terrain);

    @Post(timeOut = 60 * 2)
    void computeStateStats(GcState state, GcDifficulty difficulty, GcTerrain terrain);

    @Post(timeOut = 10)
    void loadFrenchOwnerCachesGcNames(GcUser owner);

    @Post
    void updateCoordinates(String gcName, String lat, String lon);
}
