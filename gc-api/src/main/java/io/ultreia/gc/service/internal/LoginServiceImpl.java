package io.ultreia.gc.service.internal;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.service.api.LoginService;
import io.ultreia.gc.session.GcSessionsStore;

/**
 * Created by tchemit on 21/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LoginServiceImpl extends GcServiceSupportImpl implements LoginService {
    @Override
    public String login(String login, String password) {
        return GcSessionsStore.login(login, password);
    }

    @Override
    public void logout() {
        GcSessionsStore.logout(getServiceContext().getAuthId().orElse(null));
    }
}
