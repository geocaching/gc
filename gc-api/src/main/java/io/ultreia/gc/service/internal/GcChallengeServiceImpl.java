package io.ultreia.gc.service.internal;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.challenge.ProjectGcChallengeChecker;
import io.ultreia.gc.db.entity.GcCountry;
import io.ultreia.gc.db.entity.GcState;
import io.ultreia.gc.model.GcChallengeResult;
import io.ultreia.gc.service.api.GcChallengeService;
import io.ultreia.gc.service.api.GcSearchFilter;
import java.util.Set;

/**
 * Created by tchemit on 26/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcChallengeServiceImpl extends GcServiceSupportImpl implements GcChallengeService {

    @Override
    public GcChallengeResult runProjectGcChallenge(String gcName) {

        ProjectGcChallengeChecker checker = new ProjectGcChallengeChecker();
        checker.setServiceContext(getServiceContext());

        return checker.check(gcName);

    }

    @Override
    public Set<String> getCountryChallenges(GcCountry country) {
        GcSearchFilter filter = new GcSearchFilter();
        filter.keyword = "challenge";
        filter.country = country.code();
        return getServiceContext().newCacheService().searchCaches(filter);
    }

    @Override
    public Set<String> getStateChallenges(GcState state) {
        GcSearchFilter filter = new GcSearchFilter();
        filter.keyword = "challenge";
        filter.state = state.code();
        return getServiceContext().newCacheService().searchCaches(filter);
    }

}
