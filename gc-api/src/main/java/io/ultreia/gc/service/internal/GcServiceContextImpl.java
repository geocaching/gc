package io.ultreia.gc.service.internal;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.config.GcApiConfig;
import io.ultreia.gc.db.entity.GcDbTopiaApplicationContext;
import io.ultreia.gc.db.entity.GcDbTopiaPersistenceContext;
import io.ultreia.gc.service.api.GcService;
import io.ultreia.gc.service.api.GcServiceContext;
import io.ultreia.gc.service.api.GcServiceContextSupport;
import java.lang.reflect.Modifier;
import java.util.Date;

/**
 * Created by tchemit on 16/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcServiceContextImpl extends GcServiceContextSupport {

    private final GcDbTopiaApplicationContext dbTopiaApplicationContext;
    private final GcApiConfig config;

    public static GcServiceContext create(String authId, GcApiConfig config, GcDbTopiaApplicationContext dbTopiaApplicationContext) {
        GcServiceContext gcServiceContext = new GcServiceContextImpl(config, dbTopiaApplicationContext);
        gcServiceContext.setAuthId(authId);
        return gcServiceContext;
    }

    @Override
    public <S extends GcService> Class<S> getServiceClass(Class<S> serviceType) {

        if (!serviceType.isInterface() && !Modifier.isAbstract(serviceType.getModifiers())) {
            return serviceType;
        }
        try {
            return (Class) Class.forName(serviceType.getName().replace(".api", ".internal") + "Impl");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    GcDbTopiaPersistenceContext newPersistenceContext() {
        return dbTopiaApplicationContext.newPersistenceContext();
    }

    private GcServiceContextImpl(GcApiConfig config, GcDbTopiaApplicationContext dbTopiaApplicationContext) {
        this.config = config;
        this.dbTopiaApplicationContext = dbTopiaApplicationContext;
    }

    GcApiConfig getConfig() {
        return config;
    }

    public Date now() {
        return new Date();
    }
}
