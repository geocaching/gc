package io.ultreia.gc.service.internal;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.db.entity.GcDbTopiaPersistenceContext;
import io.ultreia.gc.service.api.GcServiceSupport;
import io.ultreia.gc.session.GcSession;
import io.ultreia.gc.session.GcSessionsStore;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HRequestBuilderFactory;
import io.ultreia.java4all.http.HResponse;
import io.ultreia.java4all.http.HRestClientService;
import java.util.Optional;

/**
 * Created by tchemit on 16/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class GcServiceSupportImpl extends GcServiceSupport implements GcUrls, HRestClientService {

    @Override
    public HRequestBuilderFactory getRequestBuilderFactory() {
        return getGcSession();
    }

    @Override
    public GcServiceContextImpl getServiceContext() {
        return (GcServiceContextImpl) super.getServiceContext();
    }

    GcSession getGcSession() {
        Optional<String> optionalAuthId = getServiceContext().getAuthId();
        if (!optionalAuthId.isPresent()) {
            throw new RuntimeException("No authId found in service context");
        }
        Optional<GcSession> session = GcSessionsStore.get().getSession(optionalAuthId.get());
        if (!session.isPresent()) {
            throw new RuntimeException("session not found for authId: " + optionalAuthId.get());
        }
        return session.get();
    }

    public GcSession getNeutralGcSession() {
        Optional<GcSession> session = GcSessionsStore.get().getNeutralSession();
        if (!session.isPresent()) {
            throw new RuntimeException("Can't find neutral session");
        }
        return session.get();
    }

    @Override
    public HResponse executeRequest(HRequest request) {
        return getGcSession().executeRequest(request);
    }

    @Override
    public HResponse executeRequest(HRequest request, int expectedStatusCode) {
        return getGcSession().executeRequest(request);
    }

    public String getUsername() {
        return getGcSession().getUsername();
    }


    protected void loginProjectGc() {
        getGcSession().loginProjectGc();
    }

    GcDbTopiaPersistenceContext newPersistenceContext() {
        return getServiceContext().newPersistenceContext();
    }
}
