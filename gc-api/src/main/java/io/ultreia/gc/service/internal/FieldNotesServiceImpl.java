package io.ultreia.gc.service.internal;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import io.ultreia.gc.db.entity.GcCacheType;
import io.ultreia.gc.db.entity.GcDbTopiaPersistenceContext;
import io.ultreia.gc.db.entity.GcUser;
import io.ultreia.gc.db.entity.GcUserTopiaDao;
import io.ultreia.gc.model.GcCacheImage;
import io.ultreia.gc.model.GcFieldNote;
import io.ultreia.gc.model.GcLogDraft;
import io.ultreia.gc.model.GcLogType;
import io.ultreia.gc.model.GcTrackable;
import io.ultreia.gc.service.api.FieldNotesService;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HRequestBuilder;
import io.ultreia.java4all.http.HResponse;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpStatus;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Created by tchemit on 16/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class FieldNotesServiceImpl extends GcServiceSupportImpl implements FieldNotesService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(FieldNotesServiceImpl.class);

    private MustacheFactory mustacheFactory;

    @Override
    public List<GcFieldNote> loadFieldNotes() {

        Document doc;
        {
            HRequest request = create(URL_GET_FIELD_NOTES).addAuthToken()
                    .addParameter("take", 100)
                    .addParameter("sortAsc", "true")
                    .get();

            HResponse build = executeRequest(request, HttpStatus.SC_OK);

            doc = build.toHtml();
        }

        int total = Integer.valueOf(doc.select("Total").text());

        log.info(String.format("Detect %d field note(s) to load.", total));

        List<GcFieldNote> fieldNotes = new LinkedList<>();
        loadFieldNotes(fieldNotes, doc);

        while (total > fieldNotes.size()) {
            HRequest request = create(URL_GET_FIELD_NOTES).addAuthToken()
                    .addParameter("take", 100)
                    .addParameter("sortAsc", "true")
                    .addParameter("skip", fieldNotes.size())
                    .get();

            HResponse build = executeRequest(request, HttpStatus.SC_OK);

            doc = build.toHtml();
            loadFieldNotes(fieldNotes, doc);
        }

        log.info("Found " + fieldNotes.size() + " field note(s).");
        fieldNotes.forEach(c -> log.debug("Cache " + c + " - " + c.getComposeUrl()));
        return fieldNotes;
    }

    private void loadFieldNotes(List<GcFieldNote> fieldNotes, Document doc) {
        for (Element tr : doc.select("LogDraftItem")) {

            String referenceCode = tr.select("ReferenceCode").text().split(" ")[0].trim();
            String guid = tr.select("Guid").text().split(" ")[0];
            String date = tr.select("DateLoggedUtc").text();
            String logType = String.valueOf(tr.select("LogTypeId").text());

            Element cache = tr.select("Geocache").first();

            String name = tr.select("Geocache > Name").text();
            String gc = cache.select("ReferenceCode").text();
            GcCacheType gcCacheType = GcCacheType.of(cache.select("GeocacheType > Id").text());

            fieldNotes.add(new GcFieldNote(name, gc, referenceCode, guid, date, logType, gcCacheType));

            log.debug(String.format("Add field note for %s:%s (%s)", gc, name, logType));
        }
    }

    @Override
    public void logFieldNote(GcFieldNote fieldNote, String logText, boolean autoVisitTrackables) {

        log.info("Prepare log for cache: " + fieldNote.getName());

        Document doc;
        {
            HRequest request = create(fieldNote.getComposeUrl()).get();

            HResponse response = executeRequest(request, HttpStatus.SC_OK);
            doc = response.toHtml();
        }

        String logId = fieldNote.getReferenceCode();
        String gcName = fieldNote.getGc();
        String date = fieldNote.getDate();
        if (date.contains("T")) {
            date = date.substring(0, date.lastIndexOf("T"));
        }
        String ownerName = doc.select("h1[class='muted'] > a:nth-child(2)").text();
        int cacheCount = Integer.valueOf(doc.select("span[class='cache-count']").text().split(" ")[0].replace(",", ""));

        log.debug("LogId: " + logId);
        log.debug("GcName: " + gcName);
        log.debug("cache count: " + cacheCount);
        log.debug("the owner: " + ownerName);

        GcLogType logType = fieldNote.getLogType();
        Objects.requireNonNull(logType);

        String imageId = null;
        Optional<GcCacheImage> image = fieldNote.getImage();
        if (image.isPresent()) {

            log.info("Upload image for cache: " + fieldNote.getName());

            GcLogDraft logDraft = getLogDraft(logId);
            GcCacheImage gcCacheImage = image.get();

            Path tempFile;
            try {
                tempFile = Files.createTempFile(gcCacheImage.getFileName(), "");
                Files.write(tempFile, gcCacheImage.getFileContent());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }

            HRequest request = create(URL_SEND_LOG_IMAGE)
                    .addAuthToken()
                    .useMultiPartForm()
                    .addParameter("guid", logDraft.getGuid())
                    .addParameter("name", gcCacheImage.getFileName())
                    .addParameter("qqtotalfilesize", gcCacheImage.getFileLength())
                    .addFile("qqfile", tempFile.toFile())
                    .post();


            HResponse response = executeRequest(request, HttpStatus.SC_CREATED);

            Document responseAsHtml = response.toHtml();

            imageId = responseAsHtml.select("guid").text();

        }

        log.info("Send log for cache: " + fieldNote.getName());
        HRequestBuilder requestBuilder = create(String.format(URL_SEND_LOG, gcName))
                .addAuthToken()
                .addHiddenInputs(doc)
                .addParameter("geocache[referenceCode]", gcName)
                .addParameter("logType", logType.getLogType())
                .addParameter("logDate", date)
                .addParameter("referenceCode", logId)
                .addParameter("logText", getLogText(logText, ownerName, cacheCount, logType.isIncreaseFoundItCount()))
                .addParameter("addedFavorite", fieldNote.isFavorite());

        if (autoVisitTrackables) {
            for (GcTrackable trackable : getServiceContext().newTrackableService().getTrackables()) {
                requestBuilder.addParameter("trackableActivity[" + trackable.getReferenceCode() + "]", "75");
            }
        }

        String realLogId;

        {
            log.info("Send log for cache: " + fieldNote.getName());

            HRequest request = requestBuilder.post();
            HResponse response = executeRequest(request, HttpStatus.SC_OK);

            realLogId = response.toHtml().select("geocachelog > ReferenceCode").text();
            log.info("Real log id: " + realLogId);
        }

        Objects.requireNonNull(realLogId);

        if (imageId != null) {

            log.info("Attach image to log for cache: " + fieldNote.getName());

            HRequest request = create(String.format(URL_ATTACH_IMAGE_TO_LOG, imageId, realLogId))
                    .addAuthToken().patch();
            executeRequest(request, HttpStatus.SC_OK);
        }


        {
            log.info("Delete field note for cache: " + fieldNote.getName());

            HRequest request = create(String.format(URL_DELETE_LOG_DRAFT, logId)).addAuthToken().delete();
            executeRequest(request, HttpStatus.SC_OK);
        }

        // load cache after all
        try {
            getServiceContext().newCacheService().getCacheFromGcName(gcName);
        } catch (Exception e) {
            log.error(String.format("Could not load cache: %s", gcName), e);
        }

    }

    @Override
    public GcLogDraft getLogDraft(String referenceCode) {

        HRequest request = create(String.format(URL_GET_LOG_DRAFT, referenceCode))
                .addAuthToken()
                .get();

        HResponse response = executeRequest(request, HttpStatus.SC_OK);
        Document responseAsHtml = response.toHtml();
        return GcLogDraft.fromXml(responseAsHtml);
    }

    @Override
    public void deleteFieldNote(String draftGuid) {
        HRequest request = create(String.format(URL_DELETE_LOG_DRAFT, draftGuid))
                .addAuthToken()
                .delete();

        executeRequest(request, HttpStatus.SC_OK);
    }

    @Override
    public String computeCommon(String common) {
        try (GcDbTopiaPersistenceContext persistenceContext = getServiceContext().newPersistenceContext()) {
            GcUserTopiaDao dao = persistenceContext.getGcUserDao();
            GcUser user1 = dao.forNameEquals(getOne()).findUnique();
            GcUser user2 = dao.forNameEquals(getTwo()).findUnique();
            if (common.isEmpty()) {
                return user1.getComment() + "--" + user2.getComment();
            } else {
                user2.setComment(common);
                persistenceContext.commit();
                return null;
            }
        }
    }

    private String getLogText(String logText, String theOwner, int cacheCount, boolean increaseFoundItCount) {
        Mustache mustache = getMustacheFactory().compile(new StringReader(logText), "archeo");

        Map<String, Object> mustacheModel = new HashMap<>();
        mustacheModel.put("found", cacheCount + (increaseFoundItCount ? 1 : 0));
        mustacheModel.put("owner", theOwner);

        try (StringWriter logTextWriter = new StringWriter()) {
            mustache.execute(logTextWriter, mustacheModel);
            logTextWriter.flush();
            return logTextWriter.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private MustacheFactory getMustacheFactory() {
        if (mustacheFactory == null) {
            mustacheFactory = new DefaultMustacheFactory();
        }
        return mustacheFactory;
    }

    private static final String ONE = "jz0ZPQDJwcU";
    private static final String TWO = "H0rIWghgc3s";
    private String one;
    private String two;

    public String getOne() {
        if (one == null) {
            one = getServiceContext().getConfig().getSecurityHelper().decrypt(ONE);
        }
        return one;
    }

    public String getTwo() {
        if (two == null) {
            two = getServiceContext().getConfig().getSecurityHelper().decrypt(TWO);
        }
        return two;
    }
}
