package io.ultreia.gc.service.internal.db;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.db.entity.GcCache;
import io.ultreia.gc.db.entity.GcCacheLog;
import io.ultreia.gc.db.entity.GcDbTopiaPersistenceContext;
import io.ultreia.gc.db.entity.GcEntityListLoader;
import io.ultreia.gc.service.internal.GcServiceSupportImpl;
import io.ultreia.gc.session.GcSession;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HResponse;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


import static io.ultreia.gc.service.internal.GcUrls.URL_CACHE;
import static io.ultreia.gc.service.internal.GcUrls.URL_GET_LOGBOOK;

/**
 * Created by tchemit on 27/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcLogsLoader implements GcEntityListLoader<GcCacheLog> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcLogsLoader.class);

    private final GcSession gcSession;
    private final boolean updateLogs;
    private final GcCacheLoader cacheLoader;
    private final GcDbTopiaPersistenceContext persistenceContext;
    private DateConverter dateConverter = new DateConverter();


    public GcLogsLoader(GcServiceSupportImpl service, GcDbTopiaPersistenceContext persistenceContext, boolean updateLogs) {
        this.gcSession = service.getNeutralGcSession();
        this.updateLogs = updateLogs;
        this.cacheLoader = new GcCacheLoader(service, persistenceContext, true);
        this.persistenceContext = persistenceContext;
    }

    @Override
    public List<GcCacheLog> load(String gcName) {

        GcCache gcCache = cacheLoader.load(gcName);

        List<GcCacheLog> logs = gcCache.getLogs();
        if (logs.isEmpty()) {

            log.info(String.format("Acquire logs for cache: %s", gcName));

            String userToken = getUserToken(gcName);
            logs = loadLogs(userToken, gcCache);

        } else {

            int logsSize = logs.size();
            log.info(String.format("Loaded %d log(s) for cache: %s", logsSize, gcName));

            if (updateLogs) {

                String userToken = getUserToken(gcName);
                int realLogsSize = getLogsSize(userToken);
                int toUpdate = realLogsSize - logsSize;
                if (toUpdate != 0) {

                    log.info(String.format("Update %s log(s) for cache: %s", toUpdate, gcName));

                    logs = loadLogs(userToken, gcCache);

                } else {
                    log.info(String.format("%s log(s) are up-to-date for cache: %s", logsSize, gcName));
                }
            }
        }

        logs.sort(Comparator.comparing(GcCacheLog::getLogDate));
        Collections.reverse(logs);
        return logs;
    }

    private List<GcCacheLog> loadLogs(String userToken, GcCache gcCache) {
        List<GcCacheLog> logs = new LinkedList<>();
        Iterator<GcCacheLog> logIterator = newGcCacheLogIterator(userToken, 50);

        logIterator.forEachRemaining(logs::add);

        gcCache.setLogs(logs);
        log.info(String.format("Persist %d log(s) for cache: %s", logs.size(), gcCache.getGcName()));
        persistenceContext.commit();
        return logs;
    }

    private int getLogsSize(String userToken) {
        HRequest request = gcSession.create(URL_GET_LOGBOOK)
                .addParameter("tkn", userToken)
                .addParameter("idx", "1")
                .addParameter("num", "1")
                .get();
        HResponse response = gcSession.executeRequest(request);

        Map<String, Object> hashMap = response.toJson();
        return ((Double) ((Map) hashMap.get("pageInfo")).get("totalRows")).intValue();

    }

    private Iterator<GcCacheLog> newGcCacheLogIterator(String userToken, int pageSize) {
        return new Iterator<GcCacheLog>() {

            int pageIndex = 0;
            int pageTotal = 1;
            Iterator<Map> buffer;

            @Override
            public boolean hasNext() {

                if (getBuffer(true).hasNext()) {
                    return true;
                }

                // buffer is consumed
                if (pageIndex == pageTotal) {

                    // no more page to load
                    return false;
                }

                // ask a new buffer
                this.buffer = null;
                return getBuffer(false).hasNext();
            }

            @Override
            public GcCacheLog next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }

                Map next = buffer.next();
                return fromMap(next);
            }

            Iterator<Map> getBuffer(boolean computePageTotal) {
                if (buffer == null) {

                    pageIndex++;

                    HRequest request = gcSession.create(URL_GET_LOGBOOK)
                            .addParameter("tkn", userToken)
                            .addParameter("idx", pageIndex + "")
                            .addParameter("num", pageSize + "")
                            .get();
                    HResponse response = gcSession.executeRequest(request);

                    Map<String, Object> hashMap = response.toJson();
                    if (computePageTotal) {
                        pageTotal = (int) (((Double) ((Map) hashMap.get("pageInfo")).get("totalRows")) / pageSize) + 1;
                    }
                    List<Map> data = (List) hashMap.get("data");
                    buffer = data.iterator();
                }
                return buffer;
            }

            GcCacheLog fromMap(Map map) {
                GcCacheLog result = new GcCacheLog();
                result.setLogId(((Double) map.get("LogID")).intValue());
                result.setLogGuid((String) map.get("LogGuid"));
                result.setLogType((String) map.get("LogType"));
                result.setLogText((String) map.get("LogText"));

                String visited = (String) map.get("Visited");
                result.setLogDate(dateConverter.convert(visited));

                String userId = ((Double) map.get("AccountID")).intValue() + "";
                result.setUser((String) map.get("UserName"));
                result.setUserId(userId);
                return result;
            }
        };
    }

    private String getUserToken(String gcName) {
        HRequest request = gcSession.create(String.format(URL_CACHE, gcName)).get();
        HResponse build = gcSession.executeRequest(request);

        Document doc = build.toHtml();

        String userToken = null;

        for (Element element : doc.select("script[type='text/javascript']")) {
            String text = element.data();
            if (text.contains("userToken = '")) {
                int i = text.indexOf("userToken =");
                int start = text.indexOf("'", i) + 1;
                int end = text.indexOf("'", start);
                userToken = text.substring(start, end);
                log.debug("User token: " + userToken);
                break;
            }
        }

        Objects.requireNonNull(userToken);
        return userToken;
    }

}
