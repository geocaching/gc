package io.ultreia.gc.service.internal;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.db.entity.GcCacheTopiaDao;
import io.ultreia.gc.db.entity.GcCountry;
import io.ultreia.gc.db.entity.GcDbTopiaPersistenceContext;
import io.ultreia.gc.model.GcUser;
import io.ultreia.gc.service.api.GcUserService;
import io.ultreia.gc.service.internal.db.GcUserLoader;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HResponse;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Created by tchemit on 15/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcUserServiceImpl extends GcServiceSupportImpl implements GcUserService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcUserServiceImpl.class);

    @Override
    public Set<GcUser> getFrenchOwners(boolean updateCachesOwnedInFrance) {

        HRequest request = create("http://www.mides.fr/classement-placeurs-geocaches-en-france").get();

        HResponse response = executeRequest(request);
        Document responseAsHtml = response.toHtml();

        Set<GcUser> result = new LinkedHashSet<>();
        int i = 0;
        for (Element row : responseAsHtml.select("table[class='table'] > tbody > tr")) {

            if (i++ == 0) {
                continue;
            }
            int count = Integer.valueOf(row.select("td[class='nbx']").text());
            for (Element a : row.select("a")) {

                int ownerId = Integer.valueOf(a.attr("href").split("=")[2]);
                String ownerName = a.text();
                result.add(new GcUser(ownerName, ownerId, count));
            }
        }

        MutableInt index = new MutableInt();
        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            GcCacheTopiaDao cacheDao = persistenceContext.getGcCacheDao();
            try (GcUserLoader userLoader = new GcUserLoader(this, persistenceContext, false, true)) {
                result.parallelStream().forEach(gcOwner -> {
                    log.info(String.format("[%d/%d] Load user: %s", index.incrementAndGet(), result.size(), gcOwner.getUserName()));
                    io.ultreia.gc.db.entity.GcUser gcUser = userLoader.load(gcOwner.getUserId() + "");
                    if (updateCachesOwnedInFrance && gcUser.getCachesOwnedInFrance() == 0) {
                        long count = cacheDao.forProperties(io.ultreia.gc.db.entity.GcCache.PROPERTY_OWNER, gcUser, io.ultreia.gc.db.entity.GcCache.PROPERTY_COUNTRY, GcCountry.France).count();
                        gcUser.setCachesOwnedInFrance((int) count);
                    }
                });
            }
            persistenceContext.commit();
        }


        return result;
    }

    @Override
    public void updateFrenchOwner(String userId) {
        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {

            io.ultreia.gc.db.entity.GcUser user = new GcUserLoader(this, persistenceContext, false, false).load(userId);
            long count = persistenceContext.getGcCacheDao().forProperties(io.ultreia.gc.db.entity.GcCache.PROPERTY_OWNER, user, io.ultreia.gc.db.entity.GcCache.PROPERTY_COUNTRY, GcCountry.France).count();
            user.setCachesOwnedInFrance((int) count);
            persistenceContext.commit();
        }
    }

    @Override
    public Optional<GcUser> getUser(String userName) {
        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {

            io.ultreia.gc.db.entity.GcUser user = new GcUserLoader(this, persistenceContext, true, false).load(userName);
            return Optional.ofNullable(user == null ? null : GcUser.fromEntity(user));
        }
    }

    @Override
    public Set<GcUser> getUsers() {
        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            return persistenceContext.getGcUserDao().findAll().stream().map(GcUser::fromEntity).collect(Collectors.toSet());
        }
    }

}
