package io.ultreia.gc.service.internal;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.ultreia.gc.db.entity.GcCacheCountryStats;
import io.ultreia.gc.db.entity.GcCacheCountryStatsTopiaDao;
import io.ultreia.gc.db.entity.GcCacheRefWithCoordinates;
import io.ultreia.gc.db.entity.GcCacheStateStats;
import io.ultreia.gc.db.entity.GcCacheStateStatsTopiaDao;
import io.ultreia.gc.db.entity.GcCacheTopiaDao;
import io.ultreia.gc.db.entity.GcCacheType;
import io.ultreia.gc.db.entity.GcCountry;
import io.ultreia.gc.db.entity.GcDbTopiaPersistenceContext;
import io.ultreia.gc.db.entity.GcDifficulty;
import io.ultreia.gc.db.entity.GcDistrict;
import io.ultreia.gc.db.entity.GcState;
import io.ultreia.gc.db.entity.GcTerrain;
import io.ultreia.gc.db.entity.GcUserTopiaDao;
import io.ultreia.gc.model.GcCache;
import io.ultreia.gc.model.GcUser;
import io.ultreia.gc.service.TooMuchResultException;
import io.ultreia.gc.service.api.GcCacheService;
import io.ultreia.gc.service.api.GcSearchFilter;
import io.ultreia.gc.service.internal.db.GcCacheBinder;
import io.ultreia.gc.service.internal.db.GcCacheLoader;
import io.ultreia.gc.tool.LoadForCountryStats;
import io.ultreia.gc.tool.LoadForStateStats;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HRequestBuilder;
import io.ultreia.java4all.http.HResponse;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpStatus;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Created by tchemit on 18/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcCacheServiceImpl extends GcServiceSupportImpl implements GcCacheService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcCacheServiceImpl.class);

    public void storeCaches(List<GcCache> caches, File output) {

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (BufferedWriter writer = Files.newBufferedWriter(output.toPath())) {
            gson.toJson(caches, writer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public List<GcCache> loadCaches(File output) {

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (BufferedReader reader = Files.newBufferedReader(output.toPath())) {
            Type type = new TypeToken<List<GcCache>>() {

            }.getType();
            return gson.fromJson(reader, type);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private Optional<GcDistrict> updateDistrict(String geonamesUser, GcCacheRefWithCoordinates gcCache) {

        double lat = gcCache.getLat();
        double lon = gcCache.getLng();
        log.info(String.format("Lookup for district of french cache: %s (lat: %f lng: %f)", gcCache.getGcName(), lat, lon));

        Document document = executeRequest(create("http://api.geonames.org/findNearbyPostalCodes")
                                                   .addParameter("lat", lat)
                                                   .addParameter("lng", lon)
                                                   .addParameter("username", geonamesUser)
                                                   .get(), HttpStatus.SC_OK).toHtml();

        boolean match = false;
        for (Element element : document.select("code")) {
            match = true;
            if (element.select("countryCode").text().equals("FR")) {

                String districtCode = "FR_" + element.select("adminCode2").text();
                GcDistrict gcDistrict = GcDistrict.valueOf(districtCode);
                log.info(String.format("Found district %s of french cache: %s (lat: %f lng: %f)", gcDistrict, gcCache.getGcName(), lat, lon));
                return Optional.of(gcDistrict);
            }

        }

        if (!match && !document.toString().contains("<geonames />")) {
            throw new IllegalStateException(String.format("Can't get more results for cache: %s...\n%s", gcCache.getGcName(), document.toString()));
        }

        getBadDistrictGcs().add(gcCache);

        return Optional.empty();
    }

    private Optional<Integer> updateElevation(String geonamesUser, GcCacheRefWithCoordinates gcCache) {

        double lat = gcCache.getLat();
        double lon = gcCache.getLng();
        log.info(String.format("Lookup for elevation for cache: %s (lat: %f lng: %f)", gcCache.getGcName(), lat, lon));

        HResponse response = executeRequest(create("http://api.geonames.org/srtm1")
                                                    .addParameter("lat", lat)
                                                    .addParameter("lng", lon)
                                                    .addParameter("username", geonamesUser)
                                                    .get(), HttpStatus.SC_OK);

        try {
            int elevation = response.toInt();
            return Optional.of(elevation);
        } catch (Exception e) {
            throw new IllegalStateException(String.format("Can't get more results for cache: %s...\n%s", gcCache.getGcName(), response.toString()));
        }

    }

    @Override
    public void updateDistrict(String geonamesUser, String gcName) {

        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            io.ultreia.gc.db.entity.GcCache gcCache = persistenceContext.getGcCacheDao().forGcNameEquals(gcName).findUnique();

            if (gcCache.getDistrict() == null) {
                Optional<GcDistrict> gcDistrict = updateDistrict(geonamesUser, new GcCacheRefWithCoordinates(gcCache));
                if (gcDistrict.isPresent()) {
                    gcCache.setDistrict(gcDistrict.get());
                    persistenceContext.commit();
                }
            }
        }
    }

    @Override
    public int updateElevations(String geonamesUser) {
        Set<GcCacheRefWithCoordinates> cacheRefWithCoordinates;
        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            cacheRefWithCoordinates = persistenceContext.getGcCacheDao().findGcNamesWithoutElevation();
        }

        Set<GcCacheRefWithCoordinates> badGcs = getBadElevationGcs();
        cacheRefWithCoordinates.removeIf(badGcs::contains);

        int size = cacheRefWithCoordinates.size();

        log.info(String.format("Found %s cache(s).", size));

        int badGcsSize = badGcs.size();
        Map<String, Integer> toUpdate = acquireElevations(geonamesUser, cacheRefWithCoordinates, badGcs);

        if (!toUpdate.isEmpty()) {

            log.info(String.format("Persist elevation for %d cache(s).", toUpdate.size()));

            try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
                persistenceContext.getGcCacheDao().updateElevations(toUpdate);
                persistenceContext.commit();
            }
        }

        if (badGcs.size() > badGcsSize) {
            storeBadElevationGcs(badGcs);
        }
        return toUpdate.size();
    }

    @Override
    public Map<String, Integer> acquireElevations(String geonamesUser) {
        Set<GcCacheRefWithCoordinates> cacheRefWithCoordinates;
        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            cacheRefWithCoordinates = persistenceContext.getGcCacheDao().findGcNamesWithoutElevation();
        }

        Set<GcCacheRefWithCoordinates> badGcs = getBadElevationGcs();

        cacheRefWithCoordinates.removeIf(badGcs::contains);

        return acquireElevations(geonamesUser, cacheRefWithCoordinates, badGcs);
    }

    private Map<String, Integer> acquireElevations(String geonamesUser, Set<GcCacheRefWithCoordinates> cacheRefWithCoordinates, Set<GcCacheRefWithCoordinates> badGcs) {

        int badGcSize = badGcs.size();

        cacheRefWithCoordinates.removeIf(badGcs::contains);

        int size = cacheRefWithCoordinates.size();

        log.info(String.format("Found %s cache(s).", size));

        MutableInt i = new MutableInt();

        MutableBoolean b = new MutableBoolean();
        Map<String, Integer> toUpdate = new TreeMap<>();
        cacheRefWithCoordinates.parallelStream().forEach(cacheRefWithCoordinate -> {

            if (b.booleanValue()) {
                return;
            }

            log.info(String.format("[%d/%d] Update elevation for cache %s", i.incrementAndGet(), size, cacheRefWithCoordinate.getGcName()));
            try {
                Optional<Integer> gcDistrict = updateElevation(geonamesUser, cacheRefWithCoordinate);
                if (gcDistrict.isPresent()) {
                    toUpdate.put(cacheRefWithCoordinate.getId(), gcDistrict.get());
                } else {
                    badGcs.add(cacheRefWithCoordinate);
                }
                gcDistrict.ifPresent(d -> toUpdate.put(cacheRefWithCoordinate.getId(), d));

            } catch (Exception e) {
                log.error("Could not get elevation for cache " + cacheRefWithCoordinate.getGcName(), e);
                b.setTrue();
            }

        });

        if (badGcs.size() > badGcSize) {
            storeBadElevationGcs(badGcs);

        }
        return toUpdate;
    }

    @Override
    public void updateElevation(String geonamesUser, String gcName) {

        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            io.ultreia.gc.db.entity.GcCache gcCache = persistenceContext.getGcCacheDao().forGcNameEquals(gcName).findUnique();

            if (gcCache.getDistrict() == null) {
                Optional<Integer> gcDistrict = updateElevation(geonamesUser, new GcCacheRefWithCoordinates(gcCache));
                if (gcDistrict.isPresent()) {
                    gcCache.setElevation(gcDistrict.get());
                    persistenceContext.commit();
                }
            }
        }
    }

    @Override
    public void reloadCache(String gcName) {

        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {

            GcCacheLoader cacheLoader = new GcCacheLoader(this, persistenceContext, false);

            io.ultreia.gc.db.entity.GcCache dbCache = cacheLoader.acquire(gcName, false);
            Objects.requireNonNull(dbCache);
            dbCache.setLastUpdate(getServiceContext().now());

            persistenceContext.commit();
        }
    }

    @Override
    public int updateDistricts(String geonamesUser) {

        Set<GcCacheRefWithCoordinates> cacheRefWithCoordinates;
        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            cacheRefWithCoordinates = persistenceContext.getGcCacheDao().findFrenchGcNamesWithoutDistrict();
        }
        Set<GcCacheRefWithCoordinates> badGcs = getBadDistrictGcs();
        cacheRefWithCoordinates.removeIf(badGcs::contains);

        int size = cacheRefWithCoordinates.size();

        log.info(String.format("Found %s french cache(s).", size));

        int badGcsSize = badGcs.size();
        Map<String, GcDistrict> toUpdate = acquireDistricts(geonamesUser, cacheRefWithCoordinates, badGcs);

        if (!toUpdate.isEmpty()) {

            log.info(String.format("Persist district for %d cache(s).", toUpdate.size()));

            try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
                persistenceContext.getGcCacheDao().updateDistricts(toUpdate);
                persistenceContext.commit();
            }
        }

        if (badGcs.size() > badGcsSize) {
            storeBadDistrictGcs(badGcs);
        }
        return toUpdate.size();

    }

    private void storeBadDistrictGcs(Set<GcCacheRefWithCoordinates> badGcs) {
        Path path = getServiceContext().getConfig().getDataDirectory().toPath().resolve("bad_districts.json");

        log.info(String.format("Store %d bad gc districts to %s", badGcs.size(), path));
        try (BufferedWriter writer = Files.newBufferedWriter(path)) {

            new GsonBuilder().create().toJson(badGcs, writer);

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private void storeBadElevationGcs(Set<GcCacheRefWithCoordinates> badGcs) {
        Path path = getServiceContext().getConfig().getDataDirectory().toPath().resolve("bad_elevations.json");

        log.info(String.format("Store %d bad gc elevation to %s", badGcs.size(), path));
        try (BufferedWriter writer = Files.newBufferedWriter(path)) {

            new GsonBuilder().create().toJson(badGcs, writer);

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private Set<GcCacheRefWithCoordinates> badDistrictGcs;
    private Set<GcCacheRefWithCoordinates> badElevationGcs;

    private Set<GcCacheRefWithCoordinates> getBadDistrictGcs() {
        if (badDistrictGcs == null) {
            Path badDistricts = getServiceContext().getConfig().getDataDirectory().toPath().resolve("bad_districts.json");


            if (Files.exists(badDistricts))
                try (BufferedReader reader = Files.newBufferedReader(badDistricts)) {

                    badDistrictGcs = new GsonBuilder().create().fromJson(reader, new TypeToken<Set<GcCacheRefWithCoordinates>>() {
                    }.getType());
                    log.info(String.format("Loaded %d bad gc districts from %s", badDistrictGcs.size(), badDistricts));

                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            else {
                badDistrictGcs = new HashSet<>();
            }
        }
        return badDistrictGcs;
    }

    private Set<GcCacheRefWithCoordinates> getBadElevationGcs() {
        if (badElevationGcs == null) {
            Path badElevations = getServiceContext().getConfig().getDataDirectory().toPath().resolve("bad_elevations.json");


            if (Files.exists(badElevations))
                try (BufferedReader reader = Files.newBufferedReader(badElevations)) {

                    badElevationGcs = new GsonBuilder().create().fromJson(reader, new TypeToken<Set<GcCacheRefWithCoordinates>>() {
                    }.getType());
                    log.info(String.format("Loaded %d bad gc elevation from %s", badElevationGcs.size(), badElevations));

                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            else {
                badElevationGcs = new HashSet<>();
            }
        }
        return badElevationGcs;
    }

    @Override
    public Map<String, GcDistrict> acquireDistricts(String geonamesUser) {
        Set<GcCacheRefWithCoordinates> cacheRefWithCoordinates;
        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            cacheRefWithCoordinates = persistenceContext.getGcCacheDao().findFrenchGcNamesWithoutDistrict();
        }

        Set<GcCacheRefWithCoordinates> badGcs = getBadDistrictGcs();

        cacheRefWithCoordinates.removeIf(badGcs::contains);
        return acquireDistricts(geonamesUser, cacheRefWithCoordinates, badGcs);

    }

    private Map<String, GcDistrict> acquireDistricts(String geonamesUser, Set<GcCacheRefWithCoordinates> cacheRefWithCoordinates, Set<GcCacheRefWithCoordinates> badGcs) {

        int badGcSize = badGcs.size();

        int size = cacheRefWithCoordinates.size();

        log.info(String.format("Found %s french cache(s).", size));

        MutableInt i = new MutableInt();

        MutableBoolean b = new MutableBoolean();
        Map<String, GcDistrict> toUpdate = new TreeMap<>();
        cacheRefWithCoordinates.parallelStream().forEach(cacheRefWithCoordinate -> {

            if (b.booleanValue()) {
                return;
            }

            log.info(String.format("[%d/%d] Update district for cache %s", i.incrementAndGet(), size, cacheRefWithCoordinate.getGcName()));
            try {
                Optional<GcDistrict> gcDistrict = updateDistrict(geonamesUser, cacheRefWithCoordinate);
                if (gcDistrict.isPresent()) {
                    toUpdate.put(cacheRefWithCoordinate.getId(), gcDistrict.get());
                } else {
                    badGcs.add(cacheRefWithCoordinate);
                }
                gcDistrict.ifPresent(d -> toUpdate.put(cacheRefWithCoordinate.getId(), d));

            } catch (Exception e) {
                log.error("Could not get district for cache " + cacheRefWithCoordinate.getGcName(), e);
                b.setTrue();
            }

        });

        if (badGcs.size() > badGcSize) {
            storeBadDistrictGcs(badGcs);

        }
        return toUpdate;
    }


    @Override
    public void storeCache(GcCache cache) {

        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            io.ultreia.gc.db.entity.GcCache gcCache = persistenceContext.getGcCacheDao().forGcNameEquals(cache.getGcName()).tryFindUnique().orNull();

            if (gcCache == null) {
                gcCache = persistenceContext.getGcCacheDao().newInstance();
            }
            GcCacheBinder gcCacheBinder = new GcCacheBinder(this, persistenceContext);
            gcCacheBinder.toEntity(cache, gcCache);
            gcCache.setLastUpdate(getServiceContext().now());

            if (gcCache.getTopiaId() == null) {
                persistenceContext.getGcCacheDao().create(gcCache);
            } else {
                persistenceContext.getGcCacheDao().update(gcCache);
            }
            gcCacheBinder.addAttributes(cache, gcCache);

            persistenceContext.getGcUserDao().consolidate(gcCache.getOwner());

            persistenceContext.commit();
        }
    }

    @Override
    public Set<String> getGcNames() {
        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            return persistenceContext.getGcCacheDao().findAllGcNames();
        }
    }

    @Override
    public Set<String> getGcNames(GcCacheType cacheType, boolean nullLastUpdate) {

        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            return persistenceContext.getGcCacheDao().findAllGcNames(cacheType.getLabel(), true);
        }
    }

    public Set<String> getFrenchGcNames() {
        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            return persistenceContext.getGcCacheDao().findAllFrenchGcNames();
        }
    }

    @Override
    public Set<String> getGcNamesWithoutLogs() {
        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            return persistenceContext.getGcCacheDao().findAllGcNamesWithoutLogs();
        }
    }

    @Override
    public Set<String> searchCaches(GcSearchFilter filter) {

        Document doc = getSearchFirstPage(filter);

        int nbCaches = getSearchCount(doc);

        if (nbCaches > 1000) {
            throw new TooMuchResultException(nbCaches);
        }

        log.info(String.format("Searching %d cache(s).", nbCaches));
        Set<String> result = new LinkedHashSet<>();
        for (Element element : doc.select("table[id='searchResultsTable'] > tbody>tr")) {
            String gcName = element.attr("data-id");
            result.add(gcName);
        }
        while (result.size() < nbCaches) {

            log.info(String.format("Searching %d out of %d cache(s).", result.size(), nbCaches));

            HRequestBuilder requestBuilder = create(URL_SEARCH_MORE_RESULTS)
                    .addParameter("ot", 4)
                    .addParameter("selectAll", false)
                    .addParameter("startIndex", result.size());
            addFilter(requestBuilder, filter);

            HRequest request = requestBuilder.get();
            HResponse build = executeRequest(request, HttpStatus.SC_OK);

            String htmlString = (String) build.toJson().get("HtmlString");
            for (Element element : Jsoup.parse("<html><body><table>" + htmlString + "</table></body></html>").select("tr")) {
                String gcName = element.attr("data-id");
                result.add(gcName);
            }

        }
        return result;
    }

    @Override
    public int searchCachesCount(GcSearchFilter filter) {
        Document doc = getSearchFirstPage(filter);
        return getSearchCount(doc);
    }

    private Integer getSearchCount(Document doc) {
        return Integer.valueOf(doc.select("form[id='searchField'] > div > div > h1[class='controls-header']").text().split(" ")[0].replace(",", ""));
    }

    private Document getSearchFirstPage(GcSearchFilter filter) {
        Document doc;
        {
            HRequestBuilder requestBuilder = create(URL_SEARCH);
            addFilter(requestBuilder, filter);
            HRequest request = requestBuilder.get();
            HResponse response = executeRequest(request, HttpStatus.SC_OK);
            doc = response.toHtml();
        }
        return doc;
    }

    private void addFilter(HRequestBuilder requestBuilder, GcSearchFilter filter) {
        if (filter.keyword != null) {
            requestBuilder.addParameter("kw", filter.keyword);
        }
        if (filter.difficulty != null) {
            requestBuilder.addParameter("d", filter.difficulty.getCode());
        }
        if (filter.terrain != null) {
            requestBuilder.addParameter("t", filter.terrain.getCode());
        }

        if (filter.own) {
            requestBuilder.addParameter("o", 1);
        } else {
            requestBuilder.addParameter("o", 2);
        }
        if (filter.found) {
            requestBuilder.addParameter("f", 1);
        } else {
            requestBuilder.addParameter("f", 2);
        }
        if (filter.country != null) {
            requestBuilder.addParameter("c", filter.country);
        }
        if (filter.state != null) {
            requestBuilder.addParameter("r", filter.state);
        }
    }

    @Override
    public GcCache getCacheFromGcName(String gcName) {

        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            return GcCache.fromEntity(new GcCacheLoader(this, persistenceContext, true).load(gcName));
        }

    }

    @Override
    public GcCache getMyCacheFromGcName(String gcName) {
        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            return new GcCacheLoader(this, getGcSession(), persistenceContext, false).get(gcName);
        }
    }

    @Override
    public GcCacheCountryStats getCountryStats(GcCountry country, boolean reload) {

        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            GcCacheCountryStatsTopiaDao dao = persistenceContext.getGcCacheCountryStatsDao();
            if (reload) {
                loadCountryStats(country);
            }
            GcCacheCountryStats result = dao.newInstance();
            dao.forCountryEquals(country).findAll().forEach(s -> {
                result.setCurrent(result.getCurrent() + s.getCurrent());
                result.setTotal(result.getTotal() + s.getTotal());
            });
            return result;
        }
    }

    @Override
    public GcCacheStateStats getStateStats(GcState state, boolean reload) {
        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            GcCacheStateStatsTopiaDao dao = persistenceContext.getGcCacheStateStatsDao();
            if (reload) {
                loadStateStats(state);
            }
            GcCacheStateStats result = dao.newInstance();
            dao.forStateEquals(state).findAll().forEach(s -> {
                result.setCurrent(result.getCurrent() + s.getCurrent());
                result.setTotal(result.getTotal() + s.getTotal());
            });
            return result;
        }
    }

    @Override
    public void loadCountryStats(GcCountry country) {
        new LoadForCountryStats(getServiceContext(), country).run();
    }

    @Override
    public void loadStateStats(GcState state) {
        new LoadForStateStats(getServiceContext(), state).run();
    }

    @Override
    public void computeCountryStats(GcCountry country, GcDifficulty difficulty, GcTerrain terrain) {

        String prefix = String.format("[%s D%s T%s]", country, difficulty, terrain);

        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {

            GcDifficulty gcDifficulty = GcDifficulty.valueOf(difficulty.name());
            GcTerrain gcTerrain = GcTerrain.valueOf(terrain.name());

            GcCountry gcCountry = GcCountry.of(country.code());

            GcCacheTopiaDao dao = persistenceContext.getGcCacheDao();
            int current = (int) dao.forProperties(
                    io.ultreia.gc.db.entity.GcCache.PROPERTY_DIFFICULTY, gcDifficulty,
                    io.ultreia.gc.db.entity.GcCache.PROPERTY_TERRAIN, gcTerrain,
                    io.ultreia.gc.db.entity.GcCache.PROPERTY_COUNTRY, gcCountry)
                    .count();

            log.info(String.format("%s found %d cache(s) in db.", prefix, current));

            GcSearchFilter filter = new GcSearchFilter();
            filter.terrain = terrain;
            filter.difficulty = difficulty;
            filter.country = country.code();
            int notFoundCount = searchCachesCount(filter);
            log.info(String.format("%s found %d cache(s) I did not found.", prefix, notFoundCount));
            filter.found = true;
            int foundCount = searchCachesCount(filter);
            log.info(String.format("%s found %d cache(s) I did found.", prefix, foundCount));
            int total = notFoundCount + foundCount;

            log.info(String.format("%s found %d cache(s).", prefix, total));

            GcCacheCountryStatsTopiaDao statsDao = persistenceContext.getGcCacheCountryStatsDao();
            GcCacheCountryStats stats = statsDao.forProperties(
                    GcCacheCountryStats.PROPERTY_COUNTRY, gcCountry,
                    GcCacheCountryStats.PROPERTY_DIFFICULTY, gcDifficulty,
                    GcCacheCountryStats.PROPERTY_TERRAIN, gcTerrain

            ).tryFindUnique().orNull();
            if (stats == null) {
                statsDao.create(GcCacheCountryStats.PROPERTY_COUNTRY, gcCountry,
                                GcCacheCountryStats.PROPERTY_DIFFICULTY, gcDifficulty,
                                GcCacheCountryStats.PROPERTY_TERRAIN, gcTerrain,
                                GcCacheCountryStats.PROPERTY_CURRENT, current,
                                GcCacheCountryStats.PROPERTY_TOTAL, total
                );
            } else {
                stats.setCurrent(current);
                stats.setTotal(total);
            }
            persistenceContext.commit();
        }

    }

    @Override
    public void computeStateStats(GcState state, GcDifficulty difficulty, GcTerrain terrain) {

        String prefix = String.format("[%s D%s T%s]", state, difficulty, terrain);

        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {

            GcDifficulty gcDifficulty = GcDifficulty.valueOf(difficulty.name());
            GcTerrain gcTerrain = GcTerrain.valueOf(terrain.name());

            GcState gcState = GcState.of(state.code());


            GcCacheTopiaDao dao = persistenceContext.getGcCacheDao();
            int current = (int) dao.forProperties(
                    io.ultreia.gc.db.entity.GcCache.PROPERTY_DIFFICULTY, gcDifficulty,
                    io.ultreia.gc.db.entity.GcCache.PROPERTY_TERRAIN, gcTerrain,
                    io.ultreia.gc.db.entity.GcCache.PROPERTY_STATE, gcState)
                    .count();

            log.info(String.format("%s found %d cache(s) in db.", prefix, current));


            GcSearchFilter filter = new GcSearchFilter();
            filter.terrain = terrain;
            filter.difficulty = difficulty;
            filter.state = state.code();
            int notFoundCount = searchCachesCount(filter);
            log.info(String.format("%s found %d cache(s) I did not found.", prefix, notFoundCount));
            filter.found = true;
            int foundCount = searchCachesCount(filter);
            log.info(String.format("%s found %d cache(s) I did found.", prefix, foundCount));
            int total = notFoundCount + foundCount;

            log.info(String.format("%s found %d cache(s).", prefix, total));

            GcCacheStateStatsTopiaDao statsDao = persistenceContext.getGcCacheStateStatsDao();
            GcCacheStateStats stats = statsDao.forProperties(
                    GcCacheStateStats.PROPERTY_STATE, gcState,
                    GcCacheStateStats.PROPERTY_DIFFICULTY, gcDifficulty,
                    GcCacheStateStats.PROPERTY_TERRAIN, gcTerrain

            ).tryFindUnique().orNull();
            if (stats == null) {
                statsDao.create(GcCacheStateStats.PROPERTY_STATE, gcState,
                                GcCacheStateStats.PROPERTY_DIFFICULTY, gcDifficulty,
                                GcCacheStateStats.PROPERTY_TERRAIN, gcTerrain,
                                GcCacheStateStats.PROPERTY_CURRENT, current,
                                GcCacheStateStats.PROPERTY_TOTAL, total
                );
            } else {
                stats.setCurrent(current);
                stats.setTotal(total);
            }
            persistenceContext.commit();
        }
    }


    private Set<String> getFrenchOwnerCachesGcNames(GcUser owner) throws IOException {
        String userNormalizeName = owner.getUserName().replaceAll("\\s", "_");
        HRequest request = create("http://www.mides.fr/geocaching/util_geo/live_gpx.php")
                .addParameter("id", owner.getUserId())
                .addParameter("buffeur", userNormalizeName).get();

        HResponse response = executeRequest(request);
        String gpxContent = response.toString();

        Path tempFile = null;
        try {
            tempFile = getServiceContext().getConfig().createTempFile("gc-gpx-" + userNormalizeName);

            Files.write(tempFile, gpxContent.getBytes());
            return getServiceContext().newGpxService().getGcNames(tempFile.toFile());

        } finally {
            if (tempFile != null) {
                Files.delete(tempFile);
            }
        }

    }

    @Override
    public void loadFrenchOwnerCachesGcNames(GcUser owner) {

        try (GcDbTopiaPersistenceContext persistenceContext = newPersistenceContext()) {
            GcUserTopiaDao userDao = persistenceContext.getGcUserDao();

            io.ultreia.gc.db.entity.GcUser user = userDao.forUserIdEquals(owner.getUserId()).findUnique();

            String userName = user.getName();
            log.info(String.format("Lookup for french cache(s) for user: %s (find in db: %d - total: %d)", userName, user.getCachesOwnedInFrance(), owner.getCachesOwnedInFrance()));
            if (user.getCachesOwnedInFrance() == owner.getCachesOwnedInFrance()) {
                log.info(String.format("found %d cache(s) for owner: %s in database.", owner.getCachesOwnedInFrance(), userName));
                return;

            }

            String userNormalizeName = userName.replaceAll("\\s", "_");

            try {
                Set<String> userGcNames = getFrenchOwnerCachesGcNames(owner);

                log.info(String.format("found %d cache(s) for owner: %s", userGcNames.size(), userName));

                Set<String> gcNamesToAcquire = new LinkedHashSet<>(userGcNames);
                gcNamesToAcquire.removeAll(getGcNames());

                if (!gcNamesToAcquire.isEmpty()) {

                    log.info(String.format("acquire %d cache(s) for owner: %s", gcNamesToAcquire.size(), userName));

                    MutableInt i = new MutableInt();

                    Set<GcCache> models = new LinkedHashSet<>();

                    GcCacheLoader cacheLoader = new GcCacheLoader(this, persistenceContext, false);

                    gcNamesToAcquire.parallelStream().forEach(gc -> {

                        try {
                            models.add(cacheLoader.get(gc));
                            log.info(String.format("%S - Loaded %d/%d cache: %s", userNormalizeName, i.incrementAndGet(), gcNamesToAcquire.size(), gc));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    });

                    MutableInt j = new MutableInt();

                    models.forEach(model -> {
                        cacheLoader.persist(model, true);
                        log.info(String.format("%S - Persisted %d/%d cache: %s", userNormalizeName, j.incrementAndGet(), gcNamesToAcquire.size(), model.getGcName()));
                    });
                }

                user.setCachesOwnedInFrance(owner.getCachesOwnedInFrance());
                userDao.update(user);
                persistenceContext.commit();

            } catch (IOException e) {
                throw new IllegalStateException(e);
            }

        }
    }

    @Override
    public void updateCoordinates(String gcName, String lat, String lon) {

        String auth = null;
        String content = executeRequest(create(String.format(URL_CACHE, gcName)).get(), HttpStatus.SC_OK).toString();
        try (BufferedReader reader = new BufferedReader(new StringReader(content))) {

            String line;
            while (auth == null && (line = reader.readLine()) != null) {
                if (line.trim().startsWith("userToken = ")) {
                    auth = StringUtils.removeEnd(StringUtils.removeStart(line.trim(), "userToken = ").replaceAll("'", "").trim(), ";");
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException("Nope");
        }
        Objects.requireNonNull(auth, "can't find auth in\n" + content);
        HRequestBuilder requestBuilder = create(URL_UPDATE_COORDINATES);
        String result = executeRequest(requestBuilder
                                               .setRequestBody(String.format("{\"dto\": {\"data\":{\"lat\":%s,\"lng\":%s},\"ut\":\"%s\"}}", lat, lon, auth))
                                               .setContentType("application/json")
                                               .addHeader("Accept", "application/json")
                                               .addHeader("Content-type", "application/json")
                                               .post(),
                                       HttpStatus.SC_OK).toString();
        log.info(result);

    }

}
