package io.ultreia.gc.service.internal.db;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.db.entity.GcAttribute;
import io.ultreia.gc.db.entity.GcAttributeTopiaDao;
import io.ultreia.gc.db.entity.GcCache;
import io.ultreia.gc.db.entity.GcCacheAttributeTopiaDao;
import io.ultreia.gc.db.entity.GcCountry;
import io.ultreia.gc.db.entity.GcDbTopiaPersistenceContext;
import io.ultreia.gc.db.entity.GcDifficulty;
import io.ultreia.gc.db.entity.GcState;
import io.ultreia.gc.db.entity.GcTerrain;
import io.ultreia.gc.db.entity.GcUser;
import io.ultreia.gc.model.GcCacheAttribute;
import io.ultreia.gc.service.internal.GcServiceSupportImpl;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by tchemit on 03/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcCacheBinder {

    private final GcDbTopiaPersistenceContext persistenceContext;
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyy-MM-dd'T'hh:mm:ss'Z'");
    private final SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyy-MM-dd'T'hh:mm:ss+00:00");
    private final DateConverter dateConverter = new DateConverter();
    private GcUserLoader userLoader;

    public GcCacheBinder(GcServiceSupportImpl service, GcDbTopiaPersistenceContext persistenceContext) {
        this.persistenceContext = persistenceContext;
        this.userLoader = new GcUserLoader(service, persistenceContext, false, false);
    }

    public void toEntity(io.ultreia.gc.model.GcCache model, GcCache result) {
        result.setGcName(model.getGcName());
        result.setGcType(model.getGcType());
        result.setTerrain(GcTerrain.valueOf(model.getTerrain()));
        result.setDifficulty(GcDifficulty.valueOf(model.getDifficulty()));
        result.setDescription(model.getDesc());
        result.setLat(Double.valueOf(model.getLat()));
        result.setLon(Double.valueOf(model.getLon()));
        result.setHint(model.getHint());
        result.setGcId(model.getGcId());
        try {
            result.setTime(simpleDateFormat.parse(model.getTime()));
        } catch (ParseException e) {
            try {
                result.setTime(simpleDateFormat2.parse(model.getTime()));
            } catch (ParseException e1) {
                result.setTime(dateConverter.convert(model.getTime()));
            }
        }
        result.setShortDesc(model.getShortDesc());
        result.setShortDescHtml(model.isShortDescHtml());
        result.setLongDesc(model.getLongDesc());
        result.setLongDescHtml(model.isLongDescHtml());
        result.setPremium(model.isPremium());
        result.setContainer(model.getContainer());
        result.setArchived(model.isArchived());
        result.setAvailable(model.isAvailable());
        result.setUrl(model.getUrl());
        result.setUrlname(model.getUrlname());
        if (model.getTopiaId() != null) {
            result.setTopiaId(model.getTopiaId());
        }

        result.setCountry(GcCountry.of(model.getCountry()));
        if (model.getState() != null) {
            result.setState(GcState.of(model.getCountry() + ": " + model.getState()));
        }

//        synchronized (GcCacheBinder.class) {

        GcUser gcUser = userLoader.load(model.getOwnerId());
        result.setOwner(gcUser);
//        }

        result.clearGcCacheAttribute();

    }

    public void addAttributes(io.ultreia.gc.model.GcCache model, GcCache result) {
        GcAttributeTopiaDao attributeDao = persistenceContext.getGcAttributeDao();
        GcCacheAttributeTopiaDao cacheAttributeDao = persistenceContext.getGcCacheAttributeDao();
        List<io.ultreia.gc.db.entity.GcCacheAttribute> attributes = new LinkedList<>();
        for (GcCacheAttribute attribute : model.getAttributes()) {
            GcAttribute gcAttribute = attributeDao.forCodeEquals(attribute.getId()).findUnique();
            io.ultreia.gc.db.entity.GcCacheAttribute gcCacheAttribute = cacheAttributeDao.newInstance();
            gcCacheAttribute.setGcCache(result);
            gcCacheAttribute.setGcAttribute(gcAttribute);
            gcCacheAttribute.setInc(attribute.getInc());
            attributes.add(gcCacheAttribute);
        }
        result.addAllGcCacheAttribute(attributes);
    }
}
