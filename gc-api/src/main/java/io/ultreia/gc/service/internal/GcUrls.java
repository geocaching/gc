package io.ultreia.gc.service.internal;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created by tchemit on 26/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface GcUrls {

    // Geocaching.com

    String URL_LOGOUT = "https://www.geocaching.com/account/logout";
    String URL_LOGIN = "https://www.geocaching.com/account/login";
    String URL_MY_DEFAULT = "https://www.geocaching.com/my/default.aspx";
    String URL_OAUTH_TOKEN = "https://www.geocaching.com/account/oauth/token";

    String URL_GET_TRACKABLES = "https://www.geocaching.com/api/proxy/trackables?inCollection=false";
    String URL_GET_LOGBOOK = "https://www.geocaching.com/seek/geocache.logbook";
    String URL_GET_PROFILE = "https://www.geocaching.com/profile/?id=%s";
    String URL_GET_PROFILE_GUID = "https://www.geocaching.com/profile/?guid=%s";
    String URL_GET_PROFILE_NAME = "https://www.geocaching.com/profile/?u=%s";

    String URL_CACHE = "https://coord.info/%s";
    String URL_CACHE_DETAILS = "https://www.geocaching.com/seek/cache_details.aspx";
    String URL_SEARCH_MORE_RESULTS = "https://www.geocaching.com/play/search/more-results";
    String URL_SEARCH = "https://www.geocaching.com/play/search";
    String URL_CACHE_REF = "https://www.geocaching.com/api/proxy/web/v1/geocache/%s";

    String URL_GET_FIELD_NOTES = "https://www.geocaching.com/api/proxy/web/v1/LogDrafts";
    String URL_COMPOSE_LOG = "https://www.geocaching.com/account/drafts/home/compose?gc=%s&d=%s&dGuid=%s&lt=%s";
    String URL_SEND_LOG_IMAGE = "https://www.geocaching.com/api/proxy/web/v1/LogDrafts/images";
    String URL_SEND_LOG = "https://www.geocaching.com/api/proxy/web/v1/Geocache/%s/GeocacheLog";
    String URL_ATTACH_IMAGE_TO_LOG = "https://www.geocaching.com/api/proxy/web/v1/LogDrafts/images/%s?geocacheLogReferenceCode=%s";
    String URL_DELETE_LOG_DRAFT = "https://www.geocaching.com/api/proxy/web/v1/LogDrafts/%s";
    String URL_GET_LOG_DRAFT = "https://www.geocaching.com/api/proxy/web/v1/LogDrafts/%s";
    String URL_UPDATE_COORDINATES = "https://www.geocaching.com/seek/cache_details.aspx/SetUserCoordinate";

    // Project-gc

    String URL_PGC_GET_CHALLENGE = "https://project-gc.com/Challenges/%s";
    String URL_PGC_RUN_CHALLENGE_CHECKER = "http://lua2.project-gc.com/ajax/challenges.php";
    String URL_PGC_LOGIN = "https://project-gc.com/oauth.php";
    String URL_PGC_AUTH = "https://www.geocaching.com/OAuth/Authorize.aspx";


}
