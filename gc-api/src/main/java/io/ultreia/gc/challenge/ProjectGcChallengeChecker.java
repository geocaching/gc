package io.ultreia.gc.challenge;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.model.GcChallengeResult;
import io.ultreia.gc.service.internal.GcServiceSupportImpl;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HResponse;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpStatus;
import org.jsoup.nodes.Document;

/**
 * Created by tchemit on 19/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ProjectGcChallengeChecker extends GcServiceSupportImpl implements GcChallengeChecker {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ProjectGcChallengeChecker.class);

    @Override
    public GcChallengeResult check(String gcName) {

        loginProjectGc();

        GcChallengeResult result = new GcChallengeResult();
        result.setGcName(gcName);

        try {
            Document responseAsHtml;
            {
                HRequest request = create(String.format(URL_PGC_GET_CHALLENGE, gcName)).get();
                HResponse response = executeRequest(request, HttpStatus.SC_OK);

                responseAsHtml = response.toHtml();
            }

            String tagId = responseAsHtml.select("div[data-cc-gccode='" + gcName + "']").attr("data-cc-tagid");

            {
                HRequest request = create(URL_PGC_RUN_CHALLENGE_CHECKER)
                        .addParameter("profileName", getUsername())
                        .addParameter("cmd", "RunChecker")
                        .addParameter("tagId", tagId)
                        .post();
                HResponse response = executeRequest(request, HttpStatus.SC_OK);

                Map<String, Object> responseAsJson = response.toJson();

                Map t = (Map) responseAsJson.get("scriptReturnData");
                try {
                    Object log = t.get("log");
                    if (log instanceof String) {

                        result.setLog((String) log);
                    } else {
                        result.setLog((String) t.get("html"));

                    }
                    result.setSuccess((Boolean) t.get("ok"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                result.setTime((Double) responseAsJson.get("time"));

            }
        } catch (Exception e) {
            log.error("Can not check challenge: " + gcName, e);
            result.setError(e.getMessage());
        }
        return result;

    }

}
