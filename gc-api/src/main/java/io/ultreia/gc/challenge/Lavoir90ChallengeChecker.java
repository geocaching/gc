package io.ultreia.gc.challenge;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.model.GcCache;
import io.ultreia.gc.model.GcChallengeResult;
import io.ultreia.gc.service.api.GcCacheService;
import io.ultreia.gc.service.api.GcSearchFilter;
import io.ultreia.gc.service.internal.GcServiceSupportImpl;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 19/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class Lavoir90ChallengeChecker extends GcServiceSupportImpl implements GcChallengeChecker {

    /** Logger. */
    private static final Log log = LogFactory.getLog(Lavoir90ChallengeChecker.class);

    @Override
    public GcChallengeResult check(String gcName) {

        GcCacheService cacheService = getServiceContext().newCacheService();

        GcSearchFilter filter = new GcSearchFilter();
        filter.keyword = "lavoir";
        filter.found = true;


        List<GcCache> lavoirCaches = cacheService.searchCaches(filter).parallelStream().map(cacheService::getCacheFromGcName).collect(Collectors.toList());

        log.info("Found " + lavoirCaches.size() + " «Lavoir» cache(s).");

        Set<String> types = lavoirCaches.stream().map(GcCache::getGcType).collect(Collectors.toSet());

        double score = lavoirCaches.stream().mapToDouble(c -> c.getDifficulty() + c.getTerrain()).sum();

        StringBuilder result = new StringBuilder("\n");
        result.append("# Summary #\n\n");
        result.append(String.format("\nFound %d «Lavoir» caches.", lavoirCaches.size()));
        result.append(String.format("\nFound %d different types of «Lavoir» caches : %s", types.size(), types));
        result.append(String.format("\nYour score is %s.", score));
        result.append("\n\n# Details #\n\n");
        float totalScore = 0;
        for (GcCache lavoir : lavoirCaches) {
            float cacheScore = lavoir.getDifficulty() + lavoir.getTerrain();
            totalScore += cacheScore;
            result.append(String.format("\n * [%s - %s - %s points] «%s» = %s", lavoir.getGcType(), lavoir.getGcName(), totalScore, lavoir.getDesc(), cacheScore));
        }
        log.info(result.toString());

        GcChallengeResult r = new GcChallengeResult();
        r.setGcName("GC3F9DP");
        r.setLog(result.toString());
        r.setSuccess(score > 333 && types.size() > 2);

        return r;
    }

}
