package io.ultreia.gc.challenge;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.model.GcCache;
import io.ultreia.gc.model.GcChallengeResult;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by tchemit on 05/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ChallengerChallengeIChecker implements GcChallengeChecker {

    private final List<GcCache> gcCaches;

    private final String[] terms = {
            "église", "cathédrale", "abbaye", "cimetière", "saint", "vierge", "misericorde", "santa",
            "christ ", "dieux", "jesus", "paroisse", "croix", "rameau", "basilique", "clocher", "calvaire", "paradis"
            , "pâques", "enfer ", "prieuré", "pénitent", "curé", "madone", "noël", "divin", "sainte", "notre dame", "chapelle",
            " ange", "pêcheur", "oratoire"
    };

    public ChallengerChallengeIChecker(List<GcCache> gcCaches) {
        this.gcCaches = gcCaches;
    }

    @Override
    public GcChallengeResult check(String gcName) {

        Set<GcCache> matches = new LinkedHashSet<>();

        for (String term : terms) {
            gcCaches.stream().filter(c -> c.getDesc().toLowerCase().contains(term)).findFirst().ifPresent(matches::add);
        }
        AtomicInteger i = new AtomicInteger();
        StringBuilder builder = new StringBuilder();

        GcChallengeResult gcChallengeResult = new GcChallengeResult();
        gcChallengeResult.setGcName("GC55BJK");
        gcChallengeResult.setSuccess(matches.size() > 19);

        if (gcChallengeResult.isSuccess()) {
            builder.append("Vous avez trouvés 20 caches adéquates :");
        } else {
            builder.append(String.format("Vous avez trouvés %d caches adéquates, il vous en manque :%d", matches.size(), 20 - matches.size()));
        }
        for (GcCache match : matches) {
            builder.append(String.format("\n * [%d] %s - %s", i.incrementAndGet(), match.getGcName(), match.getDesc()));
        }
        gcChallengeResult.setLog(builder.toString());
        return gcChallengeResult;
    }
}
