package io.ultreia.gc.placespotting;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HRequestBuilder;
import io.ultreia.java4all.http.HResponse;
import io.ultreia.java4all.http.HResponseBuilder;
import java.io.IOException;
import java.util.Optional;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpStatus;

/**
 * Created by tchemit on 14/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class PlaceSpottingResolver {

    private final String placeId;
    private final String solutionIdentifier;
    private final Zoom zoom;
    private final HResponseBuilder responseBuilder;

    private PlaceSpottingResolver(String placeId) throws IOException {
        this.placeId = placeId;
        this.responseBuilder = HResponseBuilder.create();


        HRequest request = new HRequestBuilder("http://www.placespotting.com/solve.php")
                .addParameter("placeId", this.placeId)
                .get();

        HResponse response = responseBuilder.executeRequest(request, HttpStatus.SC_OK);
        String responseText = response.toString();

        int startIndex = responseText.indexOf("solutionIdentifier");
        int firstIndex = responseText.indexOf("=", startIndex);
        int lastIndex = responseText.indexOf("\"", startIndex);
        this.solutionIdentifier = responseText.substring(firstIndex + 1, lastIndex);
        this.zoom = Zoom.of(Integer.valueOf(response.toHtml().select("div[class='mainArea'] > table").get(0).select("div").get(2).text().split("\\s")[4]));
    }

    private Optional<String> test(PlaceSpottingCoordinate coordinate) throws IOException {


        HRequest request = new HRequestBuilder("http://www.placespotting.com/solutionAjax.php")
                .addParameter("placeId", this.placeId)
                .addParameter("solutionZoom", this.zoom)
                .addParameter("solutionIdentifier", this.solutionIdentifier)
                .addParameter("solutionNorth", coordinate.north + "solutionNorth=" + coordinate.north)
                .addParameter("solutionSouth", coordinate.south)
                .addParameter("solutionEast", coordinate.east)
                .addParameter("solutionWest", coordinate.west)
                .quiet()
                .post();

        HResponse response = responseBuilder.executeRequest(request, HttpStatus.SC_OK);
        String responseText = response.toString();
        return responseText.equals("NO") ? Optional.empty() : Optional.of(responseText);

    }

    private String test(PlaceSpottingCoordinate coordinateNorthWest, PlaceSpottingCoordinate coordinateSouthEast) throws IOException {

        PlaceSpottingCoordinate currentCoordinate = coordinateNorthWest;
        PlaceSpottingCoordinate eastCoordinate = coordinateNorthWest;


        int count = 0;

        while (currentCoordinate.south > coordinateSouthEast.south) {

            int factor = 160;
            while (currentCoordinate.east < coordinateSouthEast.east) {

                count++;

                log.info(String.format("%d - Test with: %s", count, currentCoordinate));

                Optional<String> test = test(currentCoordinate);
                if (test.isPresent()) {

                    log.info("Nb counts: " + count);
                    return test.get();
                }

                currentCoordinate = currentCoordinate.toLeft(zoom, factor);

            }

            currentCoordinate = eastCoordinate = eastCoordinate.toBottom(zoom, factor);
        }

        return null;

    }

    /** Logger. */
    private static final Log log = LogFactory.getLog(PlaceSpottingResolver.class);

    public static void main(String[] args) throws IOException {

//        PlaceSpottingResolver resolver = new PlaceSpottingResolver("1F04-5794D9FC-1972");
//        PlaceSpottingResolver resolver = new PlaceSpottingResolver("1415-5794C823-4A4");
//        PlaceSpottingResolver resolver = new PlaceSpottingResolver("210A-5794E134-9A2");

        PlaceSpottingResolver resolver = new PlaceSpottingResolver("209F-5794EB56-21DC"); // D4.5

        //43.397127, 3.697233 NW
        //43.395953, 3.703069 SE


        String result = resolver.test(
                PlaceSpottingCoordinate.of(43.444475f, 3.670138f),
                PlaceSpottingCoordinate.of(43.415468f, 3.709952f));

        log.info("Result: " + result);

    }

    enum Zoom {

        _20(0.000001341f, -0.000001949f);

        private final float left;
        private final float bottom;

        Zoom(float left, float bottom) {
            this.left = left;
            this.bottom = bottom;
        }

        public float getLeft() {
            return left;
        }

        public float getBottom() {
            return bottom;
        }

        public static Zoom of(int zoom) {
            for (Zoom zoom1 : values()) {
                if (zoom1.name().substring(1).equals(zoom + "")) {
                    return zoom1;
                }
            }
            throw new IllegalStateException("Can't find zoom: " + zoom);
        }
    }

    public static class PlaceSpottingCoordinate {

        private final float north;
        private final float south;
        private final float east;
        private final float west;

        public static PlaceSpottingCoordinate of(String text) {
            String[] split = text.split("=");
            return new PlaceSpottingCoordinate(Float.valueOf(split[1].replace("&S", "")),
                                               Float.valueOf(split[2].replace("&E", "")),
                                               Float.valueOf(split[3].replace("&W", "")),
                                               Float.valueOf(split[4]));
        }

        public static PlaceSpottingCoordinate of(float latitude, float longitude) {
            return new PlaceSpottingCoordinate(latitude + 0.000175403f,
                                               latitude - 0.000175403f,
                                               longitude + 0.000402331f,
                                               longitude - 0.000402331f);
        }

        PlaceSpottingCoordinate(float north, float south, float east, float west) {
            this.north = north;
            this.south = south;
            this.east = east;
            this.west = west;
        }

        PlaceSpottingCoordinate toLeft(Zoom zoom, int factor) {
            return new PlaceSpottingCoordinate(north, south, east + factor * zoom.getLeft(), west + factor * zoom.getLeft());
        }

        PlaceSpottingCoordinate toBottom(Zoom zoom, int factor) {
            return new PlaceSpottingCoordinate(north + factor * zoom.getBottom(), south + factor * zoom.getBottom(), east, west);
        }

        @Override
        public String toString() {
            return "PlaceSpottingCoordinate{" +
                    "north=" + north +
                    ", south=" + south +
                    ", east=" + east +
                    ", west=" + west +
                    '}';
        }
    }


}
