package io.ultreia.gc.tool;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.model.GcArcheoLog;
import io.ultreia.gc.service.internal.ProgressMonitor;
import java.util.EventObject;

/**
 * Created by tchemit on 18/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface ComputeArcheoScoreProgressMonitor extends ProgressMonitor {

    class GcArcheoLogEvent extends EventObject {

        private final GcArcheoLog archeoLog;
        private final int totalPoints;

        public GcArcheoLogEvent(Object source, GcArcheoLog archeoLog, int totalPoints) {
            super(source);
            this.archeoLog = archeoLog;
            this.totalPoints = totalPoints;
        }

        public GcArcheoLog getArcheoLog() {
            return archeoLog;
        }

        public int getTotalPoints() {
            return totalPoints;
        }
    }

    void onPointsAdded(GcArcheoLogEvent archeoLogEvent);

    void onConflictDetected(GcArcheoLogEvent archeoLogEvent);
}
