package io.ultreia.gc.tool;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.config.GcApiConfig;
import io.ultreia.gc.model.GcCache;
import io.ultreia.gc.service.internal.GcCacheServiceImpl;
import io.ultreia.gc.service.internal.GpxServiceImpl;
import java.nio.file.Path;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 24/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LoadMyFinds {


    /** Logger. */
    private static final Log log = LogFactory.getLog(LoadMyFinds.class);

    public static void main(String[] args) {

        GcApiConfig config = GcApiConfig.create(args);

        config.setLogin("fafahakkai");

        Path gpxPath = config.getMyFindsGpx();
        Path myDbPath = config.getMyFindsJson();

        log.info("Loading " + gpxPath);
        List<GcCache> caches = new GpxServiceImpl().getCaches(gpxPath.toFile());
        log.info("Load " + caches.size() + " cache(s).");
        new GcCacheServiceImpl().storeCaches(caches, myDbPath.toFile());
        log.info("Store to " + myDbPath);


    }
}

