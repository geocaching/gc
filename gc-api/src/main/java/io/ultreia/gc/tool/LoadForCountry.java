package io.ultreia.gc.tool;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.db.entity.GcCountry;
import io.ultreia.gc.db.entity.GcDifficulty;
import io.ultreia.gc.db.entity.GcTerrain;
import io.ultreia.gc.model.GcCombination;
import io.ultreia.gc.service.api.GcCacheService;
import io.ultreia.gc.service.api.GcSearchFilter;
import io.ultreia.gc.service.api.GcServiceFactory;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 10/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LoadForCountry implements Runnable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LoadForCountry.class);

    private final GcServiceFactory serviceFactory;
    private final GcCountry states;

    public LoadForCountry(GcServiceFactory serviceFactory, GcCountry states) {
        this.serviceFactory = serviceFactory;
        this.states = states;
    }

    @Override
    public void run() {
        List<Pair<GcTerrain, GcDifficulty>> result = new LinkedList<>();

        GcSearchFilter filter = new GcSearchFilter();
        filter.found = false;
        filter.country = states.code();

        GcCacheService cacheService = serviceFactory.newCacheService();
        Set<String> allGcNames = cacheService.getGcNames();

        Iterator<GcCombination> combinationIterator = GcCombination.newIterator(true);
        while (combinationIterator.hasNext()) {
            GcCombination combination = combinationIterator.next();
            GcDifficulty gcDifficulties = combination.getDifficulty();
            filter.difficulty = gcDifficulties;
            GcTerrain gcTerrains = combination.getTerrain();
            filter.terrain = gcTerrains;

            String prefix = String.format("[%s D%s T%s]", states, gcDifficulties, gcTerrains);

            log.info(String.format("Loading caches for %s I did not found", prefix));
            try {
                Set<String> gcNames = cacheService.searchCaches(filter);
                log.info(String.format("Found %d %s cache(s) I did not found", gcNames.size(), prefix));
                gcNames.removeAll(allGcNames);
                if (gcNames.isEmpty()) {
                    continue;
                }
                log.info(String.format("Found %d %s cache(s) to acquire", gcNames.size(), prefix));
                MutableInt i = new MutableInt();
                gcNames.parallelStream().forEach(gcName -> {

                    try {
                        log.info(String.format("%d/%d : Loading cache %s %s", i.incrementAndGet(), gcNames.size(), prefix, gcName));
                        cacheService.getCacheFromGcName(gcName);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

            } catch (Exception e) {
                result.add(Pair.of(gcTerrains, gcDifficulties));
            }

        }

        for (Pair<GcTerrain, GcDifficulty> gcTerrainsGcDifficultiesPair : result) {
            log.error(String.format("Could not load terrain %s - %s", gcTerrainsGcDifficultiesPair.getLeft(), gcTerrainsGcDifficultiesPair.getRight()));
        }
    }
}
