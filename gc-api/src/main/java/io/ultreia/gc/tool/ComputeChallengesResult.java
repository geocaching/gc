package io.ultreia.gc.tool;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.ultreia.gc.db.entity.GcCountry;
import io.ultreia.gc.db.entity.GcState;
import io.ultreia.gc.model.GcChallengeResult;
import io.ultreia.gc.service.api.GcChallengeService;
import io.ultreia.gc.service.api.GcServiceFactory;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 15/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ComputeChallengesResult {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ComputeChallengesResult.class);

    private final GcServiceFactory serviceFactory;
    private final GcState state;
    private final GcCountry country;
    private final Gson gson;

    public static ComputeChallengesResult forCountry(GcServiceFactory serviceFactory, GcCountry country) {
        return new ComputeChallengesResult(serviceFactory, country, null);
    }

    public static ComputeChallengesResult forState(GcServiceFactory serviceFactory, GcState state) {
        return new ComputeChallengesResult(serviceFactory, null, state);
    }


    private ComputeChallengesResult(GcServiceFactory serviceFactory, GcCountry country, GcState state) {
        this.serviceFactory = serviceFactory;
        this.state = state;
        this.country = country;
        this.gson = new GsonBuilder().setPrettyPrinting().create();
    }

    public List<GcChallengeResult> compute(Path dir, String userName) throws IOException {

        if (!Files.exists(dir)) {
            Files.createDirectories(dir);
        }

        GcChallengeService challengeService = serviceFactory.newChallengeService();

        String name;
        Set<String> challenges;
        if (state != null) {
            log.info("Loading challenges for state: " + state);
            challenges = challengeService.getStateChallenges(state);
            name = state.name() + "-" + userName;
        } else {
            log.info("Loading challenges for country: " + country);
            challenges = challengeService.getCountryChallenges(country);
            name = country.name() + "-" + userName;
        }


        Path succesPath = dir.resolve(name + "-challenges-success.json");
        Path failPath = dir.resolve(name + "-challenges-fail.json");

        List<GcChallengeResult> successChallenges = loadResults(succesPath);
        List<GcChallengeResult> failChallenges = loadResults(failPath);

        List<String> skipGcNames = successChallenges.stream().map(GcChallengeResult::getGcName).collect(Collectors.toList());
        skipGcNames.addAll(failChallenges.stream().map(GcChallengeResult::getGcName).collect(Collectors.toList()));

        List<GcChallengeResult> results = new LinkedList<>();
        challenges.stream().filter(gcName -> !skipGcNames.contains(gcName)).parallel().forEach(gcName -> {

            GcChallengeResult challengeResult = challengeService.runProjectGcChallenge(gcName);
            results.add(challengeResult);
        });

        try (BufferedWriter writer = Files.newBufferedWriter(succesPath)) {
            successChallenges.addAll(results.stream().filter(GcChallengeResult::isSuccess).collect(Collectors.toList()));
            log.info("Found " + successChallenges.size() + " success " + name + " challenge(s).");
            gson.toJson(successChallenges, writer);
        }
        try (BufferedWriter writer = Files.newBufferedWriter(dir.resolve(name + "-challenges-error.json"))) {

            List<GcChallengeResult> fail = results.stream().filter(GcChallengeResult::withError).collect(Collectors.toList());
            log.info("Found " + fail.size() + " error " + name + " challenge(s).");
            gson.toJson(fail, writer);
        }

        try (BufferedWriter writer = Files.newBufferedWriter(failPath)) {

            failChallenges.addAll(results.stream().filter(GcChallengeResult::isFailed).collect(Collectors.toList()));
            log.info("Found " + failChallenges.size() + " fail " + name + " challenge(s).");
            gson.toJson(failChallenges, writer);
        }

        return results;
    }

    private List<GcChallengeResult> loadResults(Path path) throws IOException {
        if (Files.exists(path)) {
            try (BufferedReader reader = Files.newBufferedReader(path)) {
                Type type = new TypeToken<List<GcChallengeResult>>() {

                }.getType();
                return gson.fromJson(reader, type);
            }
        }
        return new LinkedList<>();
    }
}
