package io.ultreia.gc.tool;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.ultreia.gc.model.GcArcheoLog;
import io.ultreia.gc.model.GcArcheoLogsComputationResult;
import io.ultreia.gc.service.api.GcLogService;
import io.ultreia.gc.service.api.GcServiceSupport;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 02/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ComputeArcheoScore extends GcServiceSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ComputeArcheoScore.class);

    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public GcArcheoLogsComputationResult computeArcheoCaches(String username, Collection<String> gcNames, List<GcArcheoLog> cache, File output, ComputeArcheoScoreProgressMonitor progressMonitor) throws IOException {

        int gcNamesSize = gcNames.size();
        Set<GcArcheoLog> logs = new LinkedHashSet<>();
        logs.addAll(cache);

        progressMonitor.setValue(cache.size());

        GcArcheoLogsComputationResult result = new GcArcheoLogsComputationResult();

        Map<String, GcArcheoLog> existingCacheMapping = new TreeMap<>();
        cache.forEach(archeoLog -> {
            existingCacheMapping.put(archeoLog.getGcName(), archeoLog);
            archeoLog.computeType();
            addResult(result, archeoLog, progressMonitor);
        });

        storeArcheoLogs(output, logs);

        GcLogService logService = getServiceContext().newLogService();
        gcNames.stream().filter(gcName -> !existingCacheMapping.containsKey(gcName)).parallel().forEach(gcName -> {
            try {

                Optional<GcArcheoLog> optionalArcheoLog = logService.getMyArcheoLogFromGcName(gcName, username);
                if (!optionalArcheoLog.isPresent()) {
                    log.warn("Could not find my found it on cache: " + gcName);
                    result.addBadGcName(gcName);
                    progressMonitor.increment(String.format("[Total points: %d] - Could not find my found it on cache: %s (%d/%d)", result.getTotalPoints(), gcName, result.getBadGcNames().size() + logs.size(), gcNamesSize));
                    return;
                }
                GcArcheoLog archeoLog = optionalArcheoLog.get();
                logs.add(archeoLog);
                addResult(result, archeoLog, progressMonitor);

                progressMonitor.increment(String.format("[Total points: %d] - Done for cache: %s (%d/%d)", result.getTotalPoints(), gcName, result.getBadGcNames().size() + logs.size(), gcNamesSize));
                synchronized (this) {
                    logs.add(archeoLog);
                    if (logs.size() % 20 == 0) {
                        log.info("Store " + logs.size() + " archeo-log(s) to " + output);
                        storeArcheoLogs(output, logs);
                    }
                }
            } catch (Exception e) {
                log.error("Could not treat cache: " + gcName, e);
            }
        });

        storeArcheoLogs(output, logs);
        return result;
    }


    private void addResult(GcArcheoLogsComputationResult result, GcArcheoLog archeoLog, ComputeArcheoScoreProgressMonitor progressMonitor) {
        archeoLog.getMyLog().setLogText(null);
        archeoLog.getPreviousLog().ifPresent(l -> l.setLogText(null));
        archeoLog.getPreviousLogOtherDay().ifPresent(l -> l.setLogText(null));
        archeoLog.getOtherLogSameDay().forEach(l -> l.setLogText(null));
        if (result.addResult(archeoLog)) {
            progressMonitor.onPointsAdded(new ComputeArcheoScoreProgressMonitor.GcArcheoLogEvent(result, archeoLog, result.getTotalPoints()));
        } else if (result.addConflict(archeoLog)) {
            progressMonitor.onConflictDetected(new ComputeArcheoScoreProgressMonitor.GcArcheoLogEvent(result, archeoLog, result.getTotalPoints()));
        }
    }

    private synchronized void storeArcheoLogs(File output, Set<GcArcheoLog> logs) {
        try {
            Files.write(output.toPath(), gson.toJson(new ArrayList<>(logs)).getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public List<GcArcheoLog> loadArcheoCaches(File cacheFile) throws IOException {
        Gson gson = new GsonBuilder().create();

        if (!cacheFile.exists()) {
            return new LinkedList<>();
        }
        try (BufferedReader reader = Files.newBufferedReader(cacheFile.toPath())) {
            return gson.fromJson(reader, new TypeToken<List<GcArcheoLog>>() {
            }.getType());
        }
    }
}
