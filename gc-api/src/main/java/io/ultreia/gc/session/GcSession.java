package io.ultreia.gc.session;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.service.internal.GcUrls;
import io.ultreia.java4all.http.HExecuteRequestException;
import io.ultreia.java4all.http.HLoginException;
import io.ultreia.java4all.http.HRequest;
import io.ultreia.java4all.http.HRequestBuilder;
import io.ultreia.java4all.http.HRequestBuilderFactory;
import io.ultreia.java4all.http.HResponse;
import io.ultreia.java4all.http.HResponseBuilder;
import io.ultreia.java4all.http.HRestClientService;
import java.io.Closeable;
import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpStatus;
import org.jsoup.nodes.Document;

/**
 * Created by tchemit on 13/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcSession implements Closeable, GcUrls, HRequestBuilderFactory, HRestClientService {

    private static final Log log = LogFactory.getLog(GcSession.class);

    private static final Map<String, String> HEADERS;

    static {
        HEADERS = new LinkedHashMap<>();
        HEADERS.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:48.0) Gecko/20100101 Firefox/48.0");
        HEADERS.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        HEADERS.put("Accept-Language", "fr-fr,en-us;q=0.7,en;q=0.3");
        HEADERS.put("DNT", "1");
        HEADERS.put("Connection", "keep-alive");
    }

    private final HResponseBuilder responseBuilder;
    private final String username;
    private final String id;
    private final Timer timer;
    private final AtomicInteger count = new AtomicInteger();
    private String authToken;
    private boolean projectGcLogged;
    private long lastHitInMilliSeconds;

    GcSession(String username, String password) {
        this.username = username;
        this.id = UUID.randomUUID().toString();
        this.responseBuilder = HResponseBuilder.create();

        login(username, password);

        (this.timer = new Timer("keepAliveSession-" + username, false))
                .schedule(new TimerTask() {
                    @Override
                    public void run() {
                        synchronized (count) {
                            if (count.incrementAndGet() > 1) {
                                log.info(String.format("Keep alive session: %s - %s [%d]", username, new Date(), count.get()));
                                logout();
                                login(username, password);
                            }
                        }
                    }
                }, new Date(), TimeUnit.MINUTES.toMillis(30));
    }

    @Override
    public HRequestBuilderFactory getRequestBuilderFactory() {
        return this;
    }

    @Override
    public HRequestBuilder create(String baseUrl) {
        return new HRequestBuilder(baseUrl).addHeaders(HEADERS).addAuthTokenSupplier(this::getAuthToken);
    }

    @Override
    public HResponse executeRequest(HRequest request) {
        lastHitInMilliSeconds = Instant.now().toEpochMilli();
        try {
            return responseBuilder.executeRequest(request);
        } catch (Exception e) {
            throw new HExecuteRequestException("Can't execute request " + request, e);
        }
    }

    @Override
    public HResponse executeRequest(HRequest request, int expectedStatusCode) {
        lastHitInMilliSeconds = Instant.now().toEpochMilli();
        try {
            return responseBuilder.executeRequest(request, expectedStatusCode);
        } catch (Exception e) {
            throw new HExecuteRequestException("Can't execute request " + request, e);
        }
    }

    private void logout() {
        log.info("Ask logout for " + username);
        executeRequest(create(URL_LOGOUT).post(), HttpStatus.SC_MOVED_TEMPORARILY);
        projectGcLogged = false;
        authToken = null;
    }

    private void login(String username, String password) {
        log.info("Ask login for " + username);

        String requestVerificationToken;
        {
            HRequest request = create(URL_LOGIN).get();

            HResponse response = executeRequest(request);

            Document doc = response.toHtml();
            requestVerificationToken = doc.select("input[name='__RequestVerificationToken'][type='hidden']").attr("value");
            log.debug("User requestVerificationToken: " + requestVerificationToken);
        }

        HRequest request = create(URL_LOGIN)
                .addHeaders(HEADERS)
                .addParameter("UserName", username)
                .addParameter("Password", password)
                .addParameter("__RequestVerificationToken", requestVerificationToken)
                .post();

        HResponse response = executeRequest(request);

        log.debug("status code: " + response.getStatusCode());
        log.debug("headers: " + Arrays.toString(response.getResponseHeaders()));

        if (response.getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
            log.info("Log in.");
        } else {
            throw new HLoginException(response.toString());
        }

    }

    @Override
    public void close() {
        log.info("Closing session for " + username);
        try {
            responseBuilder.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            log.info("Cancel session keepAlive timer for " + username);
            timer.cancel();
        }
    }

    public String getUsername() {
        return username;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GcSession gcSession = (GcSession) o;
        return Objects.equals(id, gcSession.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void loginProjectGc() {

        if (projectGcLogged) {
            return;
        }

        log.info("Will log to project-gc.");

        Document responseAsHtml;
        {
            HRequest request = create(URL_PGC_LOGIN).get();
            HResponse response = executeRequest(request, HttpStatus.SC_OK);

            responseAsHtml = response.toHtml();
        }

        {
            HRequest request = create(URL_PGC_AUTH)
                    .addHiddenInputs(responseAsHtml)
                    .addParameter("uxAllowAccessButton", "Allow Access").post();
            HResponse response = executeRequest(request, HttpStatus.SC_MOVED_TEMPORARILY);

            responseAsHtml = response.toHtml();
        }
        {
            String callbackUrl = responseAsHtml.select("a").attr("href");
            HRequest request = create(callbackUrl).get();
            executeRequest(request, HttpStatus.SC_OK);
        }

        projectGcLogged = true;
    }

    public boolean isProjectGcLogged() {
        return projectGcLogged;
    }

    private String getAuthToken() {
        if (authToken == null) {

            HRequest request = create(GcUrls.URL_OAUTH_TOKEN).get();

            HResponse build = executeRequest(request);

            Map<String, Object> responseAsJson = build.toJson();
            authToken = responseAsJson.get("token_type").toString() + " " + responseAsJson.get("access_token").toString();
        }
        return authToken;
    }
}
