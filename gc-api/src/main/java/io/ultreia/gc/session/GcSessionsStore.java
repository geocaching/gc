package io.ultreia.gc.session;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Closeable;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Created by tchemit on 16/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcSessionsStore implements Closeable {

    private static final GcSessionsStore INSTANCE = new GcSessionsStore();

    public static GcSessionsStore get() {
        return INSTANCE;
    }

    private final Set<GcSession> sessions = new LinkedHashSet<>();

    private String neutralUser;

    public Optional<GcSession> getSession(String authId) {

        return sessions.stream().filter(s -> Objects.equals(s.getId(), authId)).findAny();

    }

    public Optional<GcSession> getNeutralSession() {

        return sessions.stream().filter(s -> Objects.equals(s.getUsername(), neutralUser)).findAny();

    }

    public GcSession getSession(String username, String password) {

        Optional<GcSession> gcSession = sessions.stream().filter(s -> Objects.equals(username, s.getUsername())).findAny();
        if (gcSession.isPresent()) {
            return gcSession.get();
        }

        GcSession gcSession1 = new GcSession(username, password);
        sessions.add(gcSession1);
        return gcSession1;

    }

    public void removeSession(String authId) {
        getSession(authId).ifPresent(sessions::remove);
    }

    @Override
    public void close() {
        sessions.forEach(GcSession::close);
    }

    public static String login(String login, String password) {
        return get().getSession(login, password).getId();
    }

    public static void logout(String authId) {
        get().removeSession(authId);
    }

    public void initNeutral(String neutralLogin, String neutralPassword) {
        this.neutralUser = neutralLogin;
        login(neutralLogin, neutralPassword);

    }
}
