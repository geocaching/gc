package io.ultreia.gc.model;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;

/**
 * Created by tchemit on 23/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcCacheImage {

    private String title;
    private String description;
    private transient File content;
    private byte[] fileContent;
    private String fileName;
    private long fileLength;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public File getContent() {
        return content;
    }

    public void setContent(File content) {
        this.content = content;
        if (content != null && content.exists()) {
            fileName = content.getName();
            fileLength = content.length();
            try {
                fileContent = Files.readAllBytes(content.toPath());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GcCacheImage that = (GcCacheImage) o;
        return fileLength == that.fileLength &&
                Objects.equals(fileName, that.fileName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileName, fileLength);
    }

    public byte[] getFileContent() {
        return fileContent;
    }

    public void setFileContent(byte[] fileContent) {
        this.fileContent = fileContent;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }
}
