package io.ultreia.gc.model;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.ImageIcon;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

/**
 * Created by tchemit on 28/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum GcLogType {

    FOUND_IT(true, 2),
    DID_NOT_FOUND(false, 3),
    WRITE_NOTE(false, 4),
    ARCHIVE(false, 5),
    WILL_ATTEND(false, 9),
    ATTENDED(true, 10),
    DISABLE(false, 22),
    ENABLE(false, 23),
    PUBLISH_LISTING(false, 24),
    NEED_MAINTENANCE(false, 45),
    OWNER_MAINTENANCE(false, 46),
    ANNOUNCEMENT(false, 74),;

    private final boolean increaseFoundItCount;
    private final int logType;

    private ImageIcon icon;

    GcLogType(boolean increaseFoundItCount, int logType) {
        this.increaseFoundItCount = increaseFoundItCount;
        this.logType = logType;
    }

    public int getLogType() {
        return logType;
    }

    public ImageIcon getIcon() {
        if (icon == null) {
            icon = SwingUtil.createImageIcon("logType/" + logType + ".png");
        }
        return icon;
    }

    public boolean isIncreaseFoundItCount() {
        return increaseFoundItCount;
    }

    public static GcLogType of(String logType) {
        Integer integer = Integer.valueOf(logType);
        for (GcLogType gcLogType : values()) {
            if (integer == gcLogType.getLogType()) {
                return gcLogType;
            }
        }
        throw new IllegalStateException("Can't deal with logType: " + logType);
    }
}
