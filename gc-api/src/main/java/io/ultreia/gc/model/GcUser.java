package io.ultreia.gc.model;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created by tchemit on 15/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcUser implements GcDto {

    private final String userName;
    private final int userId;
    private final int cachesOwnedInFrance;

    public GcUser(String userName, int userId, int cachesOwnedInFrance) {
        this.userName = userName;
        this.userId = userId;
        this.cachesOwnedInFrance = cachesOwnedInFrance;
    }

    public String getUserName() {
        return userName;
    }

    public int getUserId() {
        return userId;
    }

    public int getCachesOwnedInFrance() {
        return cachesOwnedInFrance;
    }

    public static GcUser fromEntity(io.ultreia.gc.db.entity.GcUser gcUser) {
        return new GcUser(gcUser.getName(), gcUser.getUserId(), gcUser.getCachesOwnedInFrance());
    }
}
