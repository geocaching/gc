package io.ultreia.gc.model;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Created by tchemit on 18/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcArcheoLog implements Comparable<GcArcheoLog> {

    private final String gcName;

    private final GcLog myLog;
    private final List<GcLog> otherLogSameDay;
    private final GcLog previousLog;
    private final GcLog previousLogOtherDay;
    private int nbDays;
    private int score;
    private int nbDaysOtherDay;
    private int scoreOtherDay;

    private static final Map<Integer, Integer> SCORES_TABLES = new LinkedHashMap<>();

    static {
        SCORES_TABLES.put(500, 30);
        SCORES_TABLES.put(365, 20);
        SCORES_TABLES.put(270, 15);
        SCORES_TABLES.put(180, 10);
        SCORES_TABLES.put(90, 5);
    }

    public GcArcheoLog(String gcName, GcLog myLog, GcLog previousLog, GcLog previousLogOtherDay, List<GcLog> otherLogSameDay) {
        this.gcName = gcName;
        this.myLog = myLog;
        this.previousLogOtherDay = previousLogOtherDay;
        this.otherLogSameDay = otherLogSameDay;
        this.previousLog = previousLog;
        computeType();
    }

    public GcArcheoLog resolveConflict() {
        return new GcArcheoLog(gcName, myLog, previousLogOtherDay, null, otherLogSameDay);
    }

    public void computeType() {
        getPreviousLog().ifPresent(pl -> {

            nbDays = (int) (TimeUnit.DAYS.convert(myLog.getLogDate().getTime(), TimeUnit.MILLISECONDS)
                    - TimeUnit.DAYS.convert(pl.getLogDate().getTime(), TimeUnit.MILLISECONDS));

            for (Map.Entry<Integer, Integer> entry : SCORES_TABLES.entrySet()) {
                if (nbDays > entry.getKey()) {
                    score = entry.getValue();
                    break;
                }
            }
        });
        getPreviousLogOtherDay().ifPresent(pl -> {

            nbDaysOtherDay = (int) (TimeUnit.DAYS.convert(myLog.getLogDate().getTime(), TimeUnit.MILLISECONDS)
                    - TimeUnit.DAYS.convert(pl.getLogDate().getTime(), TimeUnit.MILLISECONDS));

            for (Map.Entry<Integer, Integer> entry : SCORES_TABLES.entrySet()) {
                if (nbDaysOtherDay > entry.getKey()) {
                    scoreOtherDay = entry.getValue();
                    break;
                }
            }
        });


    }

    public String getGcName() {
        return gcName;
    }

    public GcLog getMyLog() {
        return myLog;
    }

    public Optional<GcLog> getPreviousLog() {
        return Optional.ofNullable(previousLog);
    }

    public long getNbDays() {
        return nbDays;
    }

    public int getScore() {
        return score;
    }

    public boolean withLogOnSameDay() {
        return !otherLogSameDay.isEmpty();
    }

    public List<GcLog> getOtherLogSameDay() {
        return otherLogSameDay;
    }

    public Optional<GcLog> getPreviousLogOtherDay() {
        return Optional.ofNullable(previousLogOtherDay);
    }

    public boolean withPreviousLogOtherDay() {
        return previousLog != null;
    }

    public int getNbDaysOtherDay() {
        return nbDaysOtherDay;
    }

    public int getScoreOtherDay() {
        return scoreOtherDay;
    }

    @Override
    public int compareTo(GcArcheoLog o) {
        return getMyLog().getLogDate().compareTo(o.getMyLog().getLogDate());
    }
}
