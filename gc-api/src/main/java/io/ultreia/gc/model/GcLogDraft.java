package io.ultreia.gc.model;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jsoup.nodes.Document;

/**
 * Created by tchemit on 25/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcLogDraft {

    private String referenceCode;
    private String gcName;
    private String guid;
    private int logTypeId;
    private String dateLoggedUtc;
    private String dateLoggedGeocacheTime;
    private boolean useFavoritePoint;
    private boolean isArchived;

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public int getLogTypeId() {
        return logTypeId;
    }

    public void setLogTypeId(int logTypeId) {
        this.logTypeId = logTypeId;
    }

    public String getDateLoggedUtc() {
        return dateLoggedUtc;
    }

    public void setDateLoggedUtc(String dateLoggedUtc) {
        this.dateLoggedUtc = dateLoggedUtc;
    }

    public String getDateLoggedGeocacheTime() {
        return dateLoggedGeocacheTime;
    }

    public void setDateLoggedGeocacheTime(String dateLoggedGeocacheTime) {
        this.dateLoggedGeocacheTime = dateLoggedGeocacheTime;
    }

    public boolean isUseFavoritePoint() {
        return useFavoritePoint;
    }

    public void setUseFavoritePoint(boolean useFavoritePoint) {
        this.useFavoritePoint = useFavoritePoint;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public String getGcName() {
        return gcName;
    }

    public void setGcName(String gcName) {
        this.gcName = gcName;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public static GcLogDraft fromXml(Document responseAsHtml) {
        GcLogDraft logDraft = new GcLogDraft();
        logDraft.setReferenceCode(responseAsHtml.select("ReferenceCode").get(0).text());
        logDraft.setArchived(Boolean.valueOf(responseAsHtml.select("IsArchived").text()));
        logDraft.setUseFavoritePoint(Boolean.valueOf(responseAsHtml.select("UseFavoritePoint").text()));
        logDraft.setGuid(responseAsHtml.select("Guid").text());
        logDraft.setLogTypeId(Integer.valueOf(responseAsHtml.select("LogTypeId").text()));
        logDraft.setDateLoggedUtc(responseAsHtml.select("DateLoggedUtc").text());
        logDraft.setDateLoggedGeocacheTime(responseAsHtml.select("DateLoggedGeocacheTime").text());
        logDraft.setGcName(responseAsHtml.select("Geocache > ReferenceCode  ").text());
        return logDraft;
    }
}
