package io.ultreia.gc.model;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.net.URL;

/**
 * Created by tchemit on 25/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcTrackable {

    private String referenceCode;
    private URL iconUrl;
    private String name;
    private double distanceTraveledInMiles;
    private double distanceTraveledInKilometers;
    private String currentGoal;
    private String description;
    private String dateReleased;
    private boolean allowedToBeCollected;
    private GcTrackableLocationReleased locationReleased;
    private GcTrackableOwner owner;
    private GcTrackableHolder holder;
    private boolean inHolderCollection;
    private boolean isMissing;
    private boolean isActive;
    private boolean isLocked;
    private String trackingNumberSha512Hash;
    private int journeyStepsCount;
    private int ownerImagesCount;
    private int activityImagesCount;
    private int activityCount;
    private GcTrackableType trackableType;

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public URL getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(URL iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDistanceTraveledInMiles() {
        return distanceTraveledInMiles;
    }

    public void setDistanceTraveledInMiles(double distanceTraveledInMiles) {
        this.distanceTraveledInMiles = distanceTraveledInMiles;
    }

    public double getDistanceTraveledInKilometers() {
        return distanceTraveledInKilometers;
    }

    public void setDistanceTraveledInKilometers(double distanceTraveledInKilometers) {
        this.distanceTraveledInKilometers = distanceTraveledInKilometers;
    }

    public String getCurrentGoal() {
        return currentGoal;
    }

    public void setCurrentGoal(String currentGoal) {
        this.currentGoal = currentGoal;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateReleased() {
        return dateReleased;
    }

    public void setDateReleased(String dateReleased) {
        this.dateReleased = dateReleased;
    }

    public boolean isAllowedToBeCollected() {
        return allowedToBeCollected;
    }

    public void setAllowedToBeCollected(boolean allowedToBeCollected) {
        this.allowedToBeCollected = allowedToBeCollected;
    }

    public GcTrackableLocationReleased getLocationReleased() {
        return locationReleased;
    }

    public void setLocationReleased(GcTrackableLocationReleased locationReleased) {
        this.locationReleased = locationReleased;
    }

    public GcTrackableOwner getOwner() {
        return owner;
    }

    public void setOwner(GcTrackableOwner owner) {
        this.owner = owner;
    }

    public GcTrackableHolder getHolder() {
        return holder;
    }

    public void setHolder(GcTrackableHolder holder) {
        this.holder = holder;
    }

    public boolean isInHolderCollection() {
        return inHolderCollection;
    }

    public void setInHolderCollection(boolean inHolderCollection) {
        this.inHolderCollection = inHolderCollection;
    }

    public boolean isMissing() {
        return isMissing;
    }

    public void setMissing(boolean missing) {
        isMissing = missing;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    public String getTrackingNumberSha512Hash() {
        return trackingNumberSha512Hash;
    }

    public void setTrackingNumberSha512Hash(String trackingNumberSha512Hash) {
        this.trackingNumberSha512Hash = trackingNumberSha512Hash;
    }

    public int getJourneyStepsCount() {
        return journeyStepsCount;
    }

    public void setJourneyStepsCount(int journeyStepsCount) {
        this.journeyStepsCount = journeyStepsCount;
    }

    public int getOwnerImagesCount() {
        return ownerImagesCount;
    }

    public void setOwnerImagesCount(int ownerImagesCount) {
        this.ownerImagesCount = ownerImagesCount;
    }

    public int getActivityImagesCount() {
        return activityImagesCount;
    }

    public void setActivityImagesCount(int activityImagesCount) {
        this.activityImagesCount = activityImagesCount;
    }

    public int getActivityCount() {
        return activityCount;
    }

    public void setActivityCount(int activityCount) {
        this.activityCount = activityCount;
    }

    public GcTrackableType getTrackableType() {
        return trackableType;
    }

    public void setTrackableType(GcTrackableType trackableType) {
        this.trackableType = trackableType;
    }
}
