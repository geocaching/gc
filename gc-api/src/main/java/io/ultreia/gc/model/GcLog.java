package io.ultreia.gc.model;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * logDate by tchemit on 18/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcLog implements GcDto {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcLog.class);

    private String topiaId;

    public static GcLog fromMap(Map map) {
        GcLog result = new GcLog();
        result.setLogId(((Double) map.get("LogID")).longValue());
        result.setCacheId(((Double) map.get("CacheID")).longValue());
        result.setLogGuid((String) map.get("LogGuid"));
        result.setLogType((String) map.get("LogType"));
        result.setLogText((String) map.get("LogText"));
        try {
            result.setLogDate(new SimpleDateFormat("dd/MM/yyyy").parse((String) map.get("Created")));
        } catch (ParseException e) {
            log.error(e);
        }
        result.setUserName((String) map.get("UserName"));
        return result;
    }

    private long logId;
    private long cacheId;
    private String logGuid;
    private String logType;
    private String logText;
    private Date logDate;
    private String userName;

    public boolean isFoundByUser(String userName) {
        return isFound() && userName.equals(getUserName());
    }

    private boolean isWebCamPhotoTaken() {
        return "Webcam Photo Taken" .equals(logType);
    }

    public boolean isFound() {
        return isFoundIt() || isAttended() || isWebCamPhotoTaken();
    }

    public boolean isFoundIt() {
        return "Found it" .equals(logType);
    }

    public boolean isAttended() {
        return "Attended" .equals(logType);
    }

    public int getLogId() {
        return (int) logId;
    }

    public void setLogId(long logId) {
        this.logId = logId;
    }

    public long getCacheId() {
        return cacheId;
    }

    public void setCacheId(long cacheId) {
        this.cacheId = cacheId;
    }

    public String getLogGuid() {
        return logGuid;
    }

    public void setLogGuid(String logGuid) {
        this.logGuid = logGuid;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public String getLogText() {
        return logText;
    }

    public void setLogText(String logText) {
        this.logText = logText;
    }

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date created) {
        this.logDate = created;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public static synchronized GcLog fromEntity(io.ultreia.gc.db.entity.GcCacheLog entity) {
        GcLog result = new GcLog();
        result.setLogId(entity.getLogId());
        result.setLogText(entity.getLogText());
        result.setLogGuid(entity.getLogGuid());
        result.setLogType(entity.getLogType());
        result.setLogDate(entity.getLogDate());
        result.setUserName(entity.getUser());
        return result;
    }

    public String getTopiaId() {
        return topiaId;
    }

    public void setTopiaId(String topiaId) {
        this.topiaId = topiaId;
    }
}
