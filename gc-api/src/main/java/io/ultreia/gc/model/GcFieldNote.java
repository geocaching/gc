package io.ultreia.gc.model;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.db.entity.GcCacheType;
import java.util.Optional;


import static io.ultreia.gc.service.internal.GcUrls.URL_CACHE;
import static io.ultreia.gc.service.internal.GcUrls.URL_COMPOSE_LOG;

/**
 * Created by tchemit on 15/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcFieldNote implements GcDto {

    private final String name;
    private final String url;
    private final String composeUrl;
    private final String date;
    private final GcLogType logType;
    private final GcCacheType gcCacheType;
    private GcCacheImage image;
    private boolean favorite;
    private boolean selected;
    private final String draftGuid;
    private final String referenceCode;
    private final String gc;

    public GcFieldNote(String name, String gc, String referenceCode, String draftGuid, String date, String logType, GcCacheType gcCacheType) {
        this.name = name;
        this.url = String.format(URL_CACHE, gc);
        this.composeUrl = String.format(URL_COMPOSE_LOG, gc, referenceCode, draftGuid, logType);
        this.date = date;
        this.referenceCode = referenceCode;
        this.draftGuid = draftGuid;
        this.logType = GcLogType.of(logType);
        this.gcCacheType = gcCacheType;
        this.gc = gc;
    }

    public String getGc() {
        return gc;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setImage(GcCacheImage image) {
        this.image = image;
    }

    public Optional<GcCacheImage> getImage() {
        return Optional.ofNullable(image);
    }

    public String getDate() {
        return date;
    }

    public GcLogType getLogType() {
        return logType;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public String getComposeUrl() {
        return composeUrl;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public GcCacheType getGcCacheType() {
        return gcCacheType;
    }

    @Override
    public String toString() {
        return String.format("%s - %s (%s)", name, date, logType);
    }

    public boolean withImage() {
        return image != null;
    }
}
