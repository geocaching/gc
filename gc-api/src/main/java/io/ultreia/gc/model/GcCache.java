package io.ultreia.gc.model;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.db.entity.GcCacheType;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Created by tchemit on 19/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcCache implements GcDto {

    private String gcName;
    private String gcId;
    private String topiaId;
    private String lat;
    private String oldLat;
    private String lon;
    private String oldLon;
    private String time;
    private String desc;
    private String url;
    private String urlname;
    private String sym;
    private String gpxType;
    private String gcType;
    private boolean available = true;
    private boolean archived;
    private String placedby;
    private String ownerId;
    private String owner;
    private String container;
    private float difficulty;
    private float terrain;
    private String country;
    private String state;
    private boolean shortDescHtml = true;
    private String shortDesc;
    private boolean longDescHtml = true;
    private String longDesc;
    private String hint;
    private boolean isPremium;
    private int favPoints;
    private String userNote;
    private Set<String> logs;
    private Set<GcCacheAttribute> attributes;
    private Set<String> waypoints;
    private boolean hasGsakExt = true;
    private Integer elevation;

    public String getGcName() {
        return gcName;
    }

    public void setGcName(String gcName) {
        this.gcName = gcName;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getOldLat() {
        return oldLat;
    }

    public void setOldLat(String oldLat) {
        this.oldLat = oldLat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getOldLon() {
        return oldLon;
    }

    public void setOldLon(String oldLon) {
        this.oldLon = oldLon;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlname() {
        return urlname;
    }

    public void setUrlname(String urlname) {
        this.urlname = urlname;
    }

    public String getSym() {
        return sym;
    }

    public void setSym(String sym) {
        this.sym = sym;
    }

    public String getGpxType() {
        return gpxType;
    }

    public void setGpxType(String gpxType) {
        this.gpxType = gpxType;
    }

    public String getGcType() {
        return gcType;
    }

    public void setGcType(String gcType) {
        this.gcType = gcType;
    }

    public String getGcId() {
        return gcId;
    }

    public void setGcId(String gcId) {
        this.gcId = gcId;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public String getPlacedby() {
        return placedby;
    }

    public void setPlacedby(String placedby) {
        this.placedby = placedby;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getContainer() {
        return container;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    public float getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(float difficulty) {
        this.difficulty = difficulty;
    }

    public float getTerrain() {
        return terrain;
    }

    public void setTerrain(float terrain) {
        this.terrain = terrain;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isShortDescHtml() {
        return shortDescHtml;
    }

    public void setShortDescHtml(boolean shortDescHtml) {
        this.shortDescHtml = shortDescHtml;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortdesc) {
        this.shortDesc = shortdesc;
    }

    public boolean isLongDescHtml() {
        return longDescHtml;
    }

    public void setLongDescHtml(boolean longDescHtml) {
        this.longDescHtml = longDescHtml;
    }

    public String getLongDesc() {
        return longDesc;
    }

    public void setLongDesc(String longDesc) {
        this.longDesc = longDesc;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }

    public int getFavPoints() {
        return favPoints;
    }

    public void setFavPoints(int favPoints) {
        this.favPoints = favPoints;
    }

    public String getUserNote() {
        return userNote;
    }

    public void setUserNote(String userNote) {
        this.userNote = userNote;
    }

    public Set<String> getLogs() {
        return logs;
    }

    public void setLogs(Set<String> logs) {
        this.logs = logs;
    }

    public Set<GcCacheAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(Set<GcCacheAttribute> attributes) {
        this.attributes = attributes;
    }

    public Set<String> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(Set<String> waypoints) {
        this.waypoints = waypoints;
    }

    public boolean isHasGsakExt() {
        return hasGsakExt;
    }

    public void setHasGsakExt(boolean hasGsakExt) {
        this.hasGsakExt = hasGsakExt;
    }

    public Integer getElevation() {
        return elevation;
    }

    public void setElevation(Integer elevation) {
        this.elevation = elevation;
    }

    public static GcCache fromXml(Document responseAsHtml) {
        GcCache gcCache = new GcCache();
        gcCache.setGcName(responseAsHtml.select("ReferenceCode").get(0).text());
        gcCache.setGcId(responseAsHtml.select("Id").text());

        gcCache.setLat(responseAsHtml.select("PostedCoordinates > Latitude").text());
        gcCache.setLon(responseAsHtml.select("PostedCoordinates > Longitude").text());

        gcCache.setOwnerId(responseAsHtml.select("Owner > Id").text());
        gcCache.setOwner(responseAsHtml.select("Owner > ReferenceCode").text());
        gcCache.setGcType(responseAsHtml.select("GeocacheType > Name").text());
        return gcCache;
    }

    public static GcCache fromHtmlSearch(Element element) {
        GcCache gcCache = new GcCache();
        gcCache.setGcName(element.attr("data-id"));
        gcCache.setGcType(element.select("span[class='cache-details']").text().split("\\|")[0].trim());
        gcCache.setDesc(element.attr("data-name"));
        gcCache.setOwner(element.select("span[class='owner']").text().substring(3).trim());
        gcCache.setDifficulty(Float.valueOf(element.select("td[data-column='Difficulty']").text()));
        gcCache.setTerrain(Float.valueOf(element.select("td[data-column='Terrain']").text()));
        return gcCache;
    }

    private static final Pattern USER_ID_PATTERN = Pattern.compile("(.+)?guid=([^&]+)?&(.+)?");

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcCache.class);

    public static GcCache fromGCHtml(Document doc) {
        GcCache result = new GcCache();
        result.setGcName(doc.select("span[id='ctl00_ContentBody_CoordInfoLinkControl1_uxCoordInfoCode']").text());
        result.setOwner(doc.select("div[id='ctl00_ContentBody_mcd1'] > a").text());
        String href = doc.select("div[id='ctl00_ContentBody_mcd1'] > a").attr("href");
        Matcher matcher = USER_ID_PATTERN.matcher(href);
        if (matcher.matches()) {
            result.setOwnerId(matcher.group(2));
        }
        String typeImageName = doc.select("div[id='cacheDetails']>p>a>img").attr("src");
        try {
            String typeImageName2 = StringUtils.removeEnd(typeImageName, ".gif");
            int i = typeImageName2.lastIndexOf("/");
            String gcTypeCode = typeImageName2.substring(i + 1);
            GcCacheType gcCacheType = GcCacheType.of(gcTypeCode);
            Objects.requireNonNull(gcCacheType);
            result.setGcType(gcCacheType.getLabel());
        } catch (Exception e) {
            log.error("Can't get gc cache type from " + typeImageName, e);
            result.setGcType(doc.select("div[id='cacheDetails']>p>a>img").attr("title"));
        }

        result.setTerrain(Float.valueOf(doc.select("span[id='ctl00_ContentBody_Localize12'] > img").attr("alt").split(" ")[0]));
        result.setDifficulty(Float.valueOf(doc.select("span[id='ctl00_ContentBody_uxLegendScale'] > img").attr("alt").split(" ")[0]));

//        result.setOwnerId(String.valueOf(entity.getOwner().getUserId()));
        result.setLat(doc.select("a[id='ctl00_ContentBody_uxViewLargerMap']").attr("href").split("=")[1].split("&")[0]);
        result.setLon(doc.select("a[id='ctl00_ContentBody_uxViewLargerMap']").attr("href").split("=")[2]);
        result.setHint(doc.select("div[id='div_hint']").text());
        result.setGcId(doc.select("link[href*='cache_details.aspx?guid']").attr("href").split("=")[1]);
        result.setTime(doc.select("div[id='ctl00_ContentBody_mcd2']").text().split(" ")[2]);
        result.setShortDesc(doc.select("span[id='ctl00_ContentBody_ShortDescription']").text());
//        result.setShortDescHtml(entity.isShortDescHtml());
        result.setLongDesc(doc.select("span[id='ctl00_ContentBody_LongDescription']").text());
//        result.setLongDescHtml(entity.isLongDescHtml());
//        result.setPremium(entity.isPremium());

        String location = doc.select("span[id='ctl00_ContentBody_Location']").text().substring(3);

        if (location.contains(",")) {
            result.setState(location.split(",")[0]);
            result.setCountry(location.split(",")[1].trim());
        } else {
            result.setCountry(location.trim());
        }

        result.setContainer(doc.select("div[id='ctl00_ContentBody_size']>p>span>small").text().replace("(", "").replace(")", ""));
//        result.setArchived(entity.isArchived());
//        result.setAvailable(entity.isAvailable());
        result.setUrl("https://coord.info/" + result.getGcName());
        result.setUrlname(doc.select("span[id='ctl00_ContentBody_CacheName']").text());
        result.setPlacedby(doc.select("div[id='ctl00_ContentBody_mcd1']>a").text());
        result.setDesc(result.getUrlname() + " by " + result.getPlacedby() + ", " + result.getGcType() + " (" + result.getDifficulty() + "/" + result.getTerrain() + ")");
        return result;
    }

    public static GcCache fromEntity(io.ultreia.gc.db.entity.GcCache entity) {
        GcCache result = new GcCache();
        result.setGcName(entity.getGcName());
        if (entity.getOwner() != null) {
            result.setOwner(entity.getOwner().getName());
            result.setOwnerId(String.valueOf(entity.getOwner().getUserId()));
        }
        result.setGcType(entity.getGcType());
        result.setTerrain(entity.getTerrain().getCode());
        result.setDifficulty(entity.getDifficulty().getCode());
        result.setDesc(entity.getDescription());
        result.setLat(String.valueOf(entity.getLat()));
        result.setLon(String.valueOf(entity.getLon()));
        result.setHint(entity.getHint());
        result.setGcId(entity.getGcId());
        result.setTime(String.valueOf(entity.getTime()));
        result.setShortDesc(entity.getShortDesc());
        result.setShortDescHtml(entity.isShortDescHtml());
        result.setLongDesc(entity.getLongDesc());
        result.setLongDescHtml(entity.isLongDescHtml());
        result.setPremium(entity.isPremium());
//        result.setState(entity.getRegion().getName());
//        result.setCountry(entity.getCountry().getName());
        result.setContainer(entity.getContainer());
        result.setArchived(entity.isArchived());
        result.setAvailable(entity.isAvailable());
        result.setUrl(entity.getUrl());
        result.setUrlname(entity.getUrlname());
        result.setTopiaId(entity.getTopiaId());
        result.setElevation(entity.getElevation());
        return result;
    }

    public String getTopiaId() {
        return topiaId;
    }

    public void setTopiaId(String topiaId) {
        this.topiaId = topiaId;
    }

    public boolean equalsCoordinates(GcCache o) {
        return Objects.equals(lat, o.getLat()) && Objects.equals(lon, o.getLon());
    }
}
