package io.ultreia.gc.model;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 18/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcArcheoLogsComputationResult {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcArcheoLogsComputationResult.class);

    private final TreeMap<Integer, List<GcArcheoLog>> data;
    private final Set<String> badGcNames;

    private int totalPoints;

    public GcArcheoLogsComputationResult() {
        data = new TreeMap<>();
        data.put(0, new LinkedList<>());
        data.put(5, new LinkedList<>());
        data.put(10, new LinkedList<>());
        data.put(15, new LinkedList<>());
        data.put(20, new LinkedList<>());
        data.put(30, new LinkedList<>());
        badGcNames = new LinkedHashSet<>();
    }

    public void addBadGcName(String gcName) {
        badGcNames.add(gcName);
    }

    public Set<String> getBadGcNames() {
        return badGcNames;
    }

    public boolean addResult(GcArcheoLog archeoLog) {
        int score = archeoLog.getScore();
        if (score > 0) {
            data.get(score).add(archeoLog);
            totalPoints += score;
            log.info(String.format("[Total: %d] Found a %s points cache: %s", totalPoints, score, archeoLog.getGcName()));
            return true;
        }
        return false;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public boolean addConflict(GcArcheoLog archeoLog) {
        int score = archeoLog.getScore();
        if (score == 0 && archeoLog.withLogOnSameDay() && archeoLog.withPreviousLogOtherDay() && archeoLog.getScoreOtherDay() > 0) {

            data.get(0).add(archeoLog);
            totalPoints += score;
            log.info(String.format("[Total: %d] Found a %s points cache: %s", totalPoints, score, archeoLog.getGcName()));
            return true;
        }
        return false;
    }

    public void sort() {
        for (List<GcArcheoLog> logs : data.values()) {
            logs.sort(GcArcheoLog::compareTo);
        }
    }
}
