package io.ultreia.gc.model;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.db.entity.GcDifficulty;
import io.ultreia.gc.db.entity.GcTerrain;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Created by tchemit on 10/05/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcCombination {

    private final GcDifficulty difficulty;
    private final GcTerrain terrain;

    public GcCombination(GcDifficulty difficulty, GcTerrain terrain) {
        this.difficulty = difficulty;
        this.terrain = terrain;
    }

    public GcDifficulty getDifficulty() {
        return difficulty;
    }

    public GcTerrain getTerrain() {
        return terrain;
    }

    @Override
    public String toString() {
        return String.format("D%sT%s", difficulty.toString(), terrain.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GcCombination that = (GcCombination) o;
        return difficulty == that.difficulty && terrain == that.terrain;
    }

    @Override
    public int hashCode() {
        return Objects.hash(difficulty, terrain);
    }

    public static Iterator<GcCombination> newIterator(boolean reverse) {

        List<GcDifficulty> gcDifficulties = Arrays.asList(GcDifficulty.values());
        if (reverse) {
            Collections.reverse(gcDifficulties);
        }
        Iterator<GcDifficulty> difficulties = gcDifficulties.iterator();

        return new Iterator<GcCombination>() {


            Iterator<GcTerrain> terrains;

            GcDifficulty difficulty;
            GcTerrain terrain;

            @Override
            public boolean hasNext() {
                return terrains == null || terrains.hasNext() || difficulties.hasNext();
            }

            @Override
            public GcCombination next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                if (terrains == null || !terrains.hasNext()) {
                    List<GcTerrain> terrains = Arrays.asList(GcTerrain.values());
                    if (reverse) {
                        Collections.reverse(terrains);
                    }
                    this.terrains = terrains.iterator();
                    difficulty = difficulties.next();
                }
                terrain = terrains.next();

                return new GcCombination(difficulty, terrain);
            }
        };
    }
}
