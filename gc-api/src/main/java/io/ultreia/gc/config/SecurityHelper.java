package io.ultreia.gc.config;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by tchemit on 04/11/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SecurityHelper {

    private final Base64.Encoder encoder = Base64.getEncoder().withoutPadding();
    private final Base64.Decoder decoder = Base64.getDecoder();
    private final Cipher cipherDecode;
    private final Cipher cipherEncode;

    public SecurityHelper(String seed) {
        byte[] encoded = decode(seed);
        SecretKey key = new SecretKeySpec(encoded, "BlowFish");
        try {
            cipherDecode = Cipher.getInstance("BlowFish");
            cipherDecode.init(Cipher.DECRYPT_MODE, key);
            cipherEncode = Cipher.getInstance("BlowFish");
            cipherEncode.init(Cipher.ENCRYPT_MODE, key);
        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException e) {
            throw new IllegalStateException(e);
        }
    }

    public byte[] encode(byte[] text) {
        return encoder.encode(text);
    }

    public byte[] decode(String text) {
        return decoder.decode(text);
    }

    public String decrypt(String text) {
        try {
            return new String(cipherDecode.doFinal(decode(text)));
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new IllegalStateException(e);
        }
    }

    public String crypt(String text) {
        try {
            return new String(encode(cipherEncode.doFinal(text.getBytes())));
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new IllegalStateException(e);
        }
    }
}
