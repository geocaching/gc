package io.ultreia.gc.config;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import org.nuiton.config.ConfigOptionDef;

public enum GcApiConfigOption implements ConfigOptionDef {

    CONFIG_FILE(
            String.class,
            "config.file",
            "Configuration filename",
            "gc.conf",
            true,
            false),

    DATA_DIRECTORY(
            File.class,
            "gc.dataDirectory",
            "Data directory",
            "${user.home}/.gc",
            false,
            false),
    TEMPORARY_DIRECTORY(
            File.class,
            "gc.temporaryDirectory",
            "Temporary directory",
            "${gc.dataDirectory}/temp",
            false,
            false),

    BUILD_DATE(
            String.class,
            "gc.build.date",
            "Build date",
            null,
            true,
            false),

    BUILD_NUMBER(
            String.class,
            "gc.build.number",
            "Build Number",
            null,
            true,
            false),

    BUILD_VERSION(
            org.nuiton.version.Version.class,
            "gc.build.version",
            "Build Version",
            null,
            true,
            false),

    CLASSIFIER(
            String.class,
            "gc.classifier",
            "Instance classifier",
            "stable",
            false,
            false),


    LOGIN(
            String.class,
            "gc.auth.login",
            "Geocaching.com login",
            null,
            false,
            false),

    PASSWORD(
            String.class,
            "gc.auth.password",
            "Geocaching.com password",
            null,
            false,
            false);

    private final Class type;

    private final String key;

    private final String description;

    private String defaultValue;

    private boolean _transient;

    private boolean _final;

    GcApiConfigOption(Class type, String key, String description, String defaultValue, boolean _transient, boolean _final) {
        this.type = type;
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this._final = _final;
        this._transient = _transient;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public boolean isTransient() {
        return _transient;
    }

    @Override
    public void setTransient(boolean _transient) {
        this._transient = _transient;
    }

    @Override
    public boolean isFinal() {
        return _final;
    }

    @Override
    public void setFinal(boolean _final) {
        this._final = _final;
    }

} //GcApiConfigOption
