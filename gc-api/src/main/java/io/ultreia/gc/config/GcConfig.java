package io.ultreia.gc.config;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.util.function.Supplier;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.version.Version;

/**
 * Created by tchemit on 24/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface GcConfig extends Supplier<ApplicationConfig> {
    @Override
    ApplicationConfig get();

    File getDataDirectory();

    File getTemporaryDirectory();

    String getConfigFile();

    String getBuildDate();

    String getBuildNumber();

    Version getBuildVersion();

    String getLogin();

    void setLogin(String login);

    String getPassword();

    void setPassword(String password);

    String getClassifier();

    void setClassifier(String classifier);

    SecurityHelper getSecurityHelper();
}
