package io.ultreia.gc.config;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ApplicationConfigInit;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.version.Version;

public class GcApiConfig implements GcConfig {

    private ApplicationConfig applicationConfig;

    private static final String INTERNAL_KEY = "10yjCSpKiLXcEAjLo9U2rw";

    private final SecurityHelper securityHelper;

    public GcApiConfig() {
        this(ApplicationConfigInit.forAllScopes());
    }

    protected GcApiConfig(ApplicationConfigInit init) {
        this.applicationConfig = new ApplicationConfig(init);
        this.applicationConfig.loadDefaultOptions(GcApiConfigOption.values());
        this.securityHelper = new SecurityHelper(INTERNAL_KEY);
    }

    @Override
    public ApplicationConfig get() {
        return applicationConfig;
    }

    protected void setOption(String key, Object attrName) {
        applicationConfig.setOption(key, String.valueOf(attrName));
    }

    public Path getMyFindsGpx() {
        return getDataDirectory().toPath().resolve(String.format("myfinds-%s.gpx", getLogin()));
    }

    public Path getMyFindsJson() {
        return getDataDirectory().toPath().resolve(String.format("myfinds-%s.json", getLogin()));
    }

    @Override
    public File getDataDirectory() {
        return applicationConfig.getOptionAsFile(GcApiConfigOption.DATA_DIRECTORY.getKey());
    }

    @Override
    public String getConfigFile() {
        return applicationConfig.getOption(GcApiConfigOption.CONFIG_FILE.getKey());
    }

    public void setConfigFile(String configFile) {
        setOption(GcApiConfigOption.CONFIG_FILE.getKey(), configFile);
    }

    @Override
    public String getBuildDate() {
        return applicationConfig.getOption(GcApiConfigOption.BUILD_DATE.getKey());
    }

    public void setBuildDate(String buildDate) {
        setOption(GcApiConfigOption.BUILD_DATE.getKey(), buildDate);
    }

    @Override
    public String getBuildNumber() {
        return applicationConfig.getOption(GcApiConfigOption.BUILD_NUMBER.getKey());
    }

    public void setBuildNumber(String buildNumber) {
        setOption(GcApiConfigOption.BUILD_NUMBER.getKey(), buildNumber);
    }

    @Override
    public Version getBuildVersion() {
        return applicationConfig.getOptionAsVersion(GcApiConfigOption.BUILD_VERSION.getKey());
    }

    public void setBuildVersion(Version buildVersion) {
        setOption(GcApiConfigOption.BUILD_VERSION.getKey(), buildVersion);
    }

    @Override
    public String getLogin() {
        return applicationConfig.getOption(GcApiConfigOption.LOGIN.getKey());
    }

    @Override
    public void setLogin(String login) {
        setOption(GcApiConfigOption.LOGIN.getKey(), login);
    }

    @Override
    public String getPassword() {
        return applicationConfig.getOption(GcApiConfigOption.PASSWORD.getKey());
    }

    @Override
    public void setPassword(String password) {
        setOption(GcApiConfigOption.PASSWORD.getKey(), password);
    }

    @Override
    public String getClassifier() {
        return applicationConfig.getOption(GcApiConfigOption.CLASSIFIER.getKey());
    }

    @Override
    public void setClassifier(String classifier) {
        setOption(GcApiConfigOption.CLASSIFIER.getKey(), classifier);
    }

    public void setDataDirectory(File dataDirectory) {
        setOption(GcApiConfigOption.DATA_DIRECTORY.getKey(), dataDirectory.getAbsolutePath());
    }

    public String getConfigurationDescription() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n=====================================================================================================================");
        builder.append("\n=== Configuration ===================================================================================================");
        builder.append("\n=====================================================================================================================");
        builder.append(String.format("\n=== %1$-40s = %2$s", "Filename", get().getConfigFileName()));
        for (GcApiConfigOption option : orderedByKey()) {
            builder.append(String.format("\n=== %1$-40s = %2$s", option.getKey(), get().getOption(option.getKey())));
        }
        builder.append("\n=====================================================================================================================");
        return builder.toString();
    }

    protected List<GcApiConfigOption> orderedByKey() {
        List<GcApiConfigOption> values = Arrays.asList(GcApiConfigOption.values());
        values.sort(Comparator.comparing(GcApiConfigOption::getKey));
        return Collections.unmodifiableList(values);
    }

    public static GcApiConfig create(String... args) {
        GcApiConfig gcApiConfig = new GcApiConfig();
        try {
            gcApiConfig.get().parse(args);
        } catch (ArgumentsParserException e) {
            throw new IllegalStateException("can't parse configuration arguments", e);
        }
        return gcApiConfig;
    }

    public Path createTempFile(String s) {
        return getTemporaryDirectory().toPath().resolve(s + System.nanoTime());
    }

    public File getTemporaryDirectory() {
        return applicationConfig.getOptionAsFile(GcApiConfigOption.TEMPORARY_DIRECTORY.getKey());
    }

    @Override
    public SecurityHelper getSecurityHelper() {
        return securityHelper;
    }
}
