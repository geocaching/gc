package io.ultreia.gc.ui.actions;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.ui.AfterLoginUI;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

/**
 * Created by tchemit on 15/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class UIActionSupport<UI extends AfterLoginUI> extends AbstractAction {

    protected final UI ui;

    protected UIActionSupport(UI ui, KeyStroke keyStroke) {
        this.ui = ui;
        putValue(NAME, getClass().getName());
        putValue(ACCELERATOR_KEY, keyStroke);

        InputMap inputMap = ui.getContentPanel().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = ui.getContentPanel().getActionMap();

        inputMap.put((KeyStroke) getValue(ACCELERATOR_KEY), getClass().getName());
        actionMap.put(getClass().getName(), this);

    }

}
