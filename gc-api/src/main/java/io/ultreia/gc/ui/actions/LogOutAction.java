package io.ultreia.gc.ui.actions;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.ui.AfterLoginUI;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

/**
 * Created by tchemit on 17/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LogOutAction<UI extends AfterLoginUI> extends UIActionSupport<UI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LogOutAction.class);
    public static final ImageIcon ICON_LOGIN = SwingUtil.createIcon("/icons/auth_login.png");
    public static final ImageIcon ICON_LOGOUT = SwingUtil.createIcon("/icons/auth_logout.png");

    public LogOutAction(UI ui) {
        super(ui, KeyStroke.getKeyStroke("alt pressed O"));
        putValue(Action.MNEMONIC_KEY, (int) 'O');
        putValue(Action.SMALL_ICON, ICON_LOGIN);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        log.info("Log out");

        ui.setBusy(true);
        try {
            ui.logOut();
        } finally {
            ui.setBusy(false);
        }

    }

}
