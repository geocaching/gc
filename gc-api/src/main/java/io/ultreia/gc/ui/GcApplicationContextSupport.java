package io.ultreia.gc.ui;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.config.GcConfig;
import io.ultreia.gc.service.api.GcServiceContextSupport;
import io.ultreia.gc.session.GcSessionsStore;
import java.io.Closeable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ExecutorService;
import javax.swing.SwingWorker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 19/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class GcApplicationContextSupport<C extends GcConfig> extends GcServiceContextSupport implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcApplicationContextSupport.class);

    private final C config;

    protected GcApplicationContextSupport(C config) {
        this.config = config;
    }

    public C getConfig() {
        return config;
    }

    @Override
    public void close() {

        getAuthId().ifPresent(GcSessionsStore::logout);

        GcSessionsStore.get().close();

        try {
            Method method = SwingWorker.class.getDeclaredMethod("getWorkersExecutorService");
            method.setAccessible(true);
            ExecutorService invoke = (ExecutorService) method.invoke(null);
            log.info("close executor service: " + invoke);
            invoke.shutdown();
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }
}
