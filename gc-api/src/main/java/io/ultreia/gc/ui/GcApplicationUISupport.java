package io.ultreia.gc.ui;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.config.GcConfig;
import io.ultreia.gc.session.GcSessionsStore;
import io.ultreia.gc.ui.actions.LogOutAction;
import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import org.jdesktop.swingx.JXLoginPane;
import org.nuiton.jaxx.runtime.swing.SwingUtil;

/**
 * Created by tchemit on 15/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class GcApplicationUISupport<C extends GcConfig, Context extends GcApplicationContextSupport<C>, M, Content extends ContentPanelUI<C, Context, M>> implements AfterLoginUI<C, Context> {

    private final Context applicationContext;

    private final JFrame frame;
    private final ProgressPanelUI progressPanel;

    private final Content contentPanel;
    private final M model;
    private final String title;

    protected void applyTitle(String connexion) {
        frame.setTitle(String.format(title, connexion));
    }

    protected GcApplicationUISupport(Context applicationContext, M model, String title) {
        this.applicationContext = applicationContext;
        this.model = model;
        this.progressPanel = new ProgressPanelUI();

        C config = applicationContext.getConfig();

        this.title = String.format("%s - v%s (build %s : %s) (%%s)", title, config.getBuildVersion(), config.getBuildDate(), config.getBuildNumber());
        frame = new JFrame();
        applyTitle("Utilisateur non connecté");
        frame.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                applicationContext.close();
            }
        });

        contentPanel = createContentPanel(model);
        contentPanel.installActions(applicationContext, model);

        frame.add(contentPanel, BorderLayout.CENTER);

        frame.setMinimumSize(new Dimension(1280, 600));
        frame.setModalExclusionType(Dialog.ModalExclusionType.NO_EXCLUDE);
        frame.setVisible(true);

        frame.add(progressPanel, BorderLayout.SOUTH);

    }

    public final M getModel() {
        return model;
    }

    protected abstract void afterLogOut();

    protected abstract Content createContentPanel(M model);

    public final void logIn(boolean autoLogin) {

        LoginPanelUI loginPanel = LoginPanelUI.create(this, applicationContext.getConfig(), applicationContext);
        JXLoginPane.JXLoginDialog loginDialog = new JXLoginPane.JXLoginDialog(frame, loginPanel);
        loginPanel.init(autoLogin);
        SwingUtil.center(frame, loginDialog);
        loginDialog.setVisible(true);

    }

    @Override
    public final Content getContentPanel() {
        return contentPanel;
    }

    public final ProgressPanelUI getProgressPanel() {
        return progressPanel;
    }

    @Override
    public final void setBusy(boolean b) {
        getContentPanel().setBusy(b);
    }

    @Override
    public void afterLogin(String name, String authId) {

        applicationContext.setAuthId(authId);

        applyTitle("Utilisateur connecté: " + name);

        getContentPanel().getLogOut().setText("Log out");
        getContentPanel().getLogOut().setIcon(LogOutAction.ICON_LOGOUT);

    }

    @Override
    public final void logOut() {

        try {
            if (applicationContext.getAuthId().isPresent()) {

                GcSessionsStore.logout(applicationContext.getAuthId().get());
            }
            applicationContext.setAuthId(null);
        } finally {

            applyTitle("Utilisateur non connecté");

            getContentPanel().getLogOut().setText("Log in");
            getContentPanel().getLogOut().setIcon(LogOutAction.ICON_LOGIN);

            afterLogOut();

            logIn(false);

        }

    }

    @Override
    public final void quit() {
        frame.dispose();
    }

    @Override
    public final Context getApplicationContext() {
        return applicationContext;
    }

    public JFrame getFrame() {return frame;}
}
