package io.ultreia.gc.ui;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.config.GcConfig;
import io.ultreia.gc.service.api.GcServiceContext;
import java.lang.reflect.Field;
import javax.swing.JButton;
import org.jdesktop.swingx.JXLoginPane;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.auth.LoginService;
import org.jdesktop.swingx.auth.PasswordStore;

/**
 * Created by tchemit on 16/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LoginPanelUI<C extends GcConfig> extends JXLoginPane {

    private final C config;
    private final GcServiceContext serviceContext;

    public static <C extends GcConfig> LoginPanelUI create(AfterLoginUI ui, C config, GcServiceContext serviceContext) {
        return new LoginPanelUI<>(ui, config, serviceContext);
    }

    private LoginPanelUI(AfterLoginUI ui, C config, GcServiceContext serviceContext) {
        this.serviceContext = serviceContext;
        InternalLoginService service = new InternalLoginService();
        setLoginService(service);
        this.config = config;
        setSaveMode(SaveMode.BOTH);
        setBannerText("Geocaching.com");

        setPasswordStore(new InternalPasswordStore());

        addPropertyChangeListener("status", evt -> {

            Status newValue = (Status) evt.getNewValue();
            if (Status.SUCCEEDED.equals(newValue)) {

                ui.afterLogin(service.getName(), service.getAuthId());
            }
        });

    }

    public void init(boolean autoLogin) {

        setUserName(config.getLogin());
        String password = config.getPassword();
        if (password != null) {
            setPassword(password.toCharArray());
        }
        setStatus(Status.NOT_STARTED);

        if (autoLogin) {
            try {
                Field field = JXLoginPane.class.getDeclaredField("buttonPanel");
                field.setAccessible(true);
                JXPanel buttonPanel = (JXPanel) field.get(this);
                ((JButton) buttonPanel.getComponent(0)).doClick();

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }


    public class InternalLoginService extends LoginService {

        String name;
        String authId;

        String getAuthId() {
            return authId;
        }

        String getName() {
            return name;
        }

        @Override
        public boolean authenticate(String name, char[] password, String server) throws Exception {
            try {
                authId = serviceContext.newLoginService().login(name, new String(password));
                this.name=name;
                return true;
            } catch (Exception e) {
                return false;
            }
        }
    }

    private class InternalPasswordStore extends PasswordStore {

        @Override
        public boolean set(String username, String server, char[] password) {
            config.setLogin(username);
            config.setPassword(new String(password));
            config.get().saveForUser();
            return true;
        }

        @Override
        public char[] get(String username, String server) {
            String password = config.getPassword();
            return password == null ? new char[0] : password.toCharArray();
        }

        @Override
        public void removeUserPassword(String username) {

        }
    }

}
