package io.ultreia.gc.ui;

/*-
 * #%L
 * GC toolkit :: API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.tool.ComputeArcheoScoreProgressMonitor;
import javax.swing.JProgressBar;

/**
 * Created by tchemit on 16/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ProgressPanelUI extends JProgressBar implements ComputeArcheoScoreProgressMonitor {

    public static final String PROPERTY_ARCHEO_LOG = "archeoLog";

    public ProgressPanelUI() {
        setBorderPainted(true);
        setStringPainted(true);
    }

    public void init(int nbSteps) {
        setMinimum(0);
        setMaximum(nbSteps);
        setValue(0);
    }

    @Override
    public void increment(String stepMessage) {
        setValue(getValue() + 1);
        float progression = (float) getValue() / getMaximum() * 100;
        setString(String.format("%s (%d%%)", stepMessage, (int) progression));
    }

    @Override
    public void onPointsAdded(GcArcheoLogEvent archeoLogEvent) {
        firePropertyChange(PROPERTY_ARCHEO_LOG, null, archeoLogEvent);
    }

    @Override
    public void onConflictDetected(GcArcheoLogEvent archeoLogEvent) {
        firePropertyChange(PROPERTY_ARCHEO_LOG, null, archeoLogEvent);
    }
}
