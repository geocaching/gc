# Gc changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2017-11-06 10:27.

## Version [1.3.4](https://gitlab.com/geocaching/gc/milestones/9)

**Closed at *In progress*.**


### Issues
  * [[bug 15]](https://gitlab.com/geocaching/gc/issues/15) **Trackables are no more visited (lfn)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[bug 16]](https://gitlab.com/geocaching/gc/issues/16) **Should only increments cacheCount if logType needs it (foundIt, attented)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[feature 14]](https://gitlab.com/geocaching/gc/issues/14) **Allow to edit field note (says the date and logType) in lfn** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[feature 17]](https://gitlab.com/geocaching/gc/issues/17) **Improve lfn table of field notes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[feature 18]](https://gitlab.com/geocaching/gc/issues/18) **Add a new shortcut in lfn to open in a browser selected caches.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[feature 19]](https://gitlab.com/geocaching/gc/issues/19) **Add jpeg filter when choosing log photo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[feature 20]](https://gitlab.com/geocaching/gc/issues/20) **Use neutral user in CAS** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.3.3](https://gitlab.com/geocaching/gc/milestones/8)

**Closed at *In progress*.**


### Issues
  * [[feature 10]](https://gitlab.com/geocaching/gc/issues/10) **Add cache type icon in fln** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[feature 11]](https://gitlab.com/geocaching/gc/issues/11) **Improve table columns widths in lfn** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[feature 12]](https://gitlab.com/geocaching/gc/issues/12) **Use new page from geocaching.com for lfn** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.3.2](https://gitlab.com/geocaching/gc/milestones/7)

**Closed at *In progress*.**


### Issues
  * [[feature 8]](https://gitlab.com/geocaching/gc/issues/8) **Suppression de l&#39;installeur webstart** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[feature 9]](https://gitlab.com/geocaching/gc/issues/9) **Ajout du nom de l&#39;utilisateur lorsqu&#39;il est connecté.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.3.1](https://gitlab.com/geocaching/gc/milestones/6)

**Closed at 2017-05-26.**


### Issues
No issue.

## Version [1.3](https://gitlab.com/geocaching/gc/milestones/4)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.2](https://gitlab.com/geocaching/gc/milestones/3)

**Closed at *In progress*.**


### Issues
No issue.

## Version [1.1](https://gitlab.com/geocaching/gc/milestones/2)

**Closed at *In progress*.**


### Issues
  * [[bug 5]](https://gitlab.com/geocaching/gc/issues/5) **Can&#39;t use GAS in webstart** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[feature 3]](https://gitlab.com/geocaching/gc/issues/3) **Add photos feature** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0](https://gitlab.com/geocaching/gc/milestones/1)

**Closed at 2017-04-12.**


### Issues
  * [[feature 1]](https://gitlab.com/geocaching/gc/issues/1) **Initial import** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[feature 4]](https://gitlab.com/geocaching/gc/issues/4) **Add an archeo challenges checker** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

