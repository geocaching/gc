package io.ultreia.gc.rest.api.internal;

/*
 * #%L
 * GC toolkit :: REST API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Collection;
import java.util.Map;
import java.util.function.Supplier;

/**
 * @author Arnaud Thimel - thimel@codelutin.com
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcDtoGsonSupplier implements Supplier<Gson> {

    public static final Supplier<Gson> DEFAULT_GSON_SUPPLIER = () -> new GcDtoGsonSupplier().get();

    protected final boolean prettyPrint;

    protected GsonBuilder gsonBuilder;

    protected Gson gson;

    public GcDtoGsonSupplier() {
        this(false);
    }

    public GcDtoGsonSupplier(boolean prettyPrint) {
        this.prettyPrint = prettyPrint;
    }

    public static <K, V> TypeToken<Map<K, V>> mapOf(TypeToken<K> keyType, TypeToken<V> valueType) {
        return new TypeToken<Map<K, V>>() {
        }
                .where(new TypeParameter<K>() {
                }, keyType)
                .where(new TypeParameter<V>() {
                }, valueType);
    }

    public static <K, V> TypeToken<Map<K, Collection<V>>> multimapOf(TypeToken<K> keyType, TypeToken<V> valueType) {
        return new TypeToken<Map<K, Collection<V>>>() {
        }
                .where(new TypeParameter<K>() {
                }, keyType)
                .where(new TypeParameter<V>() {
                }, valueType);
    }

    @Override
    public Gson get() {
        if (gson == null) {
            gson = getGsonBuilder(prettyPrint).create();
        }
        return gson;
    }

    protected GsonBuilder getGsonBuilder(boolean prettyPrint) {

        if (gsonBuilder == null) {

            // Assign only when initialization is finished to be thread-safe
            gsonBuilder = new GsonBuilder();

            if (prettyPrint) {
                gsonBuilder.setPrettyPrinting();
            }

            gsonBuilder.enableComplexMapKeySerialization();
        }
        return gsonBuilder;

    }

}
