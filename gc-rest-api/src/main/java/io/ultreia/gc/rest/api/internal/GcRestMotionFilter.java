package io.ultreia.gc.rest.api.internal;

/*
 * #%L
 * GC toolkit :: REST API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import io.ultreia.gc.rest.api.GcRestApiApplicationContext;
import io.ultreia.gc.rest.api.GcRestApiRequestContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.debux.webmotion.server.WebMotionFilter;
import org.debux.webmotion.server.call.HttpContext;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcRestMotionFilter extends WebMotionFilter {

    private static final String REQUEST_AUTHENTICATION_TOKEN = "Authorization";

    public void inject(HttpContext context) {

        GcRestApiApplicationContext applicationContext = GcRestApiApplicationContext.getApplicationContext(context);

        HttpServletRequest request = context.getRequest();

        String authenticationToken = getRequestHeaderOrParameterValueOrNull(request, REQUEST_AUTHENTICATION_TOKEN);
        if (Strings.isNullOrEmpty(authenticationToken)) {
            authenticationToken = null;
        } else {
            authenticationToken = authenticationToken.replaceAll("\"", "");
        }
        GcRestApiRequestContext requestContext = new GcRestApiRequestContext(applicationContext, authenticationToken);
        GcRestApiRequestContext.setRequestContext(context, requestContext);

        doProcess();

    }

    private String getRequestHeaderOrParameterValueOrNull(HttpServletRequest request, String parameterName) {

        String result = request.getHeader(parameterName);
        if (StringUtils.isBlank(result)) {
            result = getRequestParameterValueOrNull(request, parameterName);
        }
        return result;

    }

    private String getRequestParameterValueOrNull(HttpServletRequest request, String parameterName) {

        String parameterValue = request.getParameter(parameterName);
        if (StringUtils.isBlank(parameterValue)) {
            parameterValue = null;
        }
        return parameterValue;

    }

}
