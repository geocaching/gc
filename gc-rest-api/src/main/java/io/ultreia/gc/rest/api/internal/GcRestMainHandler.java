package io.ultreia.gc.rest.api.internal;

/*
 * #%L
 * GC toolkit :: REST API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.debux.webmotion.server.WebMotionHandler;
import org.debux.webmotion.server.WebMotionMainHandler;
import org.debux.webmotion.server.handler.ExecutorInstanceCreatorHandler;
import org.debux.webmotion.server.handler.ExecutorParametersConvertorHandler;
import org.debux.webmotion.server.handler.ExecutorParametersInjectorHandler;
import org.debux.webmotion.server.tools.OrderedList;

/**
 * On utilise un main handler pour supprimer la validation proposée par hibernate car ça ne fonctionne pas!
 *
 * Created on 05/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcRestMainHandler extends WebMotionMainHandler {

    @Override
    public OrderedList<Class<? extends WebMotionHandler>> getExecutorHandlers() {
        return OrderedList.asList(
                ExecutorInstanceCreatorHandler.class,
                ExecutorParametersInjectorHandler.class,
                ExecutorParametersConvertorHandler.class
        );
    }
}
