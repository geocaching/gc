package io.ultreia.gc.rest.api;

/*
 * #%L
 * GC toolkit :: REST API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import io.ultreia.gc.config.GcApiConfig;
import io.ultreia.gc.db.entity.GcDbTopiaApplicationContext;
import io.ultreia.gc.rest.api.internal.GcDtoGsonSupplier;
import io.ultreia.gc.rest.api.internal.GcRestApiApplicationListener;
import io.ultreia.gc.session.GcSession;
import io.ultreia.gc.session.GcSessionsStore;
import java.io.Closeable;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import javax.servlet.ServletContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debux.webmotion.server.call.HttpContext;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcRestApiApplicationContext implements Closeable {

    public static final String APPLICATION_CONTEXT_PARAMETER = GcRestApiApplicationContext.class.getName();

    private static final String MISSING_APPLICATION_CONTEXT =
            GcRestApiApplicationContext.class.getSimpleName() + " not found. You probably forgot to" +
                    " register " + GcRestApiApplicationListener.class.getName() + " in your web.xml";

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcRestApiApplicationContext.class);

    private GcDtoGsonSupplier gsonSupplier;

    private GcRestApiConfig config;
    private GcApiConfig serviceConfig;

    private final GcRestApiSessions sessions;
    private GcDbTopiaApplicationContext dbApplicationContext;

    public GcRestApiApplicationContext() {
        sessions = new GcRestApiSessions();
    }

    public static GcRestApiApplicationContext getApplicationContext(HttpContext context) {

        ServletContext servletContext = context.getServletContext();
        GcRestApiApplicationContext result =
                (GcRestApiApplicationContext) servletContext
                        .getAttribute(GcRestApiApplicationContext.APPLICATION_CONTEXT_PARAMETER);

        Preconditions.checkState(result != null, GcRestApiApplicationContext.MISSING_APPLICATION_CONTEXT);

        return result;

    }

    public void init(String contextPath) {

        log.info(String.format("Init application classifier (%s)", contextPath));

        // create configuration
        config = new GcRestApiConfig(contextPath);

        dbApplicationContext = GcDbTopiaApplicationContext.create(config.getClassifier());

        // create gson supplier
        boolean devMode = config.isDevMode();
        gsonSupplier = new GcDtoGsonSupplier(devMode);

        serviceConfig = new GcApiConfig();
        serviceConfig.setDataDirectory(config.getBaseDirectory());
        serviceConfig.setBuildDate(config.getBuildDate());
        serviceConfig.setBuildVersion(config.getBuildVersion());
        serviceConfig.setBuildNumber(config.getBuildNumber());
        serviceConfig.setClassifier(config.getClassifier());

        String neutralLogin = config.getNeutralLogin();
        Objects.requireNonNull(neutralLogin);
        String neutralPassword = config.getNeutralPassword();
        Objects.requireNonNull(neutralPassword );
        GcSessionsStore.get().initNeutral(neutralLogin, neutralPassword);

    }

    public GcDbTopiaApplicationContext getDbApplicationContext() {
        return dbApplicationContext;
    }

    @Override
    public void close() {

        try {
            sessions.close();
        } finally {
            dbApplicationContext.close();
        }

    }

    public GcDtoGsonSupplier getGsonSupplier() {
        return gsonSupplier;
    }

    public GcRestApiConfig getConfig() {
        return config;
    }

    public GcRestApiSessions getSessions() {
        return sessions;
    }

    public GcApiConfig getServiceConfig() {
        return serviceConfig;
    }

    public static class GcRestApiSessions implements Closeable {

        private final Set<String> sessions = new TreeSet<>();

        public String newSession(String login, String password) {
            String id = GcSessionsStore.login(login, password);
            sessions.add(id);
            return id;
        }

        public void removeSession(String session) {
            getSession(session).close();
            sessions.remove(session);
        }

        @Override
        public void close() {
            sessions.stream().map(this::getSession).forEach(GcSession::close);
            sessions.clear();
        }

        GcSession getSession(String id) {
            if (!sessions.contains(id)) {
                throw new IllegalStateException("Can't find auth id");
            }
            return GcSessionsStore.get().getSession(id).orElseThrow(IllegalStateException::new);
        }
    }
}
