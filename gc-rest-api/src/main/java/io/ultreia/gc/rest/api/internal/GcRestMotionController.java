package io.ultreia.gc.rest.api.internal;

/*
 * #%L
 * GC toolkit :: REST API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.rest.api.GcRestApiRequestContext;
import org.debux.webmotion.server.WebMotionController;

/**
 * @author Tony Chemit - dev@ultreia.io
 */
public abstract class GcRestMotionController extends WebMotionController {

    protected GcRestApiRequestContext getRequestContext() {
        return GcRestApiRequestContext.getRequestContext(getContext());
    }

}
