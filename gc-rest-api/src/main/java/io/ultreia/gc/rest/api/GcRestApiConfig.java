package io.ultreia.gc.rest.api;

/*-
 * #%L
 * GC toolkit :: REST API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.io.CharSource;
import com.google.common.io.Resources;
import io.ultreia.gc.db.ApplicationConfig2;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ApplicationConfigInit;
import org.nuiton.config.ApplicationConfigScope;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.topia.persistence.TopiaException;

public class GcRestApiConfig extends GeneratedGcRestApiConfig {

    /** Logger. */
    private static Log log = LogFactory.getLog(GcRestApiConfig.class);

    GcRestApiConfig(String classifier, String... args) {

        log.info(String.format("Create rest api config (%s), args: %s", classifier, Arrays.toString(args)));
        get().setDefaultOption(GcRestApiConfigOption.CLASSIFIER.getKey(), classifier);

        ApplicationConfig2 defaultConf = new ApplicationConfig2(ApplicationConfigInit.forScopes(ApplicationConfigScope.CLASS_PATH, ApplicationConfigScope.DEFAULTS, ApplicationConfigScope.OPTIONS).setConfigFileName("gc-rest-api.conf"));
        try {
            defaultConf.parse(args);
        } catch (ArgumentsParserException e) {
            throw new TopiaException(e);
        }
        Properties properties = defaultConf.getProperties(ApplicationConfigScope.CLASS_PATH);
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            get().setDefaultOption("" + entry.getKey(), "" + entry.getValue());
        }

        log.info(String.format("Use %s configuration file", getConfigFile()));
        get().setConfigFileName(getConfigFile());
        init(args);

        log.info(String.format("rest api config is initialized:\n%s", getConfigurationDescription()));

    }

    private void init(String... args) {

        if (log.isInfoEnabled()) {
            log.info(String.format("Init rest api config %s", Arrays.asList(args)));
        }

        try {
            get().parse(args);
        } catch (ArgumentsParserException e) {
            throw new GcRestApiConfigInitException("could not parse configuration", e);
        }

        File baseDirectory = getBaseDirectory();

        if (isDevMode() && !baseDirectory.exists()) {
            // on utilise un répertoire temporaire comme basedir

            if (log.isInfoEnabled()) {
                log.info("Using a dev mode configuration.");
            }
            try {
                // Toujours s'assurer que le répertoire temporarie du système existe
                Path tmpdir = Paths.get(System.getProperty("java.io.tmpdir"));
                if (!Files.exists(tmpdir)) {
                    Files.createDirectories(tmpdir);
                }

                baseDirectory = Files.createTempDirectory("gc-rest-api").toFile();
            } catch (IOException e) {
                throw new GcRestApiConfigInitException("could not create temporary basedir", e);
            }
            if (log.isInfoEnabled()) {
                log.info("Dev mode detected, use temporary basedir: " + baseDirectory);
            }
            setBaseDirectory(baseDirectory);
        }

        try {
            Files.createDirectories(baseDirectory.toPath());
        } catch (IOException e) {
            throw new GcRestApiConfigInitException("Impossible de créer le répertoire principal de l'application (" + baseDirectory + ")", e);
        }
        File temporaryDirectory = getTemporaryDirectory();
        try {
            Files.createDirectories(temporaryDirectory.toPath());
        } catch (IOException e) {
            throw new GcRestApiConfigInitException("Impossible de créer le répertoire temporaire de l'application (" + temporaryDirectory + ")", e);
        }

        File log4jConfigurationFile = getLog4jConfigurationFile();
        if (!log4jConfigurationFile.exists()) {

            if (log.isInfoEnabled()) {
                log.info("Generate default log4jConfigurationFile");
            }
            try {
                CharSource charSource = Resources.asCharSource(getClass().getResource("/gc-rest-api-log4j.conf"), Charsets.UTF_8);
                Files.write(log4jConfigurationFile.toPath(), charSource.readLines());
            } catch (IOException e) {
                throw new GcRestApiConfigInitException("Impossible de créer un fichier de log4j", e);
            }
        }

        initLog();

    }

    private void initLog() {

        File logFile = getLog4jConfigurationFile();

        if (!logFile.exists()) {
            throw new GcRestApiConfigInitException("Le fichier de configuration des logs (" + logFile + ") n'existe pas");
        }

        if (log.isInfoEnabled()) {
            log.info("Chargement du fichier de log : " + logFile);
        }

        Properties finalLogConfigurationProperties;
        try (BufferedReader inputStream = Files.newBufferedReader(logFile.toPath(), Charsets.UTF_8)) {

            Properties logConfigurationProperties = new Properties();
            logConfigurationProperties.load(inputStream);

            finalLogConfigurationProperties = loadProperties(logConfigurationProperties, get());

        } catch (Exception e) {
            throw new GcRestApiConfigInitException("Impossible de charger le fichier de configuration des logs", e);
        }

        LogManager.resetConfiguration();
        PropertyConfigurator.configure(finalLogConfigurationProperties);

        log = LogFactory.getLog(GcRestApiConfig.class);
        if (log.isInfoEnabled()) {
            log.info("Configuration des logs chargée depuis le fichier " + logFile);
        }

    }

    public static Properties loadProperties(Properties sourceProperties, ApplicationConfig config) {

        Properties targetProperties = new Properties();
        for (Map.Entry<Object, Object> entry : sourceProperties.entrySet()) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            String newValue = config.replaceRecursiveOptions(value);
            targetProperties.setProperty(key, newValue);
        }
        return targetProperties;
    }


}
