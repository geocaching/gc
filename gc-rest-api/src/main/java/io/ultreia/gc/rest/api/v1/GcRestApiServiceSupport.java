package io.ultreia.gc.rest.api.v1;

/*
 * #%L
 * GC toolkit :: REST API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.rest.api.GcRestApiRequestContext;
import io.ultreia.gc.rest.api.internal.GcRestMotionController;
import io.ultreia.gc.service.api.GcService;
import io.ultreia.gc.service.api.GcServiceContext;
import io.ultreia.gc.service.internal.GcServiceContextImpl;
import io.ultreia.java4all.http.HRestApiService;
import org.debux.webmotion.server.WebMotionContextable;

/**
 * Created on 30/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class GcRestApiServiceSupport<S extends GcService> extends GcRestMotionController implements GcService , HRestApiService<S> {

    private final Class<S> serviceType;
    private GcServiceContext serviceContext;

    protected GcRestApiServiceSupport(Class<S> serviceType) {
        this.serviceType = serviceType;
    }

    @Override
    public final S getService() {
        return serviceContext.newService(serviceType);
    }

    @Override
    public void setServiceContext(GcServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    @Override
    public GcServiceContext getServiceContext() {
        return serviceContext;
    }

    @Override
    public void setContextable(WebMotionContextable contextable) {
        super.setContextable(contextable);
        GcRestApiRequestContext requestContext = getRequestContext();
        setServiceContext(GcServiceContextImpl.create(requestContext.getAuthenticationToken(),
                                                      requestContext.getApplicationContext().getServiceConfig(),
                                                      requestContext.getApplicationContext().getDbApplicationContext()));
    }

}
