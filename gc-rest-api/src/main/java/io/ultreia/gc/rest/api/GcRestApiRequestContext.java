package io.ultreia.gc.rest.api;

/*
 * #%L
 * GC toolkit :: REST API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.debux.webmotion.server.call.HttpContext;

/**
 * Created on 4/25/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcRestApiRequestContext {

    private static final String REQUEST_GC_REST_REQUEST_CONTEXT = GcRestApiRequestContext.class.getName();

    public static GcRestApiRequestContext getRequestContext(HttpContext httpContext) {

        return (GcRestApiRequestContext)
                httpContext.getRequest().getAttribute(REQUEST_GC_REST_REQUEST_CONTEXT);
    }

    public static void setRequestContext(HttpContext httpContext, GcRestApiRequestContext serviceContext) {
        httpContext.getRequest().setAttribute(REQUEST_GC_REST_REQUEST_CONTEXT, serviceContext);
    }

    private final GcRestApiApplicationContext applicationContext;

    private final String optionalAuthenticationToken;

    public GcRestApiRequestContext(GcRestApiApplicationContext applicationContext, String authenticationToken) {
        this.applicationContext = applicationContext;
        this.optionalAuthenticationToken = authenticationToken;
    }

    public GcRestApiApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public String getAuthenticationToken() {
//        checkAuthenticationTokenIsPresent();
        return optionalAuthenticationToken;
    }

    private void checkAuthenticationTokenIsPresent() {
        if (optionalAuthenticationToken == null) {
            throw new IllegalStateException("No authentication token found in request");
        }
    }

}
