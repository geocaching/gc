package io.ultreia.gc.rest.api.internal.injector;

/*
 * #%L
 * GC toolkit :: REST API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import io.ultreia.gc.model.GcDto;
import java.lang.reflect.Type;
import org.apache.commons.beanutils.converters.AbstractConverter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.handler.ExecutorParametersInjectorHandler;
import org.debux.webmotion.server.mapping.Mapping;

/**
 * Created on 07/09/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GcDtoInjector extends AbstractConverter implements ExecutorParametersInjectorHandler.Injector {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GcDtoInjector.class);

    private final Gson gson;

    public GcDtoInjector(Gson gson) {
        this.gson = gson;
    }

    @Override
    public Object getValue(Mapping mapping, Call call, String name, Class<?> type, Type generic) {

        GcDto observeDto = null;
        if (GcDto.class.isAssignableFrom(type)) {

            Call.ParameterTree parameterTree = call.getParameterTree().getObject().get(name);

            String gsonContent = ((String[]) parameterTree.getValue())[0];

            observeDto = gson.fromJson(gsonContent, generic);

            log.debug("Inject observeDto: " + observeDto);

        }

        return observeDto;

    }

    @Override
    protected <T> T convertToType(Class<T> type, Object value) throws Throwable {
        T observeDto;

        if (GcDto.class.isAssignableFrom(type)) {

            String gsonContent = value.toString();

            observeDto = gson.fromJson(gsonContent, type);

            log.debug("convert observeDto: " + observeDto);

        } else {
            throw conversionException(type, value);
        }

        return observeDto;

    }

    @Override
    protected Class<?> getDefaultType() {
        return GcDto.class;
    }
}
