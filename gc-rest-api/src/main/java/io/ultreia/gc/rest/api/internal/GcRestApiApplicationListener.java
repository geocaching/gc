package io.ultreia.gc.rest.api.internal;

/*
 * #%L
 * GC toolkit :: REST API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import io.ultreia.gc.rest.api.GcRestApiApplicationContext;
import io.ultreia.gc.rest.api.GcRestApiApplicationContextInitException;
import io.ultreia.gc.rest.api.internal.injector.GcDtoInjector;
import io.ultreia.gc.rest.api.internal.injector.GcReferentialInjector;
import java.util.Date;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debux.webmotion.server.WebMotionServerListener;
import org.debux.webmotion.server.call.ServerContext;
import org.debux.webmotion.server.mapping.Mapping;

/**
 * @author Tony Chemit - dev@ultreia.io
 */
public class GcRestApiApplicationListener implements WebMotionServerListener {

    private static final Log log = LogFactory.getLog(GcRestApiApplicationListener.class);

    private GcRestApiApplicationContext applicationContext;

    @Override
    public void onStart(Mapping mapping, ServerContext context) {

        log.info("Initializing " + GcRestApiApplicationListener.class.getName());

        String contextPath = StringUtils.removeStart(context.getServletContext().getContextPath(), "/gc-rest-api");
        if (contextPath.isEmpty()) {
            contextPath = "";
        }
        int lastIndex = contextPath.lastIndexOf('-');
        if (lastIndex == -1) {
            contextPath = "";
        } else {
            contextPath = contextPath.substring(lastIndex);
        }
        log.info(String.format("Application starting on [%s] at %s...", contextPath, new Date()));

        applicationContext = new GcRestApiApplicationContext();
        try {
            applicationContext.init(contextPath);
        } catch (Exception e) {
            throw new GcRestApiApplicationContextInitException("Impossible d'initialiser le context applicatif", e);
        }

        Gson gson = applicationContext.getGsonSupplier().get();

        context.addInjector(new GcDtoInjector(gson));
        context.addInjector(new GcReferentialInjector());

        context.getServletContext().setAttribute(GcRestApiApplicationContext.APPLICATION_CONTEXT_PARAMETER, applicationContext);

        if (log.isInfoEnabled()) {
            log.info("Initializing " + GcRestApiApplicationListener.class.getName() + " done.");
        }
    }

    @Override
    public void onStop(ServerContext context) {

        if (log.isInfoEnabled()) {
            log.info("Destroying " + GcRestApiApplicationListener.class.getName());
        }

        IOUtils.closeQuietly(applicationContext);

        if (log.isInfoEnabled()) {
            log.info("Destroying " + GcRestApiApplicationListener.class.getName() + " done.");
        }
    }

}
