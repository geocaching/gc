package io.ultreia.gc.rest.api.internal.injector;

/*-
 * #%L
 * GC toolkit :: REST API
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.handler.ExecutorParametersInjectorHandler;
import org.debux.webmotion.server.mapping.Mapping;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class DateInjector implements ExecutorParametersInjectorHandler.Injector {

    private static final Log log = LogFactory.getLog(DateInjector.class);

    protected final String datePattern;

    public DateInjector(String datePattern) {
        this.datePattern = datePattern;
    }

    @Override
    public Object getValue(Mapping mapping, Call call, String name, Class<?> type, Type generic) {
        Date result = null;

        if (type.equals(Date.class)) {

            Call.ParameterTree parameterTree = call.getParameterTree().getObject().get(name);

            if (parameterTree != null) {
                String dateString = ((String[]) parameterTree.getValue())[0];

                SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);

                try {
                    result = dateFormat.parse(dateString);
                } catch (ParseException e) {
                    if (log.isErrorEnabled()) {
                        log.error("An exception occurred", e);
                    }
                    throw new JsonParseException("no parse to date from " + dateString, e);
                }
            }

        }

        return result;
    }
}
