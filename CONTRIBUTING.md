# Install commands

To be able to use some usefull commands, please run this unique command. It will create a **~/ultreiaio** directory
with some nice bash scripts.

```
wget -q -O - https://gitlab.com/ultreiaio/pom/raw/develop/bin/install.sh | bash
```

## Perform release
```
bash ~/ultreiaio/close-milestone.sh
bash ~/ultreiaio/create-stage.sh
bash ~/ultreiaio/close-and-release-stage.sh 'ioultreia'
bash ~/ultreiaio/update-changelog.sh
bash ~/ultreiaio/create-milestone.sh
```

## Regenerate changelog
``` 
bash ~/ultreiaio/update-changelog.sh
```

## Generate and publish site
``` 
mvn clean verify site site:stage scm-publish:publish-scm -DperformRelease
```