package io.ultreia.gc.cas;

/*-
 * #%L
 * GC toolkit :: CAS
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.config.GcConfig;
import io.ultreia.gc.config.SecurityHelper;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.nuiton.config.ArgumentsParserException;

public class GcCasConfig extends GeneratedGcCasConfig implements GcConfig {

    private static final String INTERNAL_KEY = "70D3RB7MIwaSv4cQG3B2OQ";

    private final SecurityHelper securityHelper;

    public static GcCasConfig create(String... args) throws ArgumentsParserException, IOException {
        GcCasConfig config = new GcCasConfig();
        config.get().parse(args);
        File dataDirectory = config.getDataDirectory();
        if (!dataDirectory.exists()) {
            Files.createDirectories(dataDirectory.toPath());
        }
        return config;
    }

    private GcCasConfig() {
        super();
        securityHelper = new SecurityHelper(INTERNAL_KEY);
    }

//    public GcCasConfig(ApplicationConfigInit init) {
//        super(init);
//        try {
//            byte[] encoded = decode(INTERNAL_KEY);
//            SecretKey key = new SecretKeySpec(encoded, "BlowFish");
//            cipherDecode = Cipher.getInstance("BlowFish");
//            cipherDecode.init(Cipher.ENCRYPT_MODE, key);
//        } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
//            throw new IllegalStateException(e);
//        }
//    }

    public File getMyFindsDefaultFile(String userName) {
        return getDataDirectory().toPath().resolve(String.format("myfinds-%s.gpx", userName)).toFile();
    }

    public File getArcheoDefaultFile(String userName) {
        return getDataDirectory().toPath().resolve(String.format("archeo-logs-%s.json", userName)).toFile();
    }

    @Override
    public String getWishKey() {
        return securityHelper.decrypt(super.getWishKey());
    }

    @Override
    public void setWishKey(String wishKey) {
        super.setWishKey(securityHelper.crypt(wishKey));
    }

    public static void main(String[] args) throws IOException, ArgumentsParserException {
        GcCasConfig config = create(args);
        SecurityHelper securityHelper = config.getSecurityHelper();
        System.out.println(securityHelper.crypt("teddylabeer"));
        System.out.println(securityHelper.crypt("Babod"));
    }

    public SecurityHelper getSecurityHelper() {
        return securityHelper;
    }
}
