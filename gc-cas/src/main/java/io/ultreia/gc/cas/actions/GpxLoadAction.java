package io.ultreia.gc.cas.actions;

/*-
 * #%L
 * GC toolkit :: CAS
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.cas.ui.ComputeArcheoScoreUI;
import io.ultreia.gc.ui.actions.UIActionSupport;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.KeyStroke;
import org.nuiton.jaxx.widgets.file.JaxxFileChooser;

/**
 * Created by tchemit on 18/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GpxLoadAction extends UIActionSupport<ComputeArcheoScoreUI> {

    public GpxLoadAction(ComputeArcheoScoreUI ui) {
        super(ui, KeyStroke.getKeyStroke("ctrl L"));
        putValue(NAME, "...");
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        File file = JaxxFileChooser.forLoadingFile()
                .setStartDirectory(ui.getApplicationContext().getConfig().getDataDirectory())
                .setApprovalText("Choose gpx file")
                .setFileHidingEnabled(true)
                .setParent(ui.getContentPanel())
                .setPatternOrDescriptionFilters(".+\\.gpx", "Gpx file")
                .choose();
        if (file != null) {
            ui.getModel().setGpxInputFile(file);
            ui.getContentPanel().updateApply();
        }
    }
}
