package io.ultreia.gc.cas.actions;

/*-
 * #%L
 * GC toolkit :: CAS
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.cas.ui.ComputeArcheoScoreUI;
import io.ultreia.gc.model.GcArcheoLog;
import io.ultreia.gc.model.GcArcheoLogsComputationResult;
import io.ultreia.gc.service.api.GpxService;
import io.ultreia.gc.service.internal.GpxServiceImpl;
import io.ultreia.gc.tool.ComputeArcheoScore;
import io.ultreia.gc.tool.ComputeArcheoScoreProgressMonitor;
import io.ultreia.gc.ui.ProgressPanelUI;
import io.ultreia.gc.ui.actions.UIActionSupport;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.List;
import java.util.Set;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;


import static io.ultreia.gc.ui.ProgressPanelUI.PROPERTY_ARCHEO_LOG;

/**
 * Created by tchemit on 18/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ComputeArcheoScoreAction extends UIActionSupport<ComputeArcheoScoreUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ComputeArcheoScoreAction.class);
    private final ComputeArcheoScore logService;
    private final GpxService gpxService;
    private Set<String> gcNames;

    public ComputeArcheoScoreAction(ComputeArcheoScoreUI ui, ComputeArcheoScore logService, GpxService gpxService) {
        super(ui, KeyStroke.getKeyStroke("alt pressed C"));
        putValue(MNEMONIC_KEY, (int) 'C');
        this.logService = logService;
        this.gpxService = gpxService;

        ui.getProgressPanel().addPropertyChangeListener(PROPERTY_ARCHEO_LOG, evt -> {
            ComputeArcheoScoreProgressMonitor.GcArcheoLogEvent newValue = (ComputeArcheoScoreProgressMonitor.GcArcheoLogEvent) evt.getNewValue();
            SwingUtilities.invokeLater(() -> ui.getContentPanel().addArcheoLogMatch(newValue.getArcheoLog(), newValue.getTotalPoints()));
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        ui.setBusy(true);

        gcNames = new GpxServiceImpl().getGcNames(ui.getModel().getGpxInputFile());
        log.info("Got " + gcNames.size() + " cache(s) in gpx file.");

        ui.getProgressPanel().init(gcNames.size() + 1);

        ui.getContentPanel().resetScores();

        new RunWorker().execute();

    }

    public class RunWorker extends SwingWorker<Void, Integer> {

        private boolean done;
        private GcArcheoLogsComputationResult gcArcheoLogsComputationResult;

        @Override
        protected Void doInBackground() throws Exception {

            long t0 = System.nanoTime();

            ProgressPanelUI progressPanel = ui.getProgressPanel();

            String username = ui.getModel().getArcheoUser();

            File cacheFile = ui.getApplicationContext().getConfig().getDataDirectory().toPath().resolve(String.format("archeo-logs-%s.json", username)).toFile();

            List<GcArcheoLog> cache = logService.loadArcheoCaches(cacheFile);
            log.info("Got already " + cache.size() + " cache(s) registered in cache.");

            log.info("Got " + gcNames.size() + " cache(s) to seek.");


            try {
                gcArcheoLogsComputationResult = logService.computeArcheoCaches(username, gcNames, cache, cacheFile, progressPanel);
                log.info("Total points: " + gcArcheoLogsComputationResult.getTotalPoints());
                done = true;
            } catch (Exception e) {
                log.error("Error while computing archeo logs...", e);
                throw e;
            }

            long t1 = System.nanoTime();

            log.info("Done in " + StringUtil.convertTime(t1 - t0));

            return null;
        }


        @Override
        protected void done() {
            if (done) {

                gcArcheoLogsComputationResult.sort();
                ui.getProgressPanel().increment(String.format("Computed %d archeo logs with success!", gcNames.size()));
                ui.getContentPanel().generateReport(gcNames, gcArcheoLogsComputationResult);
                ui.setBusy(false);
            }
        }
    }

}
