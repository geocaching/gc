package io.ultreia.gc.cas;

/*-
 * #%L
 * GC toolkit :: CAS
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.cas.ui.ComputeArcheoScoreUI;
import io.ultreia.gc.cas.ui.ComputeArcheoScoreUIModel;
import io.ultreia.gc.rest.client.GcRestClientApplicationContext;
import io.ultreia.gc.rest.client.GcRestClientConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 14/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ComputeArcheoScore {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ComputeArcheoScore.class);

    private final ComputeArcheoScoreContext applicationContext;

    public static void main(String[] args) throws Exception {

        // broken when jnlp security manager fails (see https://gitlab.com/geocaching/gc/issues/5)
        System.setSecurityManager(null);

        ComputeArcheoScore runner = new ComputeArcheoScore(args);

        runner.start();

    }

    private ComputeArcheoScore(String... args) throws Exception {
        GcCasConfig config = GcCasConfig.create(args);

        log.info("Config loaded\n" + config.getConfigurationDescription());


        GcRestClientConfig clientConfig = GcRestClientConfig.create(args);

        log.info("Client config loaded\n" + clientConfig.getConfigurationDescription());

        GcRestClientApplicationContext client = new GcRestClientApplicationContext(clientConfig);

        applicationContext = new ComputeArcheoScoreContext(config, client);
    }

    private void start() throws Exception {

        GcCasConfig config = applicationContext.getConfig();

        ComputeArcheoScoreUIModel model = new ComputeArcheoScoreUIModel();
        model.setGpxInputFile(config.getGpxfile());
        model.setArcheoUser(config.getArcheoUser());

        ComputeArcheoScoreUI ui = new ComputeArcheoScoreUI(applicationContext, model);
        ui.setBusy(true);
        ui.openConfig();
        ui.setBusy(false);

    }

}
