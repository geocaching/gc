package io.ultreia.gc.cas.ui;

/*-
 * #%L
 * GC toolkit :: CAS
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;

/**
 * Created by tchemit on 15/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ComputeArcheoScoreUIModel {

    private File gpxInputFile;
    private String archeoUser;

    public boolean isValid() {
        if (gpxInputFile == null || !gpxInputFile.exists()) {
            return false;
        }
        if (archeoUser == null || archeoUser.isEmpty()) {
            return false;
        }
        return true;
    }

    public void setGpxInputFile(File gpxInputFile) {
        this.gpxInputFile = gpxInputFile;
    }

    public File getGpxInputFile() {
        return gpxInputFile;
    }

    public String getArcheoUser() {
        return archeoUser;
    }

    public void setArcheoUser(String archeoUser) {
        this.archeoUser = archeoUser;
    }
}
