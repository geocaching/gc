package io.ultreia.gc.cas.ui;

/*-
 * #%L
 * GC toolkit :: CAS
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.cas.ComputeArcheoScoreContext;
import io.ultreia.gc.cas.GcCasConfig;
import io.ultreia.gc.cas.actions.ComputeArcheoScoreAction;
import io.ultreia.gc.cas.actions.DoomAction;
import io.ultreia.gc.cas.actions.GpxLoadAction;
import io.ultreia.gc.model.GcArcheoLog;
import io.ultreia.gc.model.GcLog;
import io.ultreia.gc.model.GcArcheoLogsComputationResult;
import io.ultreia.gc.tool.ComputeArcheoScore;
import io.ultreia.gc.ui.ContentPanelUI;
import io.ultreia.gc.ui.actions.LogOutAction;
import io.ultreia.gc.ui.actions.QuitAction;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;

/**
 * Created by tchemit on 16/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ComputeArcheoScoreContentPanelUI extends ContentPanelUI<GcCasConfig, ComputeArcheoScoreContext, ComputeArcheoScoreUIModel> {

    private final ComputeArcheoScoreUI ui;
    private final ComputeArcheoScoreUIModel model;
    private final JButton computeArcheoLogs;
    private final JButton logOut;
    private final JButton gpxFileButton;
    private final JTextArea resultContent;
    private final ComputeArcheoScoreTreeModel scoreTreeModel;
    private final JTree scoreTree;
    final JTextField archeoUserText;
    private final JLabel gpxFileLabel;

    ComputeArcheoScoreContentPanelUI(ComputeArcheoScoreUI ui, ComputeArcheoScoreUIModel model) {
        this.ui = ui;
        this.model = model;
        setLayout(new BorderLayout());


        JPanel configuration = new JPanel();
        configuration.setLayout(new GridLayout(0, 1));
        add(configuration, BorderLayout.NORTH);

        JPanel gpxFilePanel = new JPanel();
        gpxFilePanel.setLayout(new BorderLayout());
        configuration.add(gpxFilePanel);
        gpxFileLabel = new JLabel("Choose gpx file to process");
        gpxFilePanel.add(gpxFileLabel, BorderLayout.WEST);
        gpxFileButton = new JButton("...");
        gpxFileButton.setPreferredSize(new Dimension(300, 20));
        gpxFilePanel.add(gpxFileButton, BorderLayout.EAST);

        JPanel archeoUserPanel = new JPanel();
        archeoUserPanel.setLayout(new BorderLayout());
        configuration.add(archeoUserPanel);
        JLabel jLabel = new JLabel("Geocaching.com user to test");
        archeoUserPanel.add(jLabel, BorderLayout.WEST);

        archeoUserText = new JTextField(model.getArcheoUser());
        archeoUserText.setPreferredSize(new Dimension(300, 20));
        archeoUserPanel.add(archeoUserText, BorderLayout.EAST);

        archeoUserText.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                String text = archeoUserText.getText();
                model.setArcheoUser(text);
                File findsFile = ui.getApplicationContext().getConfig().getMyFindsDefaultFile(text);
                if (findsFile.exists()) {
                    model.setGpxInputFile(findsFile);
                }
                updateApply();
            }
        });
        JPanel resultPanel = new JPanel();
        resultPanel.setLayout(new GridLayout(1, 0));
        add(resultPanel, BorderLayout.CENTER);

        scoreTreeModel = new ComputeArcheoScoreTreeModel();
        scoreTree = new JTree(scoreTreeModel);
        scoreTree.setShowsRootHandles(false);
        JScrollPane resultTreeScrollPane = new JScrollPane(scoreTree);
        resultPanel.add(resultTreeScrollPane);

        resultContent = new JTextArea();
        resultContent.setEditable(false);
        JScrollPane resultContentScrollPane = new JScrollPane(resultContent);
        resultPanel.add(resultContentScrollPane);

        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new BorderLayout());
        add(bottomPanel, BorderLayout.SOUTH);

        JPanel actions = new JPanel();
        actions.setLayout(new GridLayout(1, 0));
        bottomPanel.add(actions, BorderLayout.SOUTH);

        computeArcheoLogs = new JButton();
        logOut = new JButton();
        actions.add(logOut);
        actions.add(computeArcheoLogs);
    }

    @Override
    public void installActions(ComputeArcheoScoreContext applicationContext, ComputeArcheoScoreUIModel model) {

        gpxFileButton.setAction(new GpxLoadAction(ui));

        new QuitAction<>(ui);

        ComputeArcheoScoreAction computeArcheoScoreAction = new ComputeArcheoScoreAction(ui, applicationContext.newService(ComputeArcheoScore.class), applicationContext.newGpxService());
        computeArcheoLogs.setAction(computeArcheoScoreAction);
        computeArcheoLogs.setText("Compute archeo logs");

        LogOutAction logOutAction = new LogOutAction<>(ui);
        logOut.setAction(logOutAction);
        logOut.setText("Log in");

        new DoomAction(ui);

        updateApply();
    }

    @Override
    public void updateApply() {
        computeArcheoLogs.setEnabled(model.isValid());
        gpxFileLabel.setText(String.format("Choose gpx file to process (selected %s)", model.getGpxInputFile() == null ? "not selected" : model.getGpxInputFile()));
    }

    public void setBusy(boolean value) {
        boolean b = !value;

        logOut.setEnabled(b);
        gpxFileButton.setEnabled(b);
        archeoUserText.setEnabled(b);
        boolean canCompute = b;

        if (b) {
            ComputeArcheoScoreContext applicationContext = ui.getApplicationContext();
            canCompute = applicationContext.getAuthId().isPresent() && model.isValid();
        }
        computeArcheoLogs.setEnabled(canCompute);
    }

    @Override
    public JButton getLogOut() {
        return logOut;
    }

    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    public void addArcheoLogMatch(GcArcheoLog archeoLog, int totalPoints) {
        int oldScore = scoreTreeModel.getRoot().getScore();
        scoreTreeModel.addScore(archeoLog);
        if (oldScore == 0) {
            scoreTree.expandRow(0);
        }
        String text;
        if (archeoLog.getScore() == 0) {
            text = String.format("\n[Total points: %d] - Found a cache %s with logs on same day from : %s [my find: %s - previous: %s]",
                                 totalPoints,
                                 archeoLog.getGcName(),
                                 archeoLog.getOtherLogSameDay().stream().map(GcLog::getUserName).collect(Collectors.toList()),
                                 SIMPLE_DATE_FORMAT.format(archeoLog.getMyLog().getLogDate()),
                                 archeoLog.getPreviousLogOtherDay().map(GcLog::getLogDate).map(SIMPLE_DATE_FORMAT::format).orElse(null));
        } else {
            text = String.format("\n[Total points: %d] - Found a %d points cache: %s (%d days) [my find: %s - previous: %s]",
                                 totalPoints,
                                 archeoLog.getScore(),
                                 archeoLog.getGcName(),
                                 archeoLog.getNbDays(),
                                 SIMPLE_DATE_FORMAT.format(archeoLog.getMyLog().getLogDate()),
                                 archeoLog.getPreviousLog().map(GcLog::getLogDate).map(SIMPLE_DATE_FORMAT::format).orElse(null));
        }
        resultContent.setText(resultContent.getText() + text);

    }

    public void generateReport(Set<String> gcNames, GcArcheoLogsComputationResult gcArcheoLogsComputationResult) {

        StringBuilder buffer = new StringBuilder();

        buffer.append("## Summary  ##");

        buffer.append("\n\n").append("Report generated by Gc-CAS - copyright ©Ultreia.io 2017.");
        buffer.append("\n\n").append("Username: ").append(model.getArcheoUser());
        buffer.append("\n").append("CacheCount:   ").append(gcNames.size());
        buffer.append("\n").append("Date:         ").append(new Date());

        buffer.append("\n\nFound:               ");

        ComputeArcheoScoreTreeModel.RootNode root = scoreTreeModel.getRoot();
        for (ComputeArcheoScoreTreeModel.ScoreNode scoreNode : root) {
            buffer.append("\n * ").append(scoreNode);
        }

        buffer.append("\n\n").append(String.format("You score is %d (on %d caches).", root.getScore(), root.getNbCaches()));

        for (ComputeArcheoScoreTreeModel.ScoreNode scoreNode : root) {

            buffer.append("\n\n## Details for caches with ").append(scoreNode.getUserObject()).append(" points ##");

            buffer.append("\n\n").append("CacheCount: ").append(scoreNode.getNbCaches());
            buffer.append("\n").append("Score: ").append(scoreNode.getScore());
            buffer.append("\n");

            List<ComputeArcheoScoreTreeModel.CacheNode> nodes = new LinkedList<>();
            for (ComputeArcheoScoreTreeModel.CacheNode cacheNode : scoreNode) {
                nodes.add(cacheNode);
            }
            nodes.sort(Comparator.comparing(cacheNode -> cacheNode.getUserObject().getMyLog().getLogDate()));
            for (ComputeArcheoScoreTreeModel.CacheNode cacheNode : nodes) {
                buffer.append("\n * ").append(cacheNode);
            }
        }

        Set<String> badGcNames = gcArcheoLogsComputationResult.getBadGcNames();
        buffer.append(String.format("\n\n## Cache I did not found (%d) ##", badGcNames.size()));
        buffer.append("\n\n");


        for (String gcName : badGcNames) {
            buffer.append("\n * ").append(gcName);
        }

        resultContent.setText(buffer.toString());
    }

    public void resetScores() {
        resultContent.setText("");
        for (ComputeArcheoScoreTreeModel.ScoreNode scoreNode : scoreTreeModel.getRoot()) {
            scoreNode.removeAllChildren();
            scoreTreeModel.nodeStructureChanged(scoreNode);
        }
    }
}
