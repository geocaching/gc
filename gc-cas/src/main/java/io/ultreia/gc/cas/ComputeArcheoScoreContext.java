package io.ultreia.gc.cas;

/*-
 * #%L
 * GC toolkit :: CAS
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.rest.client.GcRestClientApplicationContext;
import io.ultreia.gc.service.api.GcService;
import io.ultreia.gc.ui.GcApplicationContextSupport;

/**
 * Created by tchemit on 16/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ComputeArcheoScoreContext extends GcApplicationContextSupport<GcCasConfig> {

    private final GcRestClientApplicationContext client;

    ComputeArcheoScoreContext(GcCasConfig config, GcRestClientApplicationContext client) {
        super(config);
        this.client = client;
    }

    @Override
    public void setAuthId(String authId) {
        super.setAuthId(authId);
        client.setAuthId(authId);
    }

    @Override
    public <S extends GcService> Class<S> getServiceClass(Class<S> serviceType) {
        return client.getServiceClass(serviceType);
    }

    @Override
    public <S extends GcService> S newService(Class<S> serviceType) {
        return client.newService(serviceType);
    }

}
