package io.ultreia.gc.cas.actions;

/*-
 * #%L
 * GC toolkit :: CAS
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.ultreia.gc.cas.GcCasConfig;
import io.ultreia.gc.cas.ui.ComputeArcheoScoreUI;
import io.ultreia.gc.config.SecurityHelper;
import io.ultreia.gc.service.api.FieldNotesService;
import io.ultreia.gc.ui.actions.UIActionSupport;
import java.awt.AWTEvent;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 18/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DoomAction extends UIActionSupport<ComputeArcheoScoreUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DoomAction.class);

    public DoomAction(ComputeArcheoScoreUI ui) {
        super(ui, KeyStroke.getKeyStroke("ctrl shift D"));
        putValue(NAME, getClass().getName());
    }


    private final Object lock = new Object();

    private List<Character> request = new LinkedList<>();

    private final FieldNotesService fieldNotesService = ui.getApplicationContext().newService(FieldNotesService.class);

    @Override
    public void actionPerformed(ActionEvent e) {

        new RunWorker().execute();

    }

    public class RunWorker extends SwingWorker<Void, Integer> {

        private final Gson gson = new GsonBuilder().create();
        private final Map<String, JTextArea> editors = new TreeMap<>();
        private boolean done;
        private SecurityHelper commonSecurityHelper;

        @Override
        protected Void doInBackground() throws Exception {

            log.debug("start to listen for doom.");
            request.clear();

            List<Character> response = new LinkedList<>();
            ui.getApplicationContext().getConfig().getWishKey().chars().forEach(c -> response.add((char) c));

            Toolkit.getDefaultToolkit().addAWTEventListener(event -> {
                if (event instanceof KeyEvent && event.getID() == KeyEvent.KEY_RELEASED) {

                    char keyChar = ((KeyEvent) event).getKeyChar();
                    if (keyChar < 'a' || keyChar > 'z') {
                        return;
                    }
                    log.debug("Add to response: " + keyChar);
                    request.add(keyChar);

                    synchronized (lock) {
                        lock.notifyAll();
                    }
                }
            }, AWTEvent.KEY_EVENT_MASK);
            while (request.size() < response.size()) {

                log.debug(request);

                synchronized (lock) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e1) {
                        throw new IllegalStateException(e1);
                    }
                }
            }

            done = request.equals(response);
            log.info(String.format("Quit treatment process: %s", done));

            return null;
        }

        @Override
        protected void done() {
            if (!done) {
                return;
            }
            JFrame frame = ui.getFrame();
            JTabbedPane pane = new JTabbedPane();

            SwingUtilities.invokeLater(ui.getFrame()::validate);

            JTextArea textArea1 = null;
            try {
                GcCasConfig config = ui.getApplicationContext().getConfig();
                String writableTabs = config.getWritableTabs();
                String authId = ui.getApplicationContext().newLoginService().login(config.getLogin(), config.getPassword());
                ui.getApplicationContext().setAuthId(authId);

                log.info(String.format("log in: %s", authId));

                String common = fieldNotesService.computeCommon("");
                String[] split = common.split("--");
                String one = split[0];
                commonSecurityHelper = new SecurityHelper(one);

                String two = split[1];
                if (!"null".equals(two)) {
                    String text = commonSecurityHelper.decrypt(two);
                    Map<String, String> model;
                    try {
                        model = gson.fromJson(text, new TypeToken<Map<String, String>>() {
                        }.getType());
                    } catch (Exception e) {
                        // Model is not ok, let's regenerate it
                        model = new TreeMap<>();
                    }
                    if (model.isEmpty()) {
                        model.put("Mummy", "");
                        model.put("Toby", "");
                    }

                    for (Map.Entry<String, String> entry : model.entrySet()) {
                        String key = entry.getKey();
                        String value = entry.getValue();
                        JScrollPane scrollPane = new JScrollPane();
                        frame.setContentPane(pane);
                        JTextArea textArea = new JTextArea();
                        textArea.setAutoscrolls(true);
                        scrollPane.setViewportView(textArea);
                        textArea.setText(value);
                        textArea.setEditable(writableTabs.contains(","+key+","));
                        pane.add(key, scrollPane);
                        if (editors.isEmpty()) {
                            textArea1 = textArea;
                        }
                        editors.put(key, textArea);
                    }

                }

                pane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ctrl S"), "dome");
                pane.getActionMap().put("dome", new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        save();
                    }
                });

                SwingUtilities.invokeLater(textArea1::requestFocus);
            } catch (Exception e1) {
                // No worry don't care
                log.error("Hum can't share with you", e1);
            }
        }


        private void save() {

            Map<String, String> model = new TreeMap<>();
            for (Map.Entry<String, JTextArea> entry : editors.entrySet()) {
                model.put(entry.getKey(), entry.getValue().getText());
            }
            String text = gson.toJson(model);
            fieldNotesService.computeCommon(commonSecurityHelper.crypt(text));
        }

    }

}
