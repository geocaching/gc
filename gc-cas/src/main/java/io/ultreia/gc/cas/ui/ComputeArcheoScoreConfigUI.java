package io.ultreia.gc.cas.ui;

/*-
 * #%L
 * GC toolkit :: CAS
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.cas.ComputeArcheoScoreContext;
import io.ultreia.gc.model.GcUser;
import io.ultreia.gc.service.api.GcUserService;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Objects;
import java.util.Optional;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.widgets.file.JaxxFileChooser;

/**
 * Created by tchemit on 20/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
class ComputeArcheoScoreConfigUI extends JDialog {

    private final JTextField userNameEditor;
    private final JLabel gpxFileLabel;

    private File gpxFile;
    private String userName;
    private final JButton apply;
    private final ComputeArcheoScoreContext applicationContext;

    ComputeArcheoScoreConfigUI(JFrame owner, ComputeArcheoScoreContext applicationContext) {
        super(owner, true);

        this.applicationContext = applicationContext;

        setTitle("Configure CAS");
        setLayout(new BorderLayout());
        JPanel config = new JPanel(new GridLayout(0, 1));
        add(config, BorderLayout.CENTER);
        JPanel userNamePanel = new JPanel(new BorderLayout());
        config.add(userNamePanel);

        userNamePanel.add(new JLabel("User name"), BorderLayout.WEST);
        userNameEditor = new JTextField();
        userNamePanel.add(userNameEditor, BorderLayout.CENTER);
        userNameEditor.setMinimumSize(new Dimension(200, 30));

        JPanel gpxFilePanel = new JPanel(new BorderLayout());
        gpxFilePanel.setLayout(new BorderLayout());
        config.add(gpxFilePanel);
        gpxFileLabel = new JLabel("Gpx file");
        gpxFilePanel.add(gpxFileLabel, BorderLayout.WEST);
        JButton gpxFileEditor = new JButton();
        gpxFilePanel.add(gpxFileEditor, BorderLayout.EAST);

        gpxFileEditor.setAction(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gpxFile = JaxxFileChooser.forLoadingFile()
                        .setStartDirectory(applicationContext.getConfig().getDataDirectory())
                        .setApprovalText("Choose gpx file")
                        .setFileHidingEnabled(true)
                        .setParent(ComputeArcheoScoreConfigUI.this)
                        .setPatternOrDescriptionFilters(".+\\.gpx", "Gpx file")
                        .choose();

                refreshUI();
            }
        });
        gpxFileEditor.setIcon(SwingUtil.createActionIcon("fileChooser"));
        gpxFileEditor.setMnemonic('G');
        gpxFileEditor.setToolTipText("Choose gpx file (Alt G)");
        JPanel actions = new JPanel(new GridLayout(1, 0));

        add(actions, BorderLayout.SOUTH);

        JButton cancel = new JButton(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userName = null;
                gpxFile = null;
                ComputeArcheoScoreConfigUI.this.dispose();
            }
        });
        cancel.setText("Cancel");
        cancel.setMnemonic('C');
        actions.add(cancel);

        apply = new JButton(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                apply();

            }
        });
        apply.setText("Apply");
        apply.setMnemonic('A');
        actions.add(apply);

        userNameEditor.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                userName = userNameEditor.getText().trim();
                refreshUI();
            }
        });

        refreshUI();

        setMinimumSize(new Dimension(400, 100));
        pack();

        SwingUtil.center(owner, this);
        setVisible(true);

    }

    private void apply() {

        Objects.requireNonNull(userName);

        Optional<GcUser> user = applicationContext.newService(GcUserService.class).getUser(userName);

        if (!user.isPresent()) {

            JOptionPane.showMessageDialog(this, String.format("User %s not found.", userName));
            return;
        }

        ComputeArcheoScoreConfigUI.this.dispose();
    }

    private void refreshUI() {

        gpxFileLabel.setText(gpxFile == null ? "Choose a Gpx File" : "Gpx file choose: " + gpxFile.getName());
        apply.setEnabled(isComplete());
    }

    String getUserName() {
        return userName;
    }

    File getGpxFile() {
        return gpxFile;
    }

    boolean isComplete() {
        return userName != null && gpxFile != null;
    }
}
