package io.ultreia.gc.cas.ui;

/*-
 * #%L
 * GC toolkit :: CAS
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.gc.model.GcArcheoLog;
import io.ultreia.gc.model.GcLog;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Iterator;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by tchemit on 19/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ComputeArcheoScoreTreeModel extends DefaultTreeModel {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ComputeArcheoScoreTreeModel.class);

    public ComputeArcheoScoreTreeModel() {
        super(new RootNode());
        getRoot().add(new ScoreNode(5));
        getRoot().add(new ScoreNode(10));
        getRoot().add(new ScoreNode(15));
        getRoot().add(new ScoreNode(20));
        getRoot().add(new ScoreNode(30));
    }

    @Override
    public RootNode getRoot() {
        return (RootNode) super.getRoot();
    }

    public void addScore(GcArcheoLog archeoLog) {

        CacheNode newNode = archeoLog.getScore() == 0 ? new CacheWithConflictNode(archeoLog) : new CacheNode(archeoLog);

        int score = archeoLog.getScore();
        Optional<ScoreNode> optionalScoreNode = getRoot().getScoreNode(score);
        ScoreNode scoreNode;
        if (optionalScoreNode.isPresent()) {
            scoreNode = optionalScoreNode.get();
        } else {

            int newPosition = getRoot().findNewPosition(score);

            insertNodeInto(scoreNode = new ScoreNode(score), getRoot(), newPosition);
        }

        int newPosition = scoreNode.findNewPosition(newNode);

        insertNodeInto(newNode, scoreNode, newPosition);
        log.debug("root updated: " + getRoot().toString());

        TreeNode[] pathToRoot = getPathToRoot(newNode);
        for (TreeNode treeNode : pathToRoot) {
            fireTreeNodesChanged(this, getPathToRoot(treeNode), null, null);
        }
    }

    public static class RootNode extends DefaultMutableTreeNode implements Iterable<ScoreNode> {

        Optional<ScoreNode> getScoreNode(int score) {
            return stream().filter(s -> score == s.getUserObject()).findFirst();
        }

        @Override
        public Iterator<ScoreNode> iterator() {
            return Collections.list(children()).iterator();
        }

        public int getScore() {
            return stream().mapToInt(ScoreNode::getScore).sum();
        }

        public int getNbCaches() {
            return stream().mapToInt(ScoreNode::getNbCaches).sum();
        }

        @Override
        public String toString() {
            return String.format("%d caches - total score: %d", getNbCaches(), getScore());
        }

        private Stream<ScoreNode> stream() {
            return StreamSupport.stream(spliterator(), false);
        }

        public int findNewPosition(int score) {
            int result = 0;
            Iterator<ScoreNode> iterator = iterator();
            while (iterator.hasNext()) {
                ScoreNode node = iterator.next();
                if (node.getScore() < score) {
                    result++;
                }
                break;
            }
            return result;
        }
    }

    public static class ScoreNode extends DefaultMutableTreeNode implements Iterable<CacheNode> {

        ScoreNode(int userObject) {
            super(userObject);
        }

        @Override
        public Integer getUserObject() {
            return (Integer) super.getUserObject();
        }

        @Override
        public Iterator<CacheNode> iterator() {
            return Collections.list(children()).iterator();
        }

        public int getScore() {
            return stream().mapToInt(CacheNode::getScore).sum();
        }

        public int getNbCaches() {
            return (int) stream().count();
        }

        @Override
        public String toString() {
            return String.format("%d caches with %d points - total score: %d", getNbCaches(), getUserObject(), getScore());
        }

        private Stream<CacheNode> stream() {
            return StreamSupport.stream(spliterator(), false);
        }

        public int findNewPosition(CacheNode newNode) {
            int i = 0;
            for (CacheNode cacheNode : this) {
                if (newNode.compareTo(cacheNode) < 0) {
                    return i;
                }
                i++;
            }
            return i;
        }
    }

    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    public static class CacheNode extends DefaultMutableTreeNode implements Comparable<CacheNode> {

        CacheNode(GcArcheoLog userObject) {
            super(userObject);
        }

        @Override
        public GcArcheoLog getUserObject() {
            return (GcArcheoLog) super.getUserObject();
        }

        public int getScore() {
            return getUserObject().getScore();
        }

        @Override
        public String toString() {
            GcArcheoLog archeoLog = getUserObject();
            return String.format("Cache %s - %d days (my: %s - previous: %s)", archeoLog.getGcName(),
                                 archeoLog.getNbDays(),
                                 SIMPLE_DATE_FORMAT.format(archeoLog.getMyLog().getLogDate()),
                                 archeoLog.getPreviousLog().map(GcLog::getLogDate).map(SIMPLE_DATE_FORMAT::format).orElse(null));
        }

        @Override
        public int compareTo(CacheNode o) {
            return getUserObject().compareTo(o.getUserObject());
        }
    }

    public static class CacheWithConflictNode extends CacheNode {

        CacheWithConflictNode(GcArcheoLog userObject) {
            super(userObject);
        }

        public int getScore() {
            return getUserObject().getScore();
        }

        @Override
        public String toString() {
            GcArcheoLog archeoLog = getUserObject();
            return String.format("Cache %s - %d days (score %d) (my: %s - previous: %s) (friends: %s)", archeoLog.getGcName(),
                                 archeoLog.getNbDaysOtherDay(),
                                 archeoLog.getScoreOtherDay(),
                                 SIMPLE_DATE_FORMAT.format(archeoLog.getMyLog().getLogDate()),
                                 archeoLog.getPreviousLogOtherDay().map(GcLog::getLogDate).map(SIMPLE_DATE_FORMAT::format).orElse(null),
                                 archeoLog.getOtherLogSameDay().stream().map(GcLog::getUserName).collect(Collectors.toList()));
        }
    }

}
